#include "DsimPhspace.hh"
#include <fstream>

#include "G4String.hh"

#include "TFile.h"
#include "TTree.h"

DsimPhspace::DsimPhspace(G4String filename){
  ConnectFile(filename);
  Nphoton=0;
  TotalE=0;
}

DsimPhspace::~DsimPhspace(){}

void DsimPhspace::ConnectFile(G4String filename)
{
  f = new TFile(filename,"read");
  
  if( !f->IsOpen() ) { 
    G4cout << "Error: Phasespace file <"<<filename<<"> could not be opened." << G4endl; 
    exit(1); 
  }else{
    G4cout << "Reading Phasespace : <"<<filename<<"> ..." << G4endl; 
  }
  
  t = (TTree*)f->Get("phase");
  t->SetBranchAddress("x",&x);
  t->SetBranchAddress("y",&y);
  t->SetBranchAddress("z",&z);
  t->SetBranchAddress("theta",&theta);
  t->SetBranchAddress("phi",&phi);
  t->SetBranchAddress("E",&E);
  
  N=t->GetEntries();
  G4cout << "> "<<N<<" entries" << G4endl; 
}


G4int DsimPhspace::Next()
{
  Nphoton++;
  //cout<<"............ "<<Nphoton<<endl;
  t->GetEntry( Nphoton );
  TotalE=TotalE+E;
  //cout<<Nphoton<<"............."<<endl;
  if (Nphoton>=N) return -1;
  else            return Nphoton;
}