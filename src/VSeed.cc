#include "G4RandomDirection.hh"
#include "Randomize.hh"
#include "VSeed.hh"

G4ThreeVector VSeed::SelectPointOnDisk(G4double rho) {
  G4double r = rho * sqrt(G4UniformRand());
  G4ThreeVector v = G4ThreeVector(r,0,0);
  v.setPhi(2*CLHEP::pi*G4UniformRand());
  return v;
}

G4ThreeVector VSeed::SelectPointOnCylinder(G4double rho, G4double halfLength) {
  G4ThreeVector v = G4ThreeVector(rho,0,0);
  v.setPhi(2.*CLHEP::pi*G4UniformRand());
  v.setZ(halfLength*(2.*G4UniformRand()-1.));
  return v;
}

G4ThreeVector VSeed::GetEmissionDirection() { return G4RandomDirection(); }

G4double VSeed::GetEmissionEnergy() {
  G4double rval = G4UniformRand();
  for (G4int i=0;i<numentries;i++) {
    if (rval<CDF[i]) return energy[i];
  }
  return energy[numentries-1];
}

// all in [cm]
G4double VSeed::RadialFunction(G4double r) { 
  if ( r>R[1] ) r = R[1];
  else if ( r<R[0] ) r = R[0];
  return (a[0]*pow(r,-2)+a[1]*pow(r,-1)+a[2]+a[3]*r+a[4]*pow(r,2)+a[5]*pow(r,3))*exp(-a[6]*r);
}
