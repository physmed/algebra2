//################################################################
// http://vali.physics.carleton.ca/clrp/seed_database/I125/ProstaSeed_125SL/
//################################################################
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4RandomDirection.hh"
#include "Randomize.hh"
#include "ProstaSeedSLI125.hh"

G4double  ProstaSeedSLI125::energy[5] = { 27.202*CLHEP::keV, 27.472*CLHEP::keV, 30.98*CLHEP::keV, 31.71*CLHEP::keV, 35.492*CLHEP::keV };

G4double  ProstaSeedSLI125::CDF[5] = { 0.2750, 0.7878, 0.9246, 0.9543, 1.00 };

G4double ProstaSeedSLI125::a[7] = {6.1867E-04,-1.5851E-02,1.1656,4.6505E-01,-4.1911E-03,3.6906E-03,4.7930E-01};

G4double ProstaSeedSLI125::R[2] = {0.05,10.};

ProstaSeedSLI125::ProstaSeedSLI125():VSeed("ProstaSeedSLI125") {
  VSeed::numentries = 5;
  VSeed::energy = ProstaSeedSLI125::energy;
  VSeed::CDF    = ProstaSeedSLI125::CDF;
  VSeed::a      = ProstaSeedSLI125::a;
  VSeed::R      = ProstaSeedSLI125::R;
  VSeed::Calibration = 0.4947; // +/- 0.0005
  VSeed::Lambda = 0.917;
  VSeed::HalfLife = 59.43*24; // [h]

  //################################################################
  // Seed dimensions
  //################################################################
  TiInRadius = 350.*CLHEP::um;
  TiOutRadius = 400.*CLHEP::um;
  TiZHalfLength = (3.7/2.)*CLHEP::mm;
  CoatingThickness = 1.*CLHEP::um;
  AgOutRadius = 250*CLHEP::um;
  AgZHalfLength = 1.50*CLHEP::mm;
  AirSphereRadius=350.*CLHEP::um;
  AgSpherePos=0.8*CLHEP::mm;
  AgSpherePos2=1.6*CLHEP::mm;
  VSeed::OutRadius = TiOutRadius;
  VSeed::ZHalfLength = TiZHalfLength + TiOutRadius;
  VSeed::ActiveLength = 2*AgZHalfLength;

}

void ProstaSeedSLI125::Construct() {

  //################################################################
  //Define the seed materials
  //################################################################
  G4Material* Air = G4NistManager::Instance()->FindOrBuildMaterial("G4_AIR");
  G4Material* mat_Ti = G4NistManager::Instance()->FindOrBuildMaterial("G4_Ti");
  G4Material* mat_Ag = G4NistManager::Instance()->FindOrBuildMaterial("G4_Ag");

  //################################################################
  // Define the seed geometry
  //################################################################
  
  // Air gap cylinder
  G4Tubs* AirCyl_vol = new G4Tubs("AirCyl_vol",0.,TiInRadius,TiZHalfLength-0.25*CLHEP::mm,0.,CLHEP::twopi);
  G4Sphere* R_air_sphere = new G4Sphere("Air_Sphere",0,AirSphereRadius,0.,CLHEP::twopi,0,CLHEP::halfpi);
  G4Sphere* L_air_sphere = new G4Sphere("Air_Sphere",0,AirSphereRadius,0.,CLHEP::twopi,CLHEP::halfpi,CLHEP::pi);
  G4UnionSolid* Vol_1 = new G4UnionSolid("Vol_1",AirCyl_vol,R_air_sphere,0,G4ThreeVector(0.,0.,TiZHalfLength-0.25*CLHEP::mm));
  G4UnionSolid* Air_caps = new G4UnionSolid("Air_caps",Vol_1,L_air_sphere,0,G4ThreeVector(0.,0.,-TiZHalfLength+0.25*CLHEP::mm));

  gap_log = new G4LogicalVolume(Air_caps,Air,"gap_log",0,0,0);
  G4VisAttributes* gap_att = new G4VisAttributes(G4Colour(0.9,0.9,1,0.5));
  gap_att->SetVisibility(true);
  gap_att->SetForceSolid(true);
  gap_log->SetVisAttributes(gap_att);

  // Ag sphere
  G4Sphere* Ag_sphere = new G4Sphere( "Ag Sphere",0,AgOutRadius,0.,CLHEP::twopi,0,CLHEP::pi);
  Ag_log = new G4LogicalVolume(Ag_sphere,mat_Ag,"Ag_log",0,0,0);
  G4VisAttributes* Ag_att = new G4VisAttributes(G4Colour(0.2,0.2,1.0));
  Ag_att->SetVisibility(true);
  Ag_att->SetForceSolid(true);
  Ag_log->SetVisAttributes(Ag_att);

  //Outer Titanium Capsule
  G4Tubs* TiCyl_vol = new G4Tubs("TiCyl_vol",0,TiOutRadius,TiZHalfLength,0.,CLHEP::twopi);
  G4Sphere* RCap = new G4Sphere("RCap",0,TiOutRadius,0.,CLHEP::twopi,0,CLHEP::halfpi);
  G4Sphere* LCap = new G4Sphere("LCap",0,TiOutRadius,0.,CLHEP::twopi,CLHEP::halfpi,CLHEP::pi);
  G4UnionSolid* Vol1 = new G4UnionSolid("Vol1",TiCyl_vol,RCap,0,G4ThreeVector(0.,0.,TiZHalfLength));
  G4UnionSolid* caps = new G4UnionSolid("caps",Vol1,LCap,0,G4ThreeVector(0.,0.,-TiZHalfLength));
  VSeed::OuterShell = caps;

  caps_log = new G4LogicalVolume(caps,mat_Ti,"caps_log",0,0,0);
  G4VisAttributes* caps_att = new G4VisAttributes(G4Colour(1,1,0.7,0.5));
  caps_att->SetVisibility(true);
  caps_att->SetForceSolid(true);
  caps_log->SetVisAttributes(caps_att);

}

void ProstaSeedSLI125::PlaceYourself(G4VPhysicalVolume* parent,G4int copyNo) {
  if ( !VSeed::OuterShell ) Construct();
  G4PVPlacement* caps_phys = new G4PVPlacement(0,G4ThreeVector(),"caps_phys",caps_log,parent,false,copyNo);
  G4PVPlacement* gaps_phys = new G4PVPlacement(0,G4ThreeVector(),"gap_phys",gap_log,caps_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0,0,-AgSpherePos2),"Ag_phys",Ag_log,gaps_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0,0,AgSpherePos2),"Ag_phys",Ag_log,gaps_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0,0,-AgSpherePos),"Ag_phys",Ag_log,gaps_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0,0,AgSpherePos),"Ag_phys",Ag_log,gaps_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(),"Ag_phys",Ag_log,gaps_phys,false,copyNo);
}

G4ThreeVector ProstaSeedSLI125::GetEmissionPosition() {
  G4ThreeVector v = G4RandomDirection();
  v.setR( AgOutRadius+G4UniformRand()*CoatingThickness );
  G4double u = G4UniformRand();
  if ( u<0.2 ) v[2] -= AgSpherePos;
  else if ( u<0.4 ) v[2] += AgSpherePos;
  else if ( u<0.6 ) v[2] -= AgSpherePos2;
  else if ( u<0.8 ) v[2] += AgSpherePos2;
  return v;
}
