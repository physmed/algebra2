#ifndef PhysicsList_hh
#define PhysicsList_hh

#include "G4VUserPhysicsList.hh"

class PhysicsList: public G4VUserPhysicsList {
public:
  PhysicsList(G4String& parWorldName);
  ~PhysicsList();

protected:
  void ConstructParticle();
  void ConstructProcess();
  void SetCuts();

private:
  void AddParallelWorldProcess();
  void ConstructEM();
  G4String pWorldName;

};

#endif
