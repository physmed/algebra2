#ifndef MicroSelectronV2Ir192_hh
#define MicroSelectronV2Ir192_hh

#include "VSeed.hh"

class G4LogicalVolume;

class MicroSelectronV2Ir192: public VSeed {
public:
  MicroSelectronV2Ir192();
  ~MicroSelectronV2Ir192(){;}
 
  void Construct();
  void PlaceYourself(G4VPhysicalVolume*,G4int);
  G4ThreeVector GetEmissionPosition();
  
private:
  static G4double energy[34];
  static G4double CDF[34];
  static G4double a[7];
  static G4double R[2];

  G4double source_c1_radius;
  G4double source_c1_half;
  G4double source_cons_rmax2;
  G4double source_cons_half;

  G4double caps_c_radius;
  G4double caps_c_half;
  G4double caps_con_r1;
  G4double caps_con_r2;
  G4double caps_con_half;

  G4double cable_half;
  G4double cable_pos;
  G4double phase;

  G4LogicalVolume* caps_log;
  G4LogicalVolume* core_log;
  G4LogicalVolume* cable_log;
};

#endif
