#include "DsimStackingAction.hh"
#include "G4Track.hh"

G4ClassificationOfNewTrack DsimStackingAction::ClassifyNewTrack(const G4Track* aTrack) {
    return Ctr->ClassifyNewTrack(aTrack); 
}
