//################################################################
// http://vali.physics.carleton.ca/clrp/seed_database/I125/Prospera_Med3631_ideal/                                                                                              
//################################################################
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4RandomDirection.hh"
#include "Randomize.hh"
#include "ProsperaSeed3631I125.hh"

G4double  ProsperaSeed3631I125::energy[5] = {27.202*CLHEP::keV, 27.472*CLHEP::keV, 30.98*CLHEP::keV, 31.71*CLHEP::keV, 35.492*CLHEP::keV};

G4double  ProsperaSeed3631I125::CDF[5] = {0.2750,0.7878,0.9246,0.9543,1.00};

G4double ProsperaSeed3631I125::a[7] = {8.8512E-04,-6.3400E-02,1.1537,2.9318E-01,-3.4692E-02,1.3775E-03,3.0090E-01};

G4double ProsperaSeed3631I125::R[2] = {0.05,10.};

ProsperaSeed3631I125::ProsperaSeed3631I125():VSeed("ProsperaSeed3631I125") {
  VSeed::numentries = 5;
  VSeed::energy = ProsperaSeed3631I125::energy;
  VSeed::CDF    = ProsperaSeed3631I125::CDF;
  VSeed::a      = ProsperaSeed3631I125::a;
  VSeed::R      = ProsperaSeed3631I125::R;
  VSeed::Calibration = 0.4486; // +/- 0.0004
  VSeed::Lambda = 0.917;
  VSeed::HalfLife = 59.43*24; // [h]

  //################################################################
  // Seed dimensions
  //################################################################
  TiInRadius = 355.*CLHEP::um;
  TiOutRadius = 405.*CLHEP::um;
  TiZHalfLength = (3.89/2.)*CLHEP::mm;
  CoatingThickness = 3.*CLHEP::um;
  PolyOutRadius = 280*CLHEP::um;
  PolyZHalfLength = 2.10*CLHEP::mm;
  AirSphereRadius=355.*CLHEP::um;
  PolySpherePos=1.084*CLHEP::mm;
  PolySpherePos2=1.807*CLHEP::mm;
  MarkerSpherePos=0.361*CLHEP::mm;
  VSeed::OutRadius = TiOutRadius;
  VSeed::ZHalfLength = TiZHalfLength + TiOutRadius;
  VSeed::ActiveLength = 2*PolyZHalfLength;
}

void ProsperaSeed3631I125::Construct() {

  //################################################################
  //Define the seed materials
  //################################################################
  G4int nel;
  G4double density, a, z;
  G4String name, symbol;

  G4Element* el_Cu = new G4Element(name="Copper",symbol="Cu",z=29,a=63.546*CLHEP::g/CLHEP::mole);
  G4Element* el_Au = new G4Element(name="Gold",symbol="Au",z= 79,a=196.96655*CLHEP::g/CLHEP::mole);
  G4Element* el_H  = new G4Element(name="Hydrogen",symbol="H",z=1,a=1.00794*CLHEP::g/CLHEP::mole);
  G4Element* el_C  = new G4Element(name= "Carbon",symbol="C",z=6,a=12.0107*CLHEP::g/CLHEP::mole);
  G4Element* el_N  = new G4Element(name="Nitrogen",symbol="N",z=7,a=14.00674*CLHEP::g/CLHEP::mole);
  G4Element* el_O  = new G4Element(name="Oxygene",symbol="O",z=8,a=16.00*CLHEP::g/CLHEP::mole);

  G4Material* Air = G4NistManager::Instance()->FindOrBuildMaterial("G4_AIR");
  G4Material* mat_Ti = G4NistManager::Instance()->FindOrBuildMaterial("G4_Ti");

  G4Material* mat_polystyrene = new G4Material(name="Polystyrene",density=1.26*CLHEP::g/CLHEP::cm3,nel=4 );
  mat_polystyrene->AddElement(el_H,0.0785);
  mat_polystyrene->AddElement(el_O,0.0168);
  mat_polystyrene->AddElement(el_C,0.8973);
  mat_polystyrene->AddElement(el_N,0.0074);

  G4Material* mat_marker = new G4Material(name="Ag/Cu",density=15.5*CLHEP::g/CLHEP::cm3,nel=2);
  mat_marker->AddElement(el_Au,0.8);
  mat_marker->AddElement(el_Cu,0.2);

  //################################################################
  // Define the seed geometry
  //################################################################

  // Air gap cylinder
  G4Tubs* AirCyl_vol = new G4Tubs("AirCyl_vol",0.,TiInRadius,TiZHalfLength,0.,CLHEP::twopi);
  G4Sphere* R_air_sphere = new G4Sphere("Air Sphere",0,AirSphereRadius,0.,CLHEP::twopi,0,CLHEP::halfpi);
  G4Sphere* L_air_sphere = new G4Sphere("Air Sphere",0,AirSphereRadius,0.,CLHEP::twopi,CLHEP::halfpi,CLHEP::pi);
  G4UnionSolid* Vol_1 = new G4UnionSolid("Vol_1",AirCyl_vol,R_air_sphere,0,G4ThreeVector(0.,0.,TiZHalfLength-0.05*CLHEP::mm));
  G4UnionSolid* Air_caps = new G4UnionSolid("Air_caps",Vol_1,L_air_sphere,0,G4ThreeVector(0.,0.,-TiZHalfLength+0.05*CLHEP::mm));

  gap_log = new G4LogicalVolume(Air_caps,Air,"gap_log",0,0,0);
  G4VisAttributes* gap_att = new G4VisAttributes(G4Colour(0.9,0.9,1,0.5));
  gap_att->SetVisibility(true);
  gap_att->SetForceSolid(true);
  gap_log->SetVisAttributes(gap_att);

  //Marker Sphere
  G4Sphere* marker_sphere = new G4Sphere("Marker Sphere",0,PolyOutRadius,0.,CLHEP::twopi,0,CLHEP::pi);
  marker_log = new G4LogicalVolume(marker_sphere,mat_marker,"marker_log",0,0,0);
  G4VisAttributes* marker_att = new G4VisAttributes(G4Colour(0.8,0.2,1.0));
  marker_att->SetVisibility(true);
  marker_att->SetForceSolid(true);
  marker_log->SetVisAttributes(marker_att);

  //Polystyrene sphere
  G4Sphere* Polystyrene_sphere = new G4Sphere("Polystyrene Sphere",0,PolyOutRadius,0.,CLHEP::twopi,0,CLHEP::pi);
  polysphere_log = new G4LogicalVolume(Polystyrene_sphere,mat_polystyrene,"polysphere_log",0,0,0);
  G4VisAttributes* polysphere_att = new G4VisAttributes(G4Colour(0.2,0.2,1.0));
  polysphere_att->SetVisibility(true);
  polysphere_att->SetForceSolid(true);
  polysphere_log->SetVisAttributes(polysphere_att);

  //Outer Titanium Capsule
  G4Tubs* TiCyl_vol = new G4Tubs("TiCyl_vol",0.,TiOutRadius,TiZHalfLength,0.,CLHEP::twopi);
  G4Sphere* RCap = new G4Sphere("RCap",0,TiOutRadius,0.,CLHEP::twopi,0,CLHEP::halfpi);
  G4Sphere* LCap = new G4Sphere("LCap",0,TiOutRadius,0.,CLHEP::twopi,CLHEP::halfpi,CLHEP::pi);
  G4UnionSolid* Vol1 = new G4UnionSolid("Vol1",TiCyl_vol,RCap,0,G4ThreeVector(0.,0.,TiZHalfLength));
  G4UnionSolid* caps = new G4UnionSolid("caps",Vol1,LCap,0,G4ThreeVector(0.,0.,-TiZHalfLength));
  VSeed::OuterShell = caps;

  caps_log = new G4LogicalVolume(caps,mat_Ti,"caps_log",0,0,0);
  G4VisAttributes* caps_att = new G4VisAttributes(G4Colour(1,1,0.7,0.5));
  caps_att->SetVisibility(true);
  caps_att->SetForceSolid(true);
  caps_log->SetVisAttributes(caps_att);

}

void ProsperaSeed3631I125::PlaceYourself(G4VPhysicalVolume* parent,G4int copyNo) {
  if ( !VSeed::OuterShell ) Construct(); 
  G4PVPlacement* caps_phys = new G4PVPlacement(0,G4ThreeVector(),"caps_phys",caps_log,parent,false,copyNo);
  G4PVPlacement* gap_phys = new G4PVPlacement(0,G4ThreeVector(),"gap_phys",gap_log,caps_phys,false,copyNo); 
  new G4PVPlacement(0,G4ThreeVector(0,0,-PolySpherePos2),"polysphere_phys",polysphere_log,gap_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0,0,PolySpherePos2),"polysphere_phys",polysphere_log,gap_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0,0,-PolySpherePos),"polysphere_phys",polysphere_log,gap_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0,0,PolySpherePos),"polysphere_phys",polysphere_log,gap_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0,0,-MarkerSpherePos),"marker_phys",marker_log,gap_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0,0,MarkerSpherePos),"marker_phys",marker_log,gap_phys,false,copyNo);
}

G4ThreeVector ProsperaSeed3631I125::GetEmissionPosition() {
  G4ThreeVector v = G4RandomDirection();     
  v.setR( PolyOutRadius+G4UniformRand()*CoatingThickness );
  G4double u = G4UniformRand();
  if ( u<0.25 ) v[2] += PolySpherePos;
  else if ( u<0.5 ) v[2] -= PolySpherePos;
  else if ( u<0.75 ) v[2] += PolySpherePos2;
  else v[2] -= PolySpherePos2;
  return v;
}
