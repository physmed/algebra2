#ifndef OrganicMaterial_hh
#define OrganicMaterial_hh

using namespace std;
class G4Material;
class TH1F;

class OrganicMaterial {
public :
  OrganicMaterial();
  ~OrganicMaterial();

  G4int GetNumberOfMaterials();
  G4int CreateBaseMaterial(G4String);
  vector<G4Material*> MatList;
  vector<TH1F*> DensityList;
  
  G4Material* Build(G4String);

private :
  G4int nMaterials;

};

#endif
