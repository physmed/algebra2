//################################################################
// http://vali.physics.carleton.ca/clrp/seed_database/Ir192_HDR/microSelectron_v2/
//################################################################
#include "G4Cons.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4Sphere.hh"
#include "G4Element.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Gamma.hh"
#include "Randomize.hh"
#include "GammaPlusPDRIr192.hh"

G4double GammaPlusPDRIr192::energy[34] = {65.1*CLHEP::keV, 66.8*CLHEP::keV, 75.7*CLHEP::keV, 136.3*CLHEP::keV, 296.0*CLHEP::keV, 308.5*CLHEP::keV, 316.5*CLHEP::keV, 468.1*CLHEP::keV, 476.5*CLHEP::keV, 588.6*CLHEP::keV,
           593.4*CLHEP::keV, 604.4*CLHEP::keV, 612.5*CLHEP::keV, 884.5*CLHEP::keV, 1061.5*CLHEP::keV, 1089.0*CLHEP::keV, 1378.0*CLHEP::keV, 61.5*CLHEP::keV, 63.0*CLHEP::keV, 71.4*CLHEP::keV, 177.0*CLHEP::keV, 201.3*CLHEP::keV,
           205.8*CLHEP::keV, 214.7*CLHEP::keV, 280.0*CLHEP::keV, 283.3*CLHEP::keV, 314.8*CLHEP::keV, 374.5*CLHEP::keV, 415.4*CLHEP::keV, 416.5*CLHEP::keV, 420.5*CLHEP::keV, 484.6*CLHEP::keV, 485.0*CLHEP::keV, 489.1*CLHEP::keV};

G4double GammaPlusPDRIr192::CDF[34] = {0.0117433, 0.0318809, 0.0406232, 0.0414104, 0.1662375, 0.2958489, 0.6568471, 0.8643123, 0.8643249, 0.8838101,
           0.8839980, 0.9191844, 0.9420621, 0.9432974, 0.9435253, 0.9435301, 0.9435357, 0.9485810, 0.9573232, 0.9610637, 0.9611072, 0.9630818,
           0.9769128, 0.9769185, 0.9769259, 0.9780089, 0.9781828, 0.9812665, 0.9812883, 0.9841763, 0.9844546, 0.9981117, 0.9981211, 1.0000000};

G4double GammaPlusPDRIr192::a[7] = {8.3320E-04,-1.5812E-03,9.9016E-01,1.1193E-01,5.2333E-03,-1.1597E-07,1.0127E-01};

G4double GammaPlusPDRIr192::R[2] = {0.2,20.}; //[cm]

GammaPlusPDRIr192::GammaPlusPDRIr192():VSeed("GammaPlusPDRIr192") { 
  VSeed::numentries = 34;
  VSeed::energy = GammaPlusPDRIr192::energy;
  VSeed::CDF    = GammaPlusPDRIr192::CDF;
  VSeed::a      = GammaPlusPDRIr192::a;
  VSeed::R      = GammaPlusPDRIr192::R;
  VSeed::Calibration = 0.9663; // +/- 0.0007
  VSeed::Lambda = 1.12;
  VSeed::HalfLife = 73.827*24; // [h]

  //################################################################
  // Seed dimensions
  //################################################################
  active_cl_radius = 0.3*CLHEP::mm;
  active_cl_half   = 0.25*CLHEP::mm;  // active length = 0.5 mm
  //
  cyln_al_radius = 0.3*CLHEP::mm;
  cyln_al_half   = 0.7*CLHEP::mm;   // aluminium cylinder
  //
  cap_steel_r1 = 0.0*CLHEP::mm;
  cap_steel_r2 = 0.45*CLHEP::mm;
  cap_steel_half = 0.06*CLHEP::mm;  // h=0.12
  //
  cyln1_steel_radius = 0.45*CLHEP::mm;
  cyln1_steel_half   = 0.15*CLHEP::mm;   // h=0.3
  //
  cyln2_steel_radius = 0.45*CLHEP::mm;
  cyln2_steel_radius_inner = 0.35*CLHEP::mm;
  cyln2_steel_half   = 1.0*CLHEP::mm;   // h=1.4 mm
  //
  cyln3_steel_radius = 0.45*CLHEP::mm;
  cyln3_steel_half   = 0.25*CLHEP::mm;   // h=0.5 mm
  // cable
  cable_steel_radius = 0.45*CLHEP::mm;
  //cable_steel_half   = 1.00*CLHEP::mm;   // h=2.0 mm
  cable_steel_half   = 30.0*CLHEP::mm;   // h=6 cm
  //
  VSeed::OutRadius = cyln1_steel_radius;
  VSeed::ZHalfLength = (0.3+0.5+1.4+0.1+0.62+2.0)/2;
  VSeed::ActiveLength = 0.5*CLHEP::mm;
}

void GammaPlusPDRIr192::Construct() {
  //################################################################
  // Define the seed materials
  //################################################################
  G4int nel;
  G4double density, a, z, pct;
  G4String name, symbol;

  G4Element* Ir = new G4Element(name="Ir",  symbol="Ir", z=77., a=192.21700*CLHEP::g/CLHEP::mole);
  G4Element* Fe = new G4Element(name="Iron",     symbol="Fe", z=26., a=55.845000*CLHEP::g/CLHEP::mole);
  G4Element* Cr = new G4Element(name="Chromium", symbol="Cr", z=24., a=51.996100*CLHEP::g/CLHEP::mole);
  G4Element* Ni = new G4Element(name="Nickel",   symbol="Ni", z=28., a=58.693400*CLHEP::g/CLHEP::mole);
  G4Element* Mn = new G4Element(name="Manganese",symbol="Mn", z=25., a=54.938049*CLHEP::g/CLHEP::mole);
  G4Element* Si = new G4Element(name="Silicium", symbol="Si", z=14., a=28.085500*CLHEP::g/CLHEP::mole);

  // Ir-192
  G4Material* mat_Ir = new G4Material("Ir",density=22.42*CLHEP::g/CLHEP::cm3,nel=1);
  mat_Ir->AddElement(Ir,pct=1.0);

  // Stainless Steel 316L (capsule)
  G4Material* mat_Steel = new G4Material("INOX_316L_capsule",density=7.80*CLHEP::g/CLHEP::cm3,nel=5);
  mat_Steel->AddElement(Fe, pct=0.6800);
  mat_Steel->AddElement(Cr, pct=0.1700);
  mat_Steel->AddElement(Ni, pct=0.1200);
  mat_Steel->AddElement(Mn, pct=0.0200);
  mat_Steel->AddElement(Si, pct=0.0100);

  // Stainless Steel AISi 304 (cable)
  G4Material* mat_Steel_304 = new G4Material("AISI_304_cable",density=5.60*CLHEP::g/CLHEP::cm3,nel=5);
  mat_Steel_304->AddElement(Fe, pct=0.6800);
  mat_Steel_304->AddElement(Cr, pct=0.1700);
  mat_Steel_304->AddElement(Ni, pct=0.1200);
  mat_Steel_304->AddElement(Mn, pct=0.0200);
  mat_Steel_304->AddElement(Si, pct=0.0100);

  // Aluminum
  density = 2.700*CLHEP::g/CLHEP::cm3;
  a = 26.98*CLHEP::g/CLHEP::mole;
  G4Material* mat_Al = new G4Material(name="Aluminum", z=13., a, density);

  //################################################################
  // Define the seed geometry
  //################################################################

  // Cylindrical active part
  G4Tubs* active_cyl = new G4Tubs("active_cyl",0.0,active_cl_radius,active_cl_half,0.,CLHEP::twopi);
  active_cyl_log = new G4LogicalVolume(active_cyl,mat_Ir,"active_cyl_log");
  // Cylindrical Aluminium
  G4Tubs* al_cyl = new G4Tubs("al_cyl",0.0,cyln_al_radius,cyln_al_half,0.,CLHEP::twopi);
  al_cyl_log = new G4LogicalVolume(al_cyl,mat_Al,"al_cyl_log");
  // Cylindrical 1,2,3,cap
  G4Tubs* cyln1_steel = new G4Tubs("cyln1_steel",0.0,cyln1_steel_radius,cyln1_steel_half,0.,CLHEP::twopi);  // Cylindrical 1
  G4Tubs* cyln2_steel = new G4Tubs("cyln2_steel",cyln2_steel_radius_inner,cyln2_steel_radius,cyln2_steel_half,0.,CLHEP::twopi);  // Cylindrical 2
  G4Tubs* cyln3_steel = new G4Tubs("cyln3_steel",0.0,cyln3_steel_radius,cyln3_steel_half,0.,CLHEP::twopi);  // Cyln 3
  G4Cons* cap_steel = new G4Cons("cap_steel",0.0,cap_steel_r2,0.0,cap_steel_r1,cap_steel_half,0.,CLHEP::twopi);   // Cap
  // Union of Cylindrical 1,2,3,cap
  G4UnionSolid* cyln12  = new G4UnionSolid("cyln1+2", cyln1_steel,cyln2_steel,0,G4ThreeVector(0.,0.,1.15*CLHEP::mm));
  G4UnionSolid* cyln123 = new G4UnionSolid("cyln123", cyln12,     cyln3_steel,0,G4ThreeVector(0.,0.,2.40*CLHEP::mm));
  G4UnionSolid* cyln123c= new G4UnionSolid("cyln123c",cyln123,    cap_steel,  0,G4ThreeVector(0.,0.,2.71*CLHEP::mm));
  cap_steel_log = new G4LogicalVolume(cyln123c,mat_Steel,"cap_steel_log");
  // Cable
  G4Tubs* cable_steel = new G4Tubs("cable_steel",0.0,cable_steel_radius,cable_steel_half,0.,CLHEP::twopi);
  cable_steel_log = new G4LogicalVolume(cable_steel,mat_Steel_304,"cable_steel_log");

  //G4UnionSolid* shell = new G4UnionSolid("shell",caps_vol,cable_vol,0,G4ThreeVector(0.,0.,-1.*cable_pos));
  VSeed::OuterShell = cyln123c;
}

void GammaPlusPDRIr192::PlaceYourself(G4VPhysicalVolume* parent,G4int copyNo) {
  if ( !VSeed::OuterShell ) Construct();
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.),"active_cyl_phys",active_cyl_log,parent,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,0.95*CLHEP::mm),"al_cyl_phys",al_cyl_log,parent,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,-0.40*CLHEP::mm),"caps_phys",cap_steel_log,parent,false,copyNo);    // <-- caps_phys   ** These 2 phys names are important 
  new G4PVPlacement(0,G4ThreeVector(0.,0.,(-0.25-0.3-cable_steel_half)*CLHEP::mm),"cable_phys",cable_steel_log,parent,false,copyNo); // <-- cable_phys  ** 
}

G4ThreeVector GammaPlusPDRIr192::GetEmissionPosition() {
  return G4ThreeVector(0,0,0.25*CLHEP::mm)+SelectPointOnCylinder(0.3*CLHEP::mm,0.25*CLHEP::mm);
}
