#ifndef VSeed_hh
#define VSeed_hh

#include "G4VisAttributes.hh"

class G4VPhysicalVolume;
class G4VSolid;

class VSeed {
public:
  VSeed(G4String name="seedtype"):seedName(name) { OuterShell=0; theSeed = this; }
  virtual ~VSeed(){;}

  inline G4String GetName() { return seedName; }
  G4VSolid* OuterShell;

  // Virtual methods to override
  virtual void Construct()=0;
  virtual void PlaceYourself(G4VPhysicalVolume*,G4int)=0;
  virtual G4ThreeVector GetEmissionPosition()=0;
  G4ThreeVector GetEmissionDirection();
  G4double GetEmissionEnergy();
  G4double RadialFunction(G4double r);

  G4double Calibration;
  G4double Lambda;
  G4double OutRadius;
  G4double ZHalfLength;
  G4double HalfLife;
  G4double ActiveLength;

protected:
  VSeed* theSeed;

  G4ThreeVector SelectPointOnDisk(G4double rho);
  G4ThreeVector SelectPointOnCylinder(G4double rho, G4double halfLength);

  // intrinsic parameters
  G4int numentries;
  G4double *energy;
  G4double *CDF;
  G4double *a;
  G4double *R;

  G4String seedName;

};

#endif
