//################################################################
// http://vali.physics.carleton.ca/clrp/seed_database/Ir192_HDR/microSelectron_v2/
//################################################################
#include "G4Cons.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4Sphere.hh"
#include "G4Element.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Gamma.hh"
#include "Randomize.hh"
#include "MicroSelectronV2Ir192.hh"

G4double MicroSelectronV2Ir192::energy[34] = {65.1*CLHEP::keV, 66.8*CLHEP::keV, 75.7*CLHEP::keV, 136.3*CLHEP::keV, 296.0*CLHEP::keV, 308.5*CLHEP::keV, 316.5*CLHEP::keV, 468.1*CLHEP::keV, 476.5*CLHEP::keV, 588.6*CLHEP::keV,
           593.4*CLHEP::keV, 604.4*CLHEP::keV, 612.5*CLHEP::keV, 884.5*CLHEP::keV, 1061.5*CLHEP::keV, 1089.0*CLHEP::keV, 1378.0*CLHEP::keV, 61.5*CLHEP::keV, 63.0*CLHEP::keV, 71.4*CLHEP::keV, 177.0*CLHEP::keV, 201.3*CLHEP::keV,
           205.8*CLHEP::keV, 214.7*CLHEP::keV, 280.0*CLHEP::keV, 283.3*CLHEP::keV, 314.8*CLHEP::keV, 374.5*CLHEP::keV, 415.4*CLHEP::keV, 416.5*CLHEP::keV, 420.5*CLHEP::keV, 484.6*CLHEP::keV, 485.0*CLHEP::keV, 489.1*CLHEP::keV};

G4double MicroSelectronV2Ir192::CDF[34] = {0.0117433, 0.0318809, 0.0406232, 0.0414104, 0.1662375, 0.2958489, 0.6568471, 0.8643123, 0.8643249, 0.8838101,
           0.8839980, 0.9191844, 0.9420621, 0.9432974, 0.9435253, 0.9435301, 0.9435357, 0.9485810, 0.9573232, 0.9610637, 0.9611072, 0.9630818,
           0.9769128, 0.9769185, 0.9769259, 0.9780089, 0.9781828, 0.9812665, 0.9812883, 0.9841763, 0.9844546, 0.9981117, 0.9981211, 1.0000000};

G4double MicroSelectronV2Ir192::a[7] = {-0.00082166,0.0063598,0.98048,0.13689,0.006429,1.9392e-4,0.12180};

G4double MicroSelectronV2Ir192::R[2] = {0.2,20.}; //[cm]

MicroSelectronV2Ir192::MicroSelectronV2Ir192():VSeed("MicroSelectronV2Ir192") { 
  VSeed::numentries = 34;
  VSeed::energy = MicroSelectronV2Ir192::energy;
  VSeed::CDF    = MicroSelectronV2Ir192::CDF;
  VSeed::a      = MicroSelectronV2Ir192::a;
  VSeed::R      = MicroSelectronV2Ir192::R;
  VSeed::Calibration = 0.9663; // +/- 0.0007
  VSeed::Lambda = 1.108;
  VSeed::HalfLife = 73.827*24; // [h]

  //################################################################
  // Seed dimensions
  //################################################################
  source_c1_radius = 0.325*CLHEP::mm;
  source_c1_half = 1.74*CLHEP::mm;
  source_cons_rmax2 = source_c1_radius-0.06*CLHEP::mm;
  source_cons_half = 0.03*CLHEP::mm;
  caps_c_radius = 0.45*CLHEP::mm;
  caps_c_half = 1.95*CLHEP::mm;
  caps_con_r1 = 0.45*CLHEP::mm;
  caps_con_r2 = 0.35*CLHEP::mm;
  caps_con_half = 0.075*CLHEP::mm;
  cable_half = 1.*CLHEP::mm; 
  phase = 0.4*CLHEP::mm;
  cable_pos = caps_c_half + 2.*caps_con_half + cable_half;

  VSeed::OutRadius = caps_c_radius;
  VSeed::ZHalfLength = caps_c_half + 2.*caps_con_half + 2.*cable_half - phase;
  VSeed::ActiveLength = 3.6*CLHEP::mm;
}

void MicroSelectronV2Ir192::Construct() {
  //################################################################
  // Define the seed materials
  //################################################################
  G4int nel;
  G4double density, a, z, pct;
  G4String name, symbol;

  G4Element* Ir = new G4Element(name="Ir",  symbol="Ir", z=77., a=192.21700*CLHEP::g/CLHEP::mole);
  G4Element* Fe = new G4Element(name="Iron",     symbol="Fe", z=26., a=55.845000*CLHEP::g/CLHEP::mole);
  G4Element* Cr = new G4Element(name="Chromium", symbol="Cr", z=24., a=51.996100*CLHEP::g/CLHEP::mole);
  G4Element* Ni = new G4Element(name="Nickel",   symbol="Ni", z=28., a=58.693400*CLHEP::g/CLHEP::mole);
  G4Element* Mn = new G4Element(name="Manganese",symbol="Mn", z=25., a=54.938049*CLHEP::g/CLHEP::mole);
  G4Element* Si = new G4Element(name="Silicium", symbol="Si", z=14., a=28.085500*CLHEP::g/CLHEP::mole);
  //G4Element* C  = new G4Element(name="Carbon",   symbol="C" , z=6. , a=12.010700*CLHEP::g/CLHEP::mole); // 12.011

  G4Material* mat_Ir = new G4Material("Ir",density=22.42*CLHEP::g/CLHEP::cm3,nel=1);
  mat_Ir->AddElement(Ir,pct=1.0);

  // Stainless Steel 316L (capsule)
  G4Material* mat_Steel = new G4Material("INOX_316L_capsule",density=8.06*CLHEP::g/CLHEP::cm3,nel=5);
  mat_Steel->AddElement(Fe, pct=0.6800);
  mat_Steel->AddElement(Cr, pct=0.1700);
  mat_Steel->AddElement(Ni, pct=0.1200);
  mat_Steel->AddElement(Mn, pct=0.0200);
  mat_Steel->AddElement(Si, pct=0.0100);

  // cable
  G4Material* mat_Cable = G4NistManager::Instance()->BuildMaterialWithNewDensity("INOX_316L_cable",mat_Steel->GetName(),4.81*CLHEP::g/CLHEP::cm3);

  //################################################################
  // Define the seed geometry
  //################################################################

  // Cylindrical source
  G4Tubs* source_cyl = new G4Tubs("source_cyl",0.0,source_c1_radius,source_c1_half,0.,CLHEP::twopi);
  G4Cons* source_cone1 = new G4Cons("source_cone1",0.,source_c1_radius,0.,source_cons_rmax2,source_cons_half,0.,CLHEP::twopi);
  G4Cons* source_cone2 = new G4Cons("source_cone2",0.,source_cons_rmax2,0.,source_c1_radius,source_cons_half,0.,CLHEP::twopi);

  G4VSolid* source_half = new G4UnionSolid("source_half",source_cyl,source_cone1,0,G4ThreeVector(0.,0.,source_c1_half+source_cons_half));
  G4VSolid* source_Ir = new G4UnionSolid("source_Ir",source_half,source_cone2,0,G4ThreeVector(0.,0.,-1.*(source_c1_half+source_cons_half))); 

  core_log = new G4LogicalVolume(source_Ir,mat_Ir,"core_log");
  G4VisAttributes* core_att = new G4VisAttributes(G4Colour(0,0,1));
  core_att->SetVisibility(true);
  core_att->SetForceSolid(true);
  core_log->SetVisAttributes(core_att);

  // Steel Capsule
  G4Tubs* caps_cyl = new G4Tubs("caps_cyl",0.,caps_c_radius,caps_c_half,0.,CLHEP::twopi);
  G4Sphere* caps_sphere = new G4Sphere("caps_sphere",0.,caps_c_radius,0.,CLHEP::twopi,0.,CLHEP::halfpi);
  G4Cons* caps_cone = new G4Cons("caps_cone",0.,caps_con_r2,0.,caps_con_r1,caps_con_half,0.,CLHEP::twopi);

  G4VSolid* caps_1 = new G4UnionSolid("caps_1",caps_cyl,caps_sphere,0,G4ThreeVector(0.,0.,caps_c_half));
  G4VSolid* caps_vol = new G4UnionSolid("caps_2",caps_1,caps_cone,0,G4ThreeVector(0.,0.,-1.*(caps_c_half+caps_con_half)));

  caps_log = new G4LogicalVolume(caps_vol,mat_Steel,"caps_log");
  G4VisAttributes* caps_att = new G4VisAttributes(G4Colour(1,1,0,0.5));
  caps_att->SetVisibility(true);
  caps_att->SetForceSolid(true);
  caps_log->SetVisAttributes(caps_att);

  // Steel cable
  G4Tubs* cable_vol = new G4Tubs("cable_vol",0.,caps_con_r2,cable_half,0.,CLHEP::twopi);
  cable_log = new G4LogicalVolume(cable_vol,mat_Cable,"cable_log");
  G4VisAttributes* cable_att = new G4VisAttributes(G4Colour(1,1,1,0.5));
  cable_att->SetVisibility(true);
  cable_att->SetForceSolid(true);
  cable_log->SetVisAttributes(cable_att);

  G4UnionSolid* shell = new G4UnionSolid("shell",caps_vol,cable_vol,0,G4ThreeVector(0.,0.,-1.*cable_pos));
  VSeed::OuterShell = shell;
}

void MicroSelectronV2Ir192::PlaceYourself(G4VPhysicalVolume* parent,G4int copyNo) {
  if ( !VSeed::OuterShell ) Construct();
  G4PVPlacement* caps_phys = new G4PVPlacement(0,G4ThreeVector(0.,0.,-phase),"caps_phys",caps_log,parent,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,phase),"core_phys",core_log,caps_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,-1.*(cable_pos+phase)),"cable_phys",cable_log,parent,false,copyNo);
}

G4ThreeVector MicroSelectronV2Ir192::GetEmissionPosition() {
  G4double rho,phi=CLHEP::twopi*G4UniformRand(),z;
  G4double slope = (2*source_cons_rmax2-source_c1_radius)/(2*source_cons_half);
  while (true) {
    z = VSeed::ActiveLength*(G4UniformRand()-0.5);
    rho = source_c1_radius*sqrt(G4UniformRand());
    if ( fabs(z) < source_c1_half ) break;
    else if ( rho < source_c1_radius + slope*(fabs(z)-source_c1_half) ) break;
  }
  return G4ThreeVector(rho*cos(phi),rho*sin(phi),z);
}
