name := $(shell basename $(PWD))

G4TARGET := $(name)
G4EXLIB := true
G4_NO_VERBOSE := true
CPPFLAGS += $(shell root-config --cflags)

ifeq ($(G4SYSTEM),Darwin-g++)
  EXTRALIBS := $(shell root-config --ldflags) $(shell root-config --libs)
else
  EXTRALIBS := -Wl $(ROOTGLIBS) $(shell root-config --ldflags) $(shell root-config --libs)
endif

ifdef PHASESPACE
CPPFLAGS += -DPHASESPACE
endif
ifdef VIS
CPPFLAGS += -DVIS
endif

.PHONY: all
all: lib bin

include $(G4INSTALL)/config/binmake.gmk
