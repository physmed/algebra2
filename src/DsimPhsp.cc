#include <stdlib.h>     /* getenv */

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4Region.hh"
#include "G4ProductionCuts.hh"

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

#include "G4ios.hh"
#include "G4StepPoint.hh"
#include "G4ParticleDefinition.hh"
#include "G4TrackStatus.hh"
#include "G4ParticleDefinition.hh"
#include "G4Gamma.hh"
#include "G4Geantino.hh"

#include "G4String.hh"

#include "FlexiSourceIr192.hh"
#include "MicroSelectronV2Ir192.hh"
#include "OncoSeed6711I125.hh"
#include "SelectSeedI125.hh"
#include "TheraSeed200Pd103.hh"
#include "ProstaSeedSLI125.hh"
#include "ProsperaSeed3631I125.hh"
#include "MBDCA_WGIr192.hh"
#include "GammaPlusPDRIr192.hh"

#include "DsimPhsp.hh"

DsimPhsp::DsimPhsp(G4int thread1, G4String model)
{
  thread = thread1;
  sourceTag=model;
  fBoxSize = 5*cm;
}

DsimPhsp::~DsimPhsp()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DsimPhsp::Initialize(){
    
	
	G4String fn = "init/phsp_init.txt";
	fstream thefile(Form("%s/init/phsp_init.txt",getenv("ALGEBRADIR")),ios::in);
	if( !thefile.is_open() ) { 
		G4cout << "Warning: Parameters file <"<<fn<<"> could not be opened." << G4endl; 
		G4cout << "         Default values are used." <<G4endl;
	}else{
		G4cout << "Reading Parameters From : <"<<fn<<">" << G4endl;
		G4String dummy;
		thefile>>dummy>>fBoxSize;
		fBoxSize=fBoxSize*CLHEP::mm;
	}
    
    fMaterial=G4NistManager::Instance()->FindOrBuildMaterial("G4_WATER");
    
    if (sourceTag=="FlexiSourceIr192")             fSrc=new FlexiSourceIr192;
    else if (sourceTag=="MicroSelectronV2Ir192")   fSrc=new MicroSelectronV2Ir192;
    else if (sourceTag=="OncoSeed6711I125")        fSrc=new OncoSeed6711I125;
    else if (sourceTag=="SelectSeedI125")          fSrc=new SelectSeedI125;
    else if (sourceTag=="TheraSeed200Pd103")       fSrc=new TheraSeed200Pd103;
    else if (sourceTag=="ProstaSeedSLI125")        fSrc=new ProstaSeedSLI125;
    else if (sourceTag=="ProsperaSeed3631I125")    fSrc=new ProsperaSeed3631I125;  
    else if (sourceTag=="MBDCA_WGIr192")           fSrc=new MBDCA_WGIr192;  
    else if (sourceTag=="GammaPlusPDRIr192")       fSrc=new GammaPlusPDRIr192;  
    else{
      G4cout<<"Error: Wrong Source Name"<<G4endl;
      G4cout<<"  Implemented models: "<<G4endl;
      G4cout<<"          FlexiSourceIr192"     <<G4endl;
      G4cout<<"          MicroSelectronV2Ir192"<<G4endl;
      G4cout<<"          OncoSeed6711I125"     <<G4endl;
      G4cout<<"          SelectSeedI125"       <<G4endl;
      G4cout<<"          TheraSeed200Pd103"    <<G4endl;
      G4cout<<"          ProstaSeedSLI125"     <<G4endl;
      G4cout<<"          ProsperaSeed3631I125" <<G4endl;
      G4cout<<"          GammaPlusPDRIr192"        <<G4endl;
      G4cout<<"          MBDCA_WGIr192"        <<G4endl;
      exit(1);
    }
    
    //fSrc->Construct();  // <---- This line is not necessary
    
    /*
    isHDR = false;
    if ( fSrc->GetName().contains("192") ) isHDR = true;
    Z = 53; A = 125;
    if ( isHDR ) { Z = 77; A = 192; }
    else if ( fSrc->GetName().contains("103") ) { Z = 46; A = 103; } 
    */
  
    
  G4String filename = Form("%s/%s_%d.root",getenv("PHSPDIR"),sourceTag.c_str(),thread);
  G4cout<<"Creating <"<<filename<<">"<<G4endl;
  
  fParticleGun = new G4ParticleGun(1);
  fParticleGun->SetParticleEnergy(0*eV);
  fParticleGun->SetParticlePosition(G4ThreeVector());
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));

  f = new TFile(filename,"recreate");
  t = new TTree("phase","PS");
  t->Branch("x",&x,"x/F");
  t->Branch("y",&y,"y/F");
  t->Branch("z",&z,"z/F");
  t->Branch("theta",&theta,"theta/F");
  t->Branch("phi",&phi,"phi/F");
  t->Branch("E",&E,"E/F");
  
  G4cout<<"{{{{{{{{{{{{{{{{{{{{{{{{{{{"<<G4endl;
}


G4VPhysicalVolume* DsimPhsp::Construct()
{
  // Cleanup old geometry
  G4GeometryManager::GetInstance()->OpenGeometry();
  G4PhysicalVolumeStore::GetInstance()->Clean();
  G4LogicalVolumeStore::GetInstance()->Clean();
  G4SolidStore::GetInstance()->Clean();

  ///////////////////////////////////////////////////////////////////////////////////
  // World
  G4Box* sWorld = new G4Box("sldWorld",fBoxSize/2*1.1,fBoxSize/2*1.1,fBoxSize/2*1.1);
  G4LogicalVolume* lWorld = new G4LogicalVolume(sWorld,fMaterial,"logWorld");   
  fWorld = new G4PVPlacement(0,                                //no rotation
                    G4ThreeVector(),                  //at (0,0,0)
                    lWorld,                           //its logical volume
                    "hall_phys",                      //its name
                    0,                                //its mother  volume
                    false,                            //no boolean operation
                    0);                               //copy number
  
  ///////////////////////////////////////////////////////////////////////////////////
  // Source Model
  fSrc->PlaceYourself(fWorld,0);
  
  ///////////////////////////////////////////////////////////////////////////////////
  return fWorld;
}


void DsimPhsp::GeneratePrimaries(G4Event* anEvent)
{
  //if ( fParticleGun->GetParticleDefinition()==G4Geantino::Geantino() ) {
  //  fParticleGun->SetParticleDefinition( G4ParticleTable::GetParticleTable()->GetIon(Z,A,0.*CLHEP::keV) );
  //  fParticleGun->SetParticleCharge(0.*CLHEP::eplus);
  //}
  fParticleGun->SetParticleDefinition( G4Gamma::Gamma() );
  fParticleGun->SetParticlePosition( fSrc->GetEmissionPosition() );
  fParticleGun->SetParticleMomentumDirection( fSrc->GetEmissionDirection() );
  fParticleGun->SetParticleEnergy( fSrc->GetEmissionEnergy() );
  
  fParticleGun->GeneratePrimaryVertex(anEvent); 
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "G4Track.hh"
#include "G4SteppingManager.hh"
#include "G4Step.hh"

void DsimPhsp::UserSteppingAction(const G4Step* aStep)
{
  G4Track *tr = aStep->GetTrack();
  G4String volumeName = tr->GetVolume()->GetName();
	
  if ( volumeName=="caps_phys" || volumeName=="cable_phys" ) {
	  if ( tr->GetNextVolume()->GetName()=="hall_phys" ) {
	    //G4cout<<"+++";
		  tr->SetTrackStatus(fStopAndKill);
		  if ( tr->GetParticleDefinition()->GetParticleName()=="gamma" ) {
			  G4StepPoint* postStepPoint = aStep->GetPostStepPoint();
			  E = postStepPoint->GetTotalEnergy()/CLHEP::MeV;
			  if ( E > 1e-3 ) { //????????????
				  G4ThreeVector pos = postStepPoint->GetPosition();
				  x = pos[0]/CLHEP::mm;
				  y = pos[1]/CLHEP::mm;
				  z = pos[2]/CLHEP::mm;
				  G4ThreeVector mom = postStepPoint->GetMomentum();
				  theta = mom.getTheta();
				  phi = mom.getPhi();
				  t->Fill();
				  //G4cout<<"...";
			  }
		  }
	  }
	  return;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "G4Event.hh"

void DsimPhsp::BeginOfEventAction(const G4Event* anEvent) {
    anEvent->GetEventID();
}

void DsimPhsp::EndOfEventAction(const G4Event* anEvent) {
  G4int evtID=anEvent->GetEventID();
  if (evtID%10000==0) G4cout<<" #/#   "<<evtID<<"   "<<G4endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "G4Track.hh"

G4ClassificationOfNewTrack DsimPhsp::ClassifyNewTrack(const G4Track* aTrack) {
  G4ParticleDefinition* part = aTrack->GetDefinition();
  G4int ZZ = part->GetAtomicNumber();
  if ( ZZ==46 || ZZ==52 || ZZ==53 || ZZ==76 || ZZ==78 ) { // Pd(46,103) I(53,125) Os(76,192) Pt(77,192) Te(52,125)
    part->SetPDGLifeTime(0.0);
    part->SetPDGStable(false);
  }

  G4String particleName = part->GetParticleName();
       if ( particleName=="nu_e" ) return fKill;
  else if ( particleName=="anti_nu_e" ) return fKill;
  else if ( particleName=="Pt192[0.0]" ) return fKill;
  else if ( particleName=="Os192[0.0]" ) return fKill;
  else if ( particleName=="Te125[0.0]" ) return fKill;
  return fUrgent;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "G4Run.hh"

void DsimPhsp::BeginOfRunAction(const G4Run*){}

void DsimPhsp::EndOfRunAction(const G4Run* ){
  t->Write();
  f->Close();
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
