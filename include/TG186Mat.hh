#ifndef TG186Mat_hh
#define TG186Mat_hh

using namespace std;
class G4Material;

class TG186Mat {
public :
  TG186Mat();
  ~TG186Mat(){};

  G4int GetNumberOfMaterials();
  G4int CreateBaseMaterial(G4String);
  vector<G4Material*> MatList;
  
  void CompMuenMu();

private :
  G4int nMaterials;

};

#endif
