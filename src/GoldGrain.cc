#include "GoldGrain.hh"
#include "G4Tubs.hh"
#include "G4VisAttributes.hh"
#include "G4PVPlacement.hh"
#include "G4VPhysicalVolume.hh"
#include "G4NistManager.hh"
#include "G4LogicalVolume.hh"

GoldGrain::GoldGrain() {
  GoldGrainName = "CPmedical";

  OutRadius = 6*CLHEP::mm; // 0.6*mm;
  ZHalfLength = 6*CLHEP::mm; //1.5*mm;  

  // CPmedical gold grains model
  G4Tubs* grain_phys = new G4Tubs("grain_phys",0.,OutRadius,ZHalfLength,0.,CLHEP::twopi);
  grain_log = new G4LogicalVolume(grain_phys,G4NistManager::Instance()->FindOrBuildMaterial("G4_Au"),"grain_log",0,0,0);
  G4VisAttributes* grain_att = new G4VisAttributes(G4Colour(1,1,0.2,0.5));
  grain_att->SetVisibility(true);
  grain_att->SetForceSolid(true);
  grain_log->SetVisAttributes(grain_att);

}

void GoldGrain::PlaceYourself(G4VPhysicalVolume* parent,G4int copyNo) {
  new G4PVPlacement(0,G4ThreeVector(),"grain_phys",grain_log,parent,false,copyNo);
}
