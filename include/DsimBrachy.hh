#include "globals.hh"
#include <iostream>
#include "G4TrackStatus.hh"
#include "G4UserStackingAction.hh"
#include "G4ClassificationOfNewTrack.hh"

#include <vector>
#include <map>

#include "CtrBase.hh"

using namespace std;

#ifndef DsimBrachy_H
#define DsimBrachy_H 1

class G4PhysicalVolume;

class G4Step;
class G4Track;
class G4Run;
class G4Event;

class DsimDwellList;
class DsimPhspace;
class DsimPhantomReader;

class G4VPhysicalVolume;
class G4LogicalVolume;

class TH3D;

class LinearTrackLengthEstimator;

class DsimBrachy: public CtrBase
{
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
public:
    DsimBrachy(G4int thread1, G4String model);
    ~DsimBrachy();

    void Initialize();
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

private:
	
	G4double       world_size_mm;
	G4double       box_size_mm;
	G4String       box_mat_str;
  
  G4int          thread;
  G4String       sourceTag;
  
  G4Material*    fMaterial; // container box material
  
  DsimDwellList* dwells;
  DsimPhspace*   phsp;
  DsimPhantomReader*   phantom;
  LinearTrackLengthEstimator* LTE;
  vector< pair<G4int,G4double> >::iterator it,it_end;
  map<G4int,G4double>::iterator it2,it_end2;
  pair<map<G4int,double>::iterator,bool> ret;
  map<G4int,G4double> Edep;
  
  TH3D*          EDEP00;
	
  G4VPhysicalVolume* hall_phys;
  G4LogicalVolume* box_log;
  G4VPhysicalVolume* box_phys;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
	
public:

    virtual G4VPhysicalVolume* Construct();   
	virtual void GeneratePrimaries(G4Event*);
	//
	//virtual void ConstructParticle();
    //virtual void ConstructProcess();
    //virtual void SetCuts();

    virtual void UserSteppingAction(const G4Step*);    
    
    //virtual void PreUserTrackingAction(const G4Track*);
    //virtual void PostUserTrackingAction(const G4Track*);
    
    virtual G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*);
    
    virtual void BeginOfEventAction(const G4Event*);
    virtual void EndOfEventAction(const G4Event*);

    virtual void BeginOfRunAction(const G4Run*);
    virtual void   EndOfRunAction(const G4Run*);


  
};

#endif
