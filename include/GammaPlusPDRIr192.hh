#ifndef GammaPlusPDRIr192_hh
#define GammaPlusPDRIr192_hh

#include "VSeed.hh"

class G4LogicalVolume;

class GammaPlusPDRIr192: public VSeed {
public:
  GammaPlusPDRIr192();
  ~GammaPlusPDRIr192(){;}
 
  void Construct();
  void PlaceYourself(G4VPhysicalVolume*,G4int);
  G4ThreeVector GetEmissionPosition();
  
private:
  static G4double energy[34];
  static G4double CDF[34];
  static G4double a[7];
  static G4double R[2];

  G4double active_cl_radius;
  G4double active_cl_half;  
  //
  G4double cyln_al_radius;
  G4double cyln_al_half;   // aluminium cylinder
  //
  G4double cap_steel_r1;
  G4double cap_steel_r2;
  G4double cap_steel_half;  // h=0.12
  //
  G4double cyln1_steel_radius;
  G4double cyln1_steel_half;   // h=0.3
  //
  G4double cyln2_steel_radius;
  G4double cyln2_steel_radius_inner;
  G4double cyln2_steel_half;   // h=1.4 mm
  //
  G4double cyln3_steel_radius;
  G4double cyln3_steel_half;   // h=0.5 mm
  // cable
  G4double cable_steel_radius;
  G4double cable_steel_half;   // h=2.0 mm


  G4LogicalVolume* active_cyl_log;
  G4LogicalVolume* al_cyl_log;
  G4LogicalVolume* cyln1_steel_log;
  G4LogicalVolume* cyln2_steel_log;
  G4LogicalVolume* cyln3_steel_log;
  G4LogicalVolume* cap_steel_log;
  //G4LogicalVolume* capsul_steel_log;
  G4LogicalVolume* cable_steel_log;


};

#endif
