# ALGEBRA2 #
A Geant4-based Brachytherapy dose simulation tool.

Note:
- ALGEBRA2 is a new version of ALGEBRA (https://bitbucket.org/ccsri/algebra) for Geant4.10 and later.
- The previous version of ALGEBRA (https://bitbucket.org/ccsri/algebra) works well with Geant4.9.x.

### What is ALGEBRA for? ###

* Currently, it simulates the dose of brachytherapy treatment. And
* Prepare phasespace files.

### Prerequisites ###

* Geant4.10.00.p02 installed with CMake [[1]](http://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/InstallationGuide/BackupVersions/V9.4/html/ch04s02.html)
* [Root](http://root.cern.ch/drupal/).5.34 installed with CMake; as part of Root,  PyROOT is required also.
* Python 2.7, (python 3 is currently not supported)

Note:
It was observed wired behaviour of python with ROOT if the version of python you run is not the same version of the python you used to compile ROOT/Geant4. 

```
#!python

>>> import ROOT
Fatal Python error: PyThreadState_Get: no current thread
Abort trap: 6 
```
    


### Installation ###

* Go to the folder of ALGEBRA: 


```
#!bash

    $ cd path/to/ALGEBRA
```


* Type: 


```
#!bash

    $ source algebra_build.sh
```


### Execution ###

### Step 1. Generate Phasespace File(s) ###

```
#!bash

    $ python phsp.py MODEL NUMBER-OF-PARTICLE

    (ex. $ python phsp SelectSeedI125 1000000)
```

The following seeds are currently implemented:

* FlexiSourceIr192

* MicroSelectronV2Ir192

* OncoSeed6711I125

* ProsperaSeed3631I125

* ProstaSeedSLI125

* SelectSeedI125

### Step 2. Dose Simulation ###

* Type the following to generate edep files

```
#!bash

    $ python thread.py path/to/project   

    (ex., python thread.py simulations/simple)
```

* Type the following  to generate dose files

    
```
#!bash

    $ python post_thread.py path/to/project
```


Note: path/to/project is the folder that contains three prerequisites:

*  dwells.txt                     -- dwell position/time/direction
*  phantom.egsphant      -- phantom
*  run.conf                       -- contains simulation parameters

### Questions? ###

...Email to: yunzhi7@yahoo.com