#include "DsimDwellList.hh"
#include <fstream>

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

#include "Randomize.hh"
#include "G4String.hh"

DsimDwellList::DsimDwellList(G4String filename){
  fFilename=filename;
  fstream thefile(filename,ios::in);
  if( !thefile.is_open() ) { 
    G4cout << "Error: Dwells file <"<<filename<<"> could not be opened." << G4endl; 
    exit(1); 
  }else{
    G4cout << "Reading Dwells : <"<<filename<<">" << G4endl;
  }
  
  G4double x,y,z,u,v,w,t;
  while (thefile>>x>>y>>z>>t>>u>>v>>w)
  {
    fVectX.push_back(x*CLHEP::mm);
    fVectY.push_back(y*CLHEP::mm);
    fVectZ.push_back(z*CLHEP::mm);
    fVectU.push_back(u);
    fVectV.push_back(v);
    fVectW.push_back(w);
    fVectT.push_back(t);
    if (fWeights.size()==0) fWeights.push_back(t);
    else                    fWeights.push_back(fWeights[fWeights.size()-1]+t);
  }
  
  for (size_t i=0;i<fWeights.size();i++){
    fWeights[i]=fWeights[i]/fWeights[fWeights.size()-1];
  }
}

DsimDwellList::~DsimDwellList(){
}


void DsimDwellList::Dump()
{
  for (size_t i=0;i<fVectT.size();i++){
    G4cout<<"> "<<i<<", pos=("<<fVectX[i]/CLHEP::mm<<"*mm,"<<fVectY[i]/CLHEP::mm<<"*mm,"<<fVectZ[i]/CLHEP::mm<<"*mm)"
                   <<", dir=("<<fVectU[i]<<","<<fVectV[i]<<","<<fVectW[i]<<")"
	           <<", t="<<fVectT[i]<<", wt="<<fWeights[i]<<G4endl;
  }
}


G4int DsimDwellList::Select(G4double &x,G4double &y,G4double &z,G4double &u,G4double &v,G4double &w)
{
  G4double rrr=G4UniformRand();
  G4int idx;
  for (size_t i=0;i<fWeights.size();i++){
    if (rrr<fWeights[i]){
      idx=i;
      break;
    }
  }
  x=fVectX[idx];y=fVectY[idx];z=fVectZ[idx];
  u=fVectU[idx];v=fVectV[idx];w=fVectW[idx];
  return idx;
}
