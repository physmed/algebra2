#include "G4PhantomParameterisation.hh"
#include "G4Event.hh"

#include "DsimEventAction.hh"

DsimEventAction::DsimEventAction(CtrBase* aCtr) {Ctr=aCtr;}
DsimEventAction::~DsimEventAction() {}

void DsimEventAction::BeginOfEventAction(const G4Event* anEvent) {
  Ctr->BeginOfEventAction(anEvent);
}

void DsimEventAction::EndOfEventAction(const G4Event* anEvent) {
  Ctr->EndOfEventAction(anEvent);
}
