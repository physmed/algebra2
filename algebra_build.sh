#!/bin/sh

printf 'In folder: %s\n' "${PWD##*/}"
##source setenv.sh
export ALGEBRADIR=$(pwd)
mkdir Build
cd Build
cmake -j12 -DGeant4_DIR=${G4LIB} -DCMAKE_INSTALL_PREFIX=$ALGEBRADIR ..
make 
make install
cd ${ALGEBRADIR}

