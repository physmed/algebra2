#include "globals.hh"
#include <iostream>
#include "G4TrackStatus.hh"
#include "G4UserStackingAction.hh"
#include "G4ClassificationOfNewTrack.hh"
#include "G4ParticleGun.hh"
#include "G4Event.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "G4VUserDetectorConstruction.hh"
#include "ParallelWorldConstruction.hh"

#ifndef CtrBase_H
#define CtrBase_H 1

class G4Step;
class G4Track;
class G4Run;
class G4Event;

class G4VPhysicalVolume;

class CtrBase
{
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
public:
    CtrBase(){};
    ~CtrBase(){};

    virtual void Initialize()=0;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

public:

	///////////////////////////////////////////////////////////////////////
    virtual G4VPhysicalVolume* Construct()=0;
	virtual void GeneratePrimaries(G4Event*)=0;
	//
	//virtual void ConstructParticle()=0;
    //virtual void ConstructProcess()=0;
    //virtual void SetCuts()=0;

    ///////////////////////////////////////////////////////////////////////
    virtual void UserSteppingAction(const G4Step*)=0;

	///////////////////////////////////////////////////////////////////////
    //virtual void PreUserTrackingAction(const G4Track*)=0;
    //virtual void PostUserTrackingAction(const G4Track*)=0;

	///////////////////////////////////////////////////////////////////////
    virtual G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*)=0;

	///////////////////////////////////////////////////////////////////////
    virtual void BeginOfEventAction(const G4Event*)=0;
    virtual void EndOfEventAction(const G4Event*)=0;

	///////////////////////////////////////////////////////////////////////
    virtual void BeginOfRunAction(const G4Run*)=0;
    virtual void   EndOfRunAction(const G4Run*)=0;

    void setup_parallel() {
      if (fParallelBool) {
        G4String paraWorldName = "ParallelWorld";
        fDet->RegisterParallelWorld( new ParallelWorldConstruction(paraWorldName) );
      };
    }

private:

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

public:
	G4ParticleGun*               fParticleGun;
  G4VUserDetectorConstruction* fDet;
  G4bool                       fParallelBool;

};

#endif
