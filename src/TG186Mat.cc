#include "G4Material.hh"
#include "G4NistManager.hh"
#include "TG186Mat.hh"
#include "LinearTrackLengthEstimator.hh"
#include <vector>
#include <iostream>     // std::cout, std::fixed
#include <iomanip>      // std::setprecision

TG186Mat::TG186Mat() {
  nMaterials = 0;
}

G4int TG186Mat::GetNumberOfMaterials() { return nMaterials; }

void TG186Mat::CompMuenMu() {

  // energy values for Ir192
  float ee[34]={0.061, 0.063, 0.065, 0.067, 0.071, 0.076, 0.136, 0.177, 0.201, 0.206, 0.215, 0.280, 0.283, 0.296, 
	        0.308, 0.315, 0.317, 0.374, 0.415, 0.416, 0.420, 0.468, 0.477, 0.485, 0.485, 0.489, 0.589, 0.593, 
	        0.604, 0.613, 0.885, 1.062, 1.089, 1.378};
	        
  std::vector<G4String> names;
  names.push_back("Air");
  names.push_back("Water");
  names.push_back("Prostate");
  names.push_back("Mean_adipose");
  names.push_back("Mean_gland");
  names.push_back("Mean_male_soft_tissue");
  names.push_back("Mean_female_soft_tissue");
  names.push_back("Mean_skin");
  names.push_back("Cortical_bone");
  names.push_back("Femur_bone");
  names.push_back("Eye_lens");
  names.push_back("Lung");
  names.push_back("Liver");
  names.push_back("Heart");
//   names.push_back("Air");
//   names.push_back("Air");
//   names.push_back("Air");
//   names.push_back("Air");
//   names.push_back("Air");
//   names.push_back("Air");
//   names.push_back("Air");
//   names.push_back("Air");
//   names.push_back("Air");

   G4Material* mat;
   LinearTrackLengthEstimator *T = new LinearTrackLengthEstimator();
   for (size_t i=0;i<names.size();i++) {
        CreateBaseMaterial(names[i]);
        mat=MatList[i];
        //for (int k=0;k<34;k++)  G4cout<<ee[k]<<"  "<<T->MuRho(mat,ee[k])<<"  "<<T->MuenRho(mat,ee[k])<<"  "<<names[i]<<":"<<i<<G4endl;
        for (int k=0;k<34;k++)  G4cout<<fixed<<std::setprecision(6)<<"mu["<<i*34+k<<"]="<<T->MuRho(mat,ee[k])<<";  muen["<<i*34+k<<"]="<<T->MuenRho(mat,ee[k])<<";"<<G4endl;
    }
    
    for (size_t i=0;i<names.size();i++) {
        mat=MatList[i];
        G4cout<<"    rhopp["<<i<<"]="<<mat->GetDensity()/(CLHEP::g/CLHEP::cm3)<<"; // "<<names[i]<<G4endl;
    }
       
    
}

G4int TG186Mat::CreateBaseMaterial(G4String MaterialName) {
  // Definition of basic variables
  G4double density;
  G4Material* mat;
  G4int nel;

  G4NistManager* man = G4NistManager::Instance();
  G4Element* elH  = man->FindOrBuildElement( 1);
  G4Element* elC  = man->FindOrBuildElement( 6);
  G4Element* elN  = man->FindOrBuildElement( 7);
  G4Element* elO  = man->FindOrBuildElement( 8);
  G4Element* elNa = man->FindOrBuildElement(11);
  G4Element* elMg = man->FindOrBuildElement(12);
  G4Element* elP  = man->FindOrBuildElement(15);
  G4Element* elS  = man->FindOrBuildElement(16);
  G4Element* elCl = man->FindOrBuildElement(17);
  G4Element* elAr = man->FindOrBuildElement(18);
  G4Element* elK  = man->FindOrBuildElement(19);
  G4Element* elCa = man->FindOrBuildElement(20);
  G4Element* elI  = man->FindOrBuildElement(53);

  // Air (from NIST, dry, near see level, see Attix)
  if ( MaterialName == "Air" ) {
	  density = 0.001205;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=4);
    mat->AddElement(elC, 0.000124);
    mat->AddElement(elN, 0.755268);
    mat->AddElement(elO, 0.231781);
    mat->AddElement(elAr,0.012827);
    MatList.push_back(mat);

  // Water
  } else if ( MaterialName == "Water" ) {
    density = 1.;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=2);
    mat->AddElement(elH,nel=2);
    mat->AddElement(elO,nel=1);
    MatList.push_back(mat);

  // Prostate
  } else if ( MaterialName == "Prostate" ) {
    density = 1.04;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=8);
    mat->AddElement(elH, 0.105);
    mat->AddElement(elC, 0.089);
    mat->AddElement(elN, 0.025);
    mat->AddElement(elO, 0.774);
    mat->AddElement(elNa,0.002);
    mat->AddElement(elP, 0.001);
    mat->AddElement(elS, 0.002);
    mat->AddElement(elK, 0.002);
    MatList.push_back(mat);

  // Mean adipose
  } else if ( MaterialName == "Mean_adipose" ) {
    density = 0.95;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=7);
    mat->AddElement(elH, 0.114);
    mat->AddElement(elC, 0.598);
    mat->AddElement(elN, 0.007);
    mat->AddElement(elO, 0.278);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elS, 0.001);
    mat->AddElement(elCl,0.001);
    MatList.push_back(mat);

  // Mean gland
  } else if ( MaterialName == "Mean_gland" ) {
    density = 1.02;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=8);
    mat->AddElement(elH, 0.106);
    mat->AddElement(elC, 0.332);
    mat->AddElement(elN, 0.030);
    mat->AddElement(elO, 0.527);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.001);
    mat->AddElement(elS, 0.002);
    mat->AddElement(elCl,0.001);
    MatList.push_back(mat);

  // Mean male soft tissue
  } else if ( MaterialName == "Mean_male_soft_tissue" ) {
    density = 1.03;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.105);
    mat->AddElement(elC, 0.256);
    mat->AddElement(elN, 0.027);
    mat->AddElement(elO, 0.602);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.002);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.002);
    mat->AddElement(elK, 0.002);
    MatList.push_back(mat);
  

  // Mean female soft tissue
  } else if ( MaterialName == "Mean_female_soft_tissue" ) {
    density = 1.02;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.106);
    mat->AddElement(elC, 0.315);
    mat->AddElement(elN, 0.024);
    mat->AddElement(elO, 0.547);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.002);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.001);
    mat->AddElement(elK, 0.002);
    MatList.push_back(mat);

  // Mean skin
  } else if ( MaterialName == "Mean_skin" ) {
    density = 1.09;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.100);
    mat->AddElement(elC, 0.204);
    mat->AddElement(elN, 0.042);
    mat->AddElement(elO, 0.645);
    mat->AddElement(elNa,0.002);
    mat->AddElement(elP, 0.001);
    mat->AddElement(elS, 0.002);
    mat->AddElement(elCl,0.003);
    mat->AddElement(elK, 0.001);

    MatList.push_back(mat);

  // Cortical bone
  } else if ( MaterialName == "Cortical_bone" ) {
    density = 1.92;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.034);
    mat->AddElement(elC, 0.155);
    mat->AddElement(elN, 0.042);
    mat->AddElement(elO, 0.435);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elMg,0.002);
    mat->AddElement(elP, 0.103);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCa,0.225);
    MatList.push_back(mat);


  // Femur bone
  } else if ( MaterialName == "Femur_bone" ) {
    density = 1.92;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.034);
    mat->AddElement(elC, 0.155);
    mat->AddElement(elN, 0.042);
    mat->AddElement(elO, 0.435);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elMg,0.002);
    mat->AddElement(elP, 0.103);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCa,0.225);
    MatList.push_back(mat);

  // Eye lens
  } else if ( MaterialName == "Eye_lens" ) {
    density = 1.07;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=8);
    mat->AddElement(elH, 0.096);
    mat->AddElement(elC, 0.195);
    mat->AddElement(elN, 0.057);
    mat->AddElement(elO, 0.646);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.001);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.001);
    MatList.push_back(mat);

  // Lung (inflated)
  } else if ( MaterialName == "Lung" ) {
    density = 0.26;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.103);
    mat->AddElement(elC, 0.105);
    mat->AddElement(elN, 0.031);
    mat->AddElement(elO, 0.749);
    mat->AddElement(elNa,0.002);
    mat->AddElement(elP, 0.002);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.003);
    mat->AddElement(elK, 0.002);
    MatList.push_back(mat);

  // Liver
  } else if ( MaterialName == "Liver" ) {
    density = 1.06;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.102);
    mat->AddElement(elC, 0.139);
    mat->AddElement(elN, 0.030);
    mat->AddElement(elO, 0.716);
    mat->AddElement(elNa,0.002);
    mat->AddElement(elP, 0.003);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.002);
    mat->AddElement(elK, 0.003);
    MatList.push_back(mat);

  // Heart
  } else if ( MaterialName == "Heart" ) {
    density = 1.05;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.104);
    mat->AddElement(elC, 0.139);
    mat->AddElement(elN, 0.029);
    mat->AddElement(elO, 0.718);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.002);
    mat->AddElement(elS, 0.002);
    mat->AddElement(elCl,0.002);
    mat->AddElement(elK, 0.003);
    MatList.push_back(mat);

  // ContrastIodine
  } else if ( MaterialName == "ContrastIodine5%" ) {
    density = 1.0203;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0191);
    mat->AddElement(elH,0.1064);
    mat->AddElement(elI,0.0319);
    mat->AddElement(elN,0.0035);
    mat->AddElement(elO,0.8390);
    MatList.push_back(mat);

  } else if ( MaterialName == "ContrastIodine10%" ) {
    density = 1.0406;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0375);
    mat->AddElement(elH,0.1011);
    mat->AddElement(elI,0.0626);
    mat->AddElement(elN,0.0069);
    mat->AddElement(elO,0.7918);
    MatList.push_back(mat);

  } else if ( MaterialName == "ContrastIodine15%" ) {
    density = 1.0609;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0552);
    mat->AddElement(elH,0.0960);
    mat->AddElement(elI,0.0922);
    mat->AddElement(elN,0.0102);
    mat->AddElement(elO,0.7464);
    MatList.push_back(mat);

  } else if ( MaterialName == "ContrastIodine20%" ) {
    density = 1.0812;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0556);
    mat->AddElement(elH,0.0911);
    mat->AddElement(elI,0.1206);
    mat->AddElement(elN,0.0133);
    mat->AddElement(elO,0.7027);
    MatList.push_back(mat);

  } else if ( MaterialName == "ContrastIodine25%" ) {
    density = 1.1015;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0887);
    mat->AddElement(elH,0.0864);
    mat->AddElement(elI,0.1480);
    mat->AddElement(elN,0.0163);
    mat->AddElement(elO,0.6607);
    MatList.push_back(mat);

  // Calcification
  } else if ( MaterialName == "Calcification" ) {
    density = 3.06;
    mat = new G4Material(MaterialName,density*CLHEP::g/CLHEP::cm3,nel=6);
    mat->AddElement(elH, 0.003);
    mat->AddElement(elC, 0.016);
    mat->AddElement(elN, 0.005);
    mat->AddElement(elO, 0.407);
    mat->AddElement(elP, 0.187);
    mat->AddElement(elCa,0.382);
    MatList.push_back(mat);

  // Stainless steel
  } else if ( MaterialName == "Stainless_steel" ) {
    density = 7.98;
    mat = new G4Material("Stainless_steel",density*CLHEP::g/CLHEP::cm3,nel=7);
    mat->AddElement(man->FindOrBuildElement(26),0.6197); // Fe
    mat->AddElement(man->FindOrBuildElement(24),0.1800); // Cr
    mat->AddElement(man->FindOrBuildElement(28),0.1400); // Ni
    mat->AddElement(man->FindOrBuildElement( 6),0.0003); // C
    mat->AddElement(man->FindOrBuildElement(42),0.0300); // Mo
    mat->AddElement(man->FindOrBuildElement(25),0.0200); // Mn
    mat->AddElement(man->FindOrBuildElement(14),0.0100); // Si
    MatList.push_back(mat);

  } else {
    cerr << "TG186Mat; no match for : " << MaterialName << endl;
    return 0;
  }
  return 1;
}
