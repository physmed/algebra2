#!/usr/bin/env python
import os,sys,time
from random import randint
from ROOT import TFile,TCanvas,TLegend,TH1F,gStyle,TH3D,TObject
from math import log,exp
import numpy as np
import datetime

from threading import Thread
import subprocess as sp
from multiprocessing import cpu_count
nthreads = cpu_count()

DefaultStats = {'MicroSelectronV2Ir192':5,
        'FlexiSourceIr192':5,
        'SelectSeedI125':10,
        'OncoSeed6711I125':10,
        'ProsperaSeed3631I125':10,
        'ProstaSeedSLI125':10,
        'TheraSeed200Pd103':10}
# ==================================================================
# check ALGEBRADIR
# ==================================================================
if 'ALGEBRADIR' not in os.environ:
  print 'Error: ALGEBRADIR not set yet'
  print 'Stopped. Nothing done!'
  print 'Run the following to set ALGEBRADIR:'
  print '$ source setenv.sh'
  exit(0)
  
# ==================================================================
# check command line
# ==================================================================
if len(sys.argv)!=3 or sys.argv[1]=='-h':
  print 'Usage:'
  print '$ python phsp.py MODEL nDECAY'
  print 'Implemented models:'
  for key in DefaultStats.keys():
    print '   '+key
  exit(0)
  
# ==================================================================
# Configuration
# ==================================================================
i0 = 0
threads = range(i0,i0+nthreads)
Processes = []
t1=datetime.datetime.now()

# fetch config from arguments
MODEL = sys.argv[1]
if len(sys.argv)>2: nPhotons = int(sys.argv[2])
else: nPhotons = DefaultStats[MODEL]*1e6

# ==================================================================
# Threads
# ==================================================================
#os.chdir(os.environ['G4WORKDIR'])
#if not os.path.exists('data/PhaseSpace/'+MODEL): os.mkdir('data/PhaseSpace/'+MODEL)

class Run(Thread):
  def __init__(self,i):
    Thread.__init__(self)
    self.thread = i
  def run(self):
    cmd = os.environ['ALGEBRADIR']+'/bin/ALGEBRA Phsp %d %d %s %d' % (self.thread,nPhotons,MODEL,randint(0,1e15))
    print cmd
    self.P = sp.Popen(cmd,shell=True,stdout=sp.PIPE,stderr=sp.PIPE) #,env=os.environ.update({'ALGEBRADIR':D['ALGEBRADIR']})  
    Processes.append(self.P) 
    self.out = self.P.communicate()[0]

print 'Creating %d threads' % (nthreads)
th = []
for i in range(nthreads): th.append(Run(i)); th[i].start(); time.sleep(0.1)

###########################################################################################
###########################################################################################

# skip initialisation time
while True:
  l = Processes[0].stdout.readline()
  if '#/#' in l: break
  if len(l)==1: time.sleep(0.1)

# track progress 
while True:
  if Processes[0].poll()!=None: break
  update = False
  nPhotonsGenerated = 0
  try:
      line = Processes[0].stdout.readline().split('\n')[0]
  except:
      print Processes[0].stdout
      break
  if '#/#' in line:
    try:
      t2=datetime.datetime.now()
      nPhotonsGenerated += int(line.split()[1])
      percent_done = '%.1f' % (nPhotonsGenerated*100./nPhotons)
      done=nPhotonsGenerated*100./nPhotons
      print percent_done,"%    ---  #/# ", nPhotonsGenerated, ', %6.2f min left'%((t2-t1).seconds/60./done*100.-(t2-t1).seconds/60.)
      ##f = file('progress.dat','w')
      ##f.write(percent_done)
      ##f.close()
    except:
      print "parsing error: [" + line + "]"
      pass
  time.sleep(1)

# really finish
go_on = range(nthreads)
while len(go_on)>0:
  for i in go_on:
    if Processes[i].poll()!=None: go_on.remove(i)
  time.sleep(5)

for i in range(nthreads): th[i].join()
