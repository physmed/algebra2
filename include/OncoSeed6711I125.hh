#ifndef OncoSeed6711I125_h
#define OncoSeed6711I125_h 1

#include "VSeed.hh"

class G4LogicalVolume;

class OncoSeed6711I125: public VSeed {
public:
  OncoSeed6711I125();
  ~OncoSeed6711I125(){;}

  void Construct();
  void PlaceYourself(G4VPhysicalVolume*,G4int);
  G4ThreeVector GetEmissionPosition();

private:
  static G4double energy[4];
  static G4double CDF[4];
  static G4double a[7];
  static G4double R[2];
  G4double VolCDF;

  G4double TiInRadius;
  G4double TiOutRadius;
  G4double TiZHalfLength;
  G4double CoatingThickness;
  G4double AgOutRadius;
  G4double AgZHalfLength;

  G4LogicalVolume* caps_log;
  G4LogicalVolume* gap_log;
  G4LogicalVolume* coating_log;
  G4LogicalVolume* core_log;
};

#endif
