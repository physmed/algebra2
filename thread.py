#!/usr/bin/env python
import os,sys,time
from random import randint
from ROOT import TFile,TCanvas,TLegend,TH1F,gStyle,TH3D,TObject
from math import log,exp
import numpy as np
import datetime

from threading import Thread
import subprocess as sp
from multiprocessing import cpu_count
nthreads = cpu_count()

# ==================================================================
# check ALGEBRADIR
# ==================================================================
if 'ALGEBRADIR' not in os.environ:
  print 'Error: ALGEBRADIR not set yet'
  print 'Stopped. Nothing done!'
  print 'Run the following to set ALGEBRADIR:'
  print '$ source setenv.sh'
  exit(0)

# ==================================================================
# Configuration
# ==================================================================
os.chdir(sys.argv[1])

A = {}
for l in open('run.conf').readlines():
  v = l.strip().split()  
  try: A[v[0]] = eval(v[1])
  except: A[v[0]] = v[1]

for i in A.keys(): print "%15s : %-s"%(str(i),str(A[i]))

# ==================================================================
# Manage the threads
# ==================================================================
nPhotons = int(A['NPHOTONS']/nthreads)

Processes = []

t1=datetime.datetime.now()

class Run(Thread):
  def __init__(self,i):
    Thread.__init__(self)
    self.thread = i
  def run(self):
    cmd = os.environ['ALGEBRADIR']+'/bin/ALGEBRA Brachy %d %d %s %d' % (self.thread,nPhotons,A['MODEL'],randint(0,1e13))
    print cmd
    self.P = sp.Popen(cmd,shell=True,stdout=sp.PIPE,stderr=sp.PIPE,stdin=sp.PIPE)
    Processes.append(self.P)
    self.out = self.P.communicate()

print 'Creating %d threads' % (nthreads)
th = []
for i in range(nthreads): th.append(Run(i)); th[i].start(); time.sleep(0.1)

###########################################################################################
###########################################################################################

# skip initialisation time
while True:
  l = Processes[0].stdout.readline()
  if '#/#' in l: break
  if len(l)==1: time.sleep(0.1)
  
# track progress 
while True:
  if Processes[0].poll()!=None: break
  update = False
  nPhotonsGenerated = 0
  try:
      line = Processes[0].stdout.readline().split('\n')[0]
  except:
      print Processes[0].stdout
      break
  if '#/#' in line:
    try:
      t2=datetime.datetime.now()
      nPhotonsGenerated += int(line.split()[1])
      percent_done = '%.1f' % (nPhotonsGenerated*100./nPhotons)
      done=nPhotonsGenerated*100./nPhotons
      print percent_done,"%    ---  #/# ", nPhotonsGenerated, ', %6.2f min left'%((t2-t1).seconds/60./done*100.-(t2-t1).seconds/60.)
      ##f = file('progress.dat','w')
      ##f.write(percent_done)
      ##f.close()
    except:
      print "parsing error: [" + line + "]"
      pass
  time.sleep(1)

# really finish
go_on = range(nthreads)
while len(go_on)>0:
  for i in go_on:
    if Processes[i].poll()!=None: go_on.remove(i)
  time.sleep(5)

for i in range(nthreads): th[i].join()

###########################################################################################
###########################################################################################
## stop here exit(0)
## run post_thread to continue
###########################################################################################
###########################################################################################
# 
# # ==================================================================
# # Add up the root files
# # ==================================================================
# os.system('hadd -f doses.root t.*.root')
# #os.system('hadd -f edep.t.root t.*.root')
# os.system('rm -f t.*.root')
# 
# # ==================================================================
# # Open it for updating
# # ==================================================================
# f = TFile('doses.root','update')
# h = f.Get('Edep')
# 
# # ==================================================================
# # Calibration factor
# # ==================================================================
# #                               Lambda|Calibration|HalfLife in days
# pars = {'MicroSelectronV2Ir192':[1.108,   0.9663,   73.827],
#         'FlexiSourceIr192':     [1.116,   0.9658,   73.827],
#         'SelectSeedI125':       [0.917,   0.4872,   59.43],
#         'OncoSeed6711I125':     [0.917,   0.4668,   59.43],
#         'ProsperaSeed3631I125': [0.917,   0.4486,   59.43],
#         'ProstaSeedSLI125':     [0.917,   0.4947,   59.43],
#         'TheraSeed200Pd103':    [0.686,   0.6620,   17.0]}
# 
# ######## 
# factor = 0.1
# factor /= (1e-3*h.GetXaxis().GetBinWidth(1)*h.GetYaxis().GetBinWidth(1)*h.GetZaxis().GetBinWidth(1)) # mm->cm ; mm3->cm3
# factor *= 1e-2   # cGy->Gy
# factor *= 1e3   # g->kg
# factor *= A['AirKermaRate']
# factor *= pars[A['MODEL']][0]/pars[A['MODEL']][1] 
# 
# ######## TIME factor
# if '192' in A['MODEL']:   ## <--- HDR, time is short
#   TotalTime = 0.
#   for i in file('dwells.dat','r'): 
#       TotalTime += eval(i.split()[3])
#   factor *= TotalTime/3600.    #### number of hour
# else:                     ## <--- LDR, time is infinite
#   factor *= float(len(file('dwells.dat','r').readlines()))  ## # of seeds
#   factor *= pars[A['MODEL']][2]*24./log(2)                  ## convert to hours
#   
# # report
# print "The converting Factor is: ", factor
# 
# 
# # ==================================================================
# # Calculate the errors
# # ==================================================================
# N = nPhotons ###h.GetBinContent(1)   ### total number of photons simulated
# #h.SetBinContent(0,0.)
# #h.SetBinContent(1,0.)
# #---
# n = [h.GetNbinsX(),h.GetNbinsY(),h.GetNbinsZ()]
# n2 = (n[0]+2)*(n[1]+2)*(n[2]+2)
# d = h.GetArray()
# d2 = h.GetSumw2().GetArray()
# d.SetSize(n2)
# D = np.array(d,np.float64)
# d2.SetSize(n2)
# D2 = np.array(d2,np.float64)
# fN = h.fN
# D /= N
# h.Set(fN,D)
# D2 /= N
# D *= D
# D2 -= D
# D2 /= N-1.        ### <--- standard error
# h.GetSumw2().Set(fN,D2)
# h.Scale(factor)   ### <--- convert by factor  : eDep to dose
# h.Write('doses',TObject.kOverwrite)
# 
# # ==================================================================
# # Histogram of Errors
# # ==================================================================
# he = h.Clone('errors')
# he.Reset()
# d2 = h.GetSumw2().GetArray()
# d2.SetSize(n2)
# D2 = np.array(d2,np.float64)
# he.Set(fN,np.sqrt(D2))
# he.Divide(h)
# he.Write()
# 
# # ==================================================================
# # Output to a 3ddose file
# # ==================================================================
# f1 = file('doses.3ddose','w')
# f1.write(' %d %d %d\n'%(n[0],n[1],n[2]))
# 
# a = np.zeros((n[0]+1),np.double)
# for i in range(n[0]+1): a[i] = 0.1*h.GetXaxis().GetBinLowEdge(i+1)
# np.savetxt(f1,a,fmt=' %7.5f',newline='')
# f1.write('\n')
# a = np.zeros((n[1]+1),np.double)
# for i in range(n[1]+1): a[i] = 0.1*h.GetYaxis().GetBinLowEdge(i+1)
# np.savetxt(f1,a,fmt=' %7.5f',newline='')
# f1.write('\n')
# a = np.zeros((n[2]+1),np.double)
# for i in range(n[2]+1): a[i] = 0.1*h.GetZaxis().GetBinLowEdge(i+1)
# np.savetxt(f1,a,fmt=' %7.5f',newline='')
# f1.write('\n ')
# 
# for c in ['doses','errors']:
#   a = f.Get(c).GetArray()
#   a.SetSize(n2)
#   data = np.array(a)
#   data.resize(n[2]+2,n[1]+2,n[0]+2)
#   data = data[1:-1,1:-1,1:-1]
#   for data_slice in data: np.savetxt(f1,data_slice,fmt='%7.5f')
# 
# f1.close()
# 
# # ==================================================================
# # close the  ROOT file: doses.root
# # ==================================================================
# f.Close()
