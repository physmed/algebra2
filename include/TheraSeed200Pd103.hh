#ifndef TheraSeed200Pd103_hh
#define TheraSeed200Pd103_hh

#include "VSeed.hh"

class G4LogicalVolume;

class TheraSeed200Pd103: public VSeed {
public:
  TheraSeed200Pd103();
  ~TheraSeed200Pd103(){;}

  void Construct();
  void PlaceYourself(G4VPhysicalVolume*,G4int);
  G4ThreeVector GetEmissionPosition();

private:
  static G4double energy[8];
  static G4double CDF[8];
  static G4double a[7];
  static G4double R[2];
  G4double SurfCDF[6];
  G4double ratiocapsidevolume;
  G4double RatioDiskTotal;

  G4double TiInRadius;
  G4double TiOutRadius;
  G4double TiZHalfLength;
  G4double AgInRadius;
  G4double AgOutRadius;
  G4double AgZHalfLength;
  G4double ShellHalfLength;
  G4double ShellOutRadius;
  G4double ShellInRadius;
  G4double CoatingThickness;
  G4double GraphiteOutRadius;
  G4double GraphiteLength;
  G4double LeadOutRadius;
  G4double LeadLength;
  G4double HemisphereOutRadius;
  G4double HemisphereInRadius;
  G4double CylindricalHalfLength;
  G4double GapHalfLength;
  G4double PhaseGraphite;

  G4LogicalVolume* caps_log;
  G4LogicalVolume* gap_log;
  G4LogicalVolume* coating_log;
  G4LogicalVolume* core_log;
  G4LogicalVolume* marker_log;
};

#endif
