//################################################################
// http://vali.physics.carleton.ca/clrp/seed_database/Ir192_HDR/
//################################################################
#include "G4Cons.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4Sphere.hh"
#include "G4Element.hh"
#include "G4Material.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4Gamma.hh"
#include "Randomize.hh"
#include "MBDCA_WGIr192.hh"

G4double MBDCA_WGIr192::energy[34] = { 65.1*CLHEP::keV, 66.8*CLHEP::keV, 75.7*CLHEP::keV, 136.3*CLHEP::keV, 296.0*CLHEP::keV, 308.5*CLHEP::keV, 316.5*CLHEP::keV, 468.1*CLHEP::keV, 476.5*CLHEP::keV, 588.6*CLHEP::keV,
           593.4*CLHEP::keV, 604.4*CLHEP::keV, 612.5*CLHEP::keV, 884.5*CLHEP::keV, 1061.5*CLHEP::keV, 1089.0*CLHEP::keV, 1378.0*CLHEP::keV, 61.5*CLHEP::keV, 63.0*CLHEP::keV, 71.4*CLHEP::keV, 177.0*CLHEP::keV, 201.3*CLHEP::keV,
           205.8*CLHEP::keV, 214.7*CLHEP::keV, 280.0*CLHEP::keV, 283.3*CLHEP::keV, 314.8*CLHEP::keV, 374.5*CLHEP::keV, 415.4*CLHEP::keV, 416.5*CLHEP::keV, 420.5*CLHEP::keV, 484.6*CLHEP::keV, 485.0*CLHEP::keV, 489.1*CLHEP::keV};

G4double MBDCA_WGIr192::CDF[34] = { 0.0117433, 0.0318809, 0.0406232, 0.0414104, 0.1662375, 0.2958489, 0.6568471, 0.8643123, 0.8643249, 0.8838101,
           0.8839980, 0.9191844, 0.9420621, 0.9432974, 0.9435253, 0.9435301, 0.9435357, 0.9485810, 0.9573232, 0.9610637, 0.9611072, 0.9630818,
           0.9769128, 0.9769185, 0.9769259, 0.9780089, 0.9781828, 0.9812665, 0.9812883, 0.9841763, 0.9844546, 0.9981117, 0.9981211, 1.0000000};

G4double MBDCA_WGIr192::a[7] = {-0.00082166,0.0063598,0.98048,0.13689,0.006429,1.9392e-4,0.12180};

G4double MBDCA_WGIr192::R[2] = {0.2,20.}; //[cm]

MBDCA_WGIr192::MBDCA_WGIr192():VSeed("MBDCA_WGIr192") { 
  VSeed::numentries = 34;
  VSeed::energy = MBDCA_WGIr192::energy;
  VSeed::CDF    = MBDCA_WGIr192::CDF;
  VSeed::a      = MBDCA_WGIr192::a;
  VSeed::R      = MBDCA_WGIr192::R;
  VSeed::Calibration = 1.0032;// +/- 0.0007
  VSeed::Lambda = 1.111;
  VSeed::HalfLife = 73.827*24; // [h]

  //################################################################
  // Seed dimensions
  //################################################################
  core_radius = 0.3*CLHEP::mm;
  core_half_length = 1.75*CLHEP::mm;
  caps_radius = 0.5*CLHEP::mm;
  caps_half_length = 0.5*(0.9+3.5+0.1)*CLHEP::mm;
  cable_half_length = 1.*CLHEP::mm;
  phase = 0.4*CLHEP::mm;
  cable_pos = core_half_length + 0.9*CLHEP::mm + cable_half_length;

  VSeed::OutRadius = caps_radius;
  VSeed::ZHalfLength = core_half_length+0.9*CLHEP::mm+2*cable_half_length;
  VSeed::ActiveLength = 2*core_half_length;
}

void MBDCA_WGIr192::Construct() {
  //################################################################
  // Define the seed materials
  //################################################################
  G4Element* elH = new G4Element("Hydrogen","H",1,1.01*CLHEP::g/CLHEP::mole);
  G4Element* elN = new G4Element("Nitrogen","N",7,14.01*CLHEP::g/CLHEP::mole);
  G4Element* elO = new G4Element("Oxygen","O",8,16.00*CLHEP::g/CLHEP::mole);
  G4Element* elAr = new G4Element("Argon","Ar",18,39.95*CLHEP::g/CLHEP::mole);
  G4Element* elC  = new G4Element("Carbon","C",6,12.01*CLHEP::g/CLHEP::mole);

  // Air material 40% de humedad segun TG43
  G4double density,Z;
  G4Material* Air = new G4Material("Air",density=1.2*CLHEP::mg/CLHEP::cm3,5);
  Air->AddElement(elH,0.0732*CLHEP::perCent);
  Air->AddElement(elC,0.0123*CLHEP::perCent);
  Air->AddElement(elN,75.0325*CLHEP::perCent);
  Air->AddElement(elO,23.6077*CLHEP::perCent);
  Air->AddElement(elAr,1.2743*CLHEP::perCent);

  // Water at 25ºC
  G4Material* Water = new G4Material("Water",density=0.998*CLHEP::g/CLHEP::cm3,2);
  Water->AddElement(elH,2);
  Water->AddElement(elO,1);

  // 316L INOX Steel
  G4Element* elSi = new G4Element("Si","Si",Z=14,28.09*CLHEP::g/CLHEP::mole);
  G4Element* elMn = new G4Element("Mn","Mn",Z=25,54.94*CLHEP::g/CLHEP::mole);
  G4Element* elCr = new G4Element("Cr","Cr",Z=24,51.996*CLHEP::g/CLHEP::mole);
  G4Element* elNi = new G4Element("Ni","Ni",Z=28,58.71*CLHEP::g/CLHEP::mole);
  G4Element* elFe = new G4Element("Fe","Fe",Z=26,55.84*CLHEP::g/CLHEP::mole);
  //G4Element* elMo = new G4Element("Mo","Mo",Z=42,95.94*CLHEP::g/CLHEP::mole);

  G4Material* INOX316L = new G4Material("INOX316L",density=8.02*CLHEP::g/CLHEP::cm3,5);
  INOX316L->AddElement(elSi,0.01);
  INOX316L->AddElement(elMn,0.02);
  INOX316L->AddElement(elCr,0.17);
  INOX316L->AddElement(elNi,0.12);
  INOX316L->AddElement(elFe,0.68);

  G4Material* mat_Cable = new G4Material("mat_Cable",density=5.*CLHEP::g/CLHEP::cm3,5);
  mat_Cable->AddElement(elSi,0.01);
  mat_Cable->AddElement(elMn,0.02);
  mat_Cable->AddElement(elCr,0.17);
  mat_Cable->AddElement(elNi,0.12);
  mat_Cable->AddElement(elFe,0.68);

  G4Element* elIr = new G4Element("Iridium","Ir",Z=77,192.217*CLHEP::g/CLHEP::mole);
  G4Material* mat_Ir = new G4Material("mat_Ir",22.4*CLHEP::g/CLHEP::cm3,1);
  mat_Ir->AddElement(elIr,1.);

  //################################################################
  // Define the seed geometry
  //################################################################
  // Cylindrical core
  G4Tubs* core_vol = new G4Tubs("core_vol",0.,core_radius,core_half_length,0.,CLHEP::twopi);
  core_log = new G4LogicalVolume(core_vol,mat_Ir,"core_log");
  G4VisAttributes* core_att = new G4VisAttributes(G4Colour(0,0,1));
  core_att->SetVisibility(true);
  core_att->SetForceSolid(true);
  core_log->SetVisAttributes(core_att);

  // Cable
  G4Tubs* cable_vol = new G4Tubs("cable_vol",0.,caps_radius,cable_half_length,0.,CLHEP::twopi);
  cable_log = new G4LogicalVolume(cable_vol,mat_Cable,"cable_log");
  G4VisAttributes* cable_att = new G4VisAttributes(G4Colour(1,1,1,0.5));
  cable_att->SetForceSolid(true);
  cable_att->SetVisibility(true);
  cable_log->SetVisAttributes(cable_att);

  G4Tubs* caps_cyl = new G4Tubs("caps_cyl",0.,caps_radius,caps_half_length,0.,CLHEP::twopi);
  G4Sphere* caps_sphere = new G4Sphere("caps_sphere",0.,caps_radius,0.,CLHEP::twopi,0.,CLHEP::halfpi);
  G4UnionSolid* caps_vol = new G4UnionSolid("caps_vol",caps_cyl,caps_sphere,0,G4ThreeVector(0.,0.,caps_half_length));
  caps_log = new G4LogicalVolume(caps_vol,INOX316L,"caps_log");
  G4VisAttributes* caps_att = new G4VisAttributes(G4Colour(1,1,0,0.5));
  caps_att->SetVisibility(true);
  caps_att->SetForceSolid(true);
  caps_log->SetVisAttributes(caps_att);

  G4UnionSolid* shell = new G4UnionSolid("shell",caps_vol,cable_vol,0,G4ThreeVector(0.,0.,-cable_pos));
  VSeed::OuterShell = shell;
}

void MBDCA_WGIr192::PlaceYourself(G4VPhysicalVolume* parent,G4int copyNo) {
  if ( !VSeed::OuterShell ) Construct();
  G4PVPlacement* caps_phys = new G4PVPlacement(0,G4ThreeVector(0.,0.,-phase),"caps_phys",caps_log,parent,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,phase),"core_phys",core_log,caps_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,-cable_pos),"cable_phys",cable_log,parent,false,copyNo);
}

G4ThreeVector MBDCA_WGIr192::GetEmissionPosition() {
  G4double rho = core_radius*sqrt(G4UniformRand());
  G4double phi = 2.*CLHEP::pi*G4UniformRand();
  return G4ThreeVector(rho*cos(phi),rho*sin(phi),core_half_length*(2.*G4UniformRand()-1.));
}
