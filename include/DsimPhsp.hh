#include "globals.hh"
#include <iostream>
#include "G4TrackStatus.hh"
#include "G4UserStackingAction.hh"
#include "G4ClassificationOfNewTrack.hh"

#include "CtrBase.hh"

using namespace std;

#ifndef DsimPhsp_H
#define DsimPhsp_H 1

#include "VSeed.hh"
#include "TFile.h"
#include "TTree.h"

class G4PhysicalVolume;

class G4Step;
class G4Track;
class G4Run;
class G4Event;

class TTree;
class TFile;

class DsimPhsp: public CtrBase
{
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
public:
    DsimPhsp(G4int thread1, G4String model);
    ~DsimPhsp();

    void Initialize();
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

public:

    virtual G4VPhysicalVolume* Construct();   
    virtual void GeneratePrimaries(G4Event*);
    //
    //virtual void ConstructParticle();
    //virtual void ConstructProcess();
    //virtual void SetCuts();

    virtual void UserSteppingAction(const G4Step*);    
    
    //virtual void PreUserTrackingAction(const G4Track*);
    //virtual void PostUserTrackingAction(const G4Track*);
    
    virtual G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*);
    
    virtual void BeginOfEventAction(const G4Event*);
    virtual void EndOfEventAction(const G4Event*);

    virtual void BeginOfRunAction(const G4Run*);
    virtual void   EndOfRunAction(const G4Run*);

private:
  
   
  
   G4double fBoxSize;

   G4VPhysicalVolume* fWorld;
   VSeed*             fSrc;
   G4Material*        fMaterial;
   
   //G4bool             isHDR;
   //G4int              Z,A;
    
public:
  TTree*              t;
  Float_t             x,y,z,theta,phi,E;
  Int_t               N;
  
private:
  
  G4int               thread;
  G4String            sourceTag;
 
private:
  TFile*              f;
  
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
  
};

#endif
