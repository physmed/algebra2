#include <stdlib.h>     /* getenv */

#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

#include "G4Step.hh"
#include "G4RegularNavigationHelper.hh"
#include "G4VProcess.hh"
#include "G4Region.hh"
#include "TFile.h"
#include "LinearTrackLengthEstimator.hh"
#include "G4PhantomParameterisation.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"

#include "G4Region.hh"
#include "G4ProductionCuts.hh"

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

#include "G4ios.hh"
#include "G4StepPoint.hh"
#include "G4ParticleDefinition.hh"
#include "G4TrackStatus.hh"
#include "G4ParticleDefinition.hh"
#include "G4Gamma.hh"

#include "G4Color.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

#include "DsimBrachy.hh"
#include "DsimPhantomReader.hh"
#include "DsimDwellList.hh"
#include "DsimPhspace.hh"
#include "LinearTrackLengthEstimator.hh"

#include "TH3D.h"

DsimBrachy::DsimBrachy(G4int thread1, G4String model)
{
  world_size_mm=2*m;
  box_size_mm=1*m;
  box_mat_str="AIR";
  thread = thread1;
  sourceTag=model;
  Initialize();
}

DsimBrachy::~DsimBrachy(){
  delete phsp;
  delete phantom;
  delete dwells;
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DsimBrachy::Initialize(){

	G4String fn = "init/brachy_init.txt";
	fstream thefile(Form("%s/init/brachy_init.txt",getenv("ALGEBRADIR")),ios::in);
	if( !thefile.is_open() ) {
		G4cout << "Warning: Parameters file <"<<fn<<"> could not be opened." << G4endl;
		G4cout << "         Default values are used." <<G4endl;
	}else{
		G4cout << "Reading Parameters From : <"<<Form("%s/init/brachy_init.txt",getenv("ALGEBRADIR"))<<">" << G4endl;
		G4String dummy;
		thefile>>dummy>>world_size_mm>>dummy>>box_size_mm>>dummy>>box_mat_str;
	}

  fParallelBool=true;

  if (box_mat_str=="AIR")
  {
    fMaterial=G4NistManager::Instance()->FindOrBuildMaterial("G4_AIR");
	G4cout << "Container Box's Material: AIR" << G4endl;
  }else{
	fMaterial=G4NistManager::Instance()->FindOrBuildMaterial("G4_WATER");
	G4cout << "Container Box's Material: WATER" << G4endl;
  }

  dwells=new DsimDwellList("dwells.dat");
  dwells->Dump();
  G4String filename = Form("%s/%s_%d.root",getenv("PHSPDIR"),sourceTag.c_str(),thread);
  phsp = new DsimPhspace(filename);

  phantom=new DsimPhantomReader();
  phantom->LoadFile("phantom.egsphant");

  EDEP00 = new TH3D("Edep","",phantom->NbinsX,phantom->Xmin,phantom->Xmax,
		              phantom->NbinsY,phantom->Ymin,phantom->Ymax,
		              phantom->NbinsZ,phantom->Zmin,phantom->Zmax);
  EDEP00->Sumw2();

  LTE=new LinearTrackLengthEstimator();
}

G4VPhysicalVolume* DsimBrachy::Construct()
{
  // Cleanup old geometry
  G4GeometryManager::GetInstance()->OpenGeometry();
  G4PhysicalVolumeStore::GetInstance()->Clean();
  G4LogicalVolumeStore::GetInstance()->Clean();
  G4SolidStore::GetInstance()->Clean();

  /////////////////////////////////////////////////////////////////////////////
  // World and Container
  G4cout<<"[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[["<<G4endl;
  G4ThreeVector shift=phantom->GetCenter();
  G4cout<<"Container Shifts to Point:"<<shift<<" * mm"<<G4endl;
  G4cout<<"]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]"<<G4endl;

  // World
  G4double hall_side = world_size_mm * mm/2;
  G4Box* hall_vol = new G4Box("hall_vol",hall_side,hall_side,hall_side);
  G4LogicalVolume* hall_log = new G4LogicalVolume(hall_vol,fMaterial,"hall_log");
  hall_phys = new G4PVPlacement(0,G4ThreeVector(),hall_log,"hall_phys",0,false,0);
  G4VisAttributes* hall_att = new G4VisAttributes(G4Colour(1,0,0));
  hall_att->SetVisibility(true);
  hall_log->SetVisAttributes(hall_att);

  // Container box
  G4double box_side = box_size_mm * mm/2;
  G4Box* box_vol = new G4Box("box_vol",box_side,box_side,box_side);
  box_log = new G4LogicalVolume(box_vol,fMaterial,"box_log");
  box_phys = new G4PVPlacement(0,shift,box_log,"box_phys",hall_log,false,0);  // shift is here
  G4VisAttributes* box_att = new G4VisAttributes(G4Colour(0,1,0));
  box_att->SetVisibility(true);
  box_log->SetVisAttributes(box_att);

  ////////////////////////////////////////////////////////////////////////////
  // Phantom
  G4cout<<"(((((((((((((((((((((((((((((((((("<<G4endl;
  phantom->Construct(box_log);
  G4cout<<"Building Phantom Done"<<G4endl;
  G4cout<<"))))))))))))))))))))))))))))))))))"<<G4endl;
  ////////////////////////////////////////////////////////////////////////////
  //
  return hall_phys;
}

void DsimBrachy::GeneratePrimaries(G4Event* anEvent)
{
  G4ParticleDefinition* particle = G4ParticleTable::GetParticleTable()->FindParticle("gamma");
  fParticleGun->SetParticleDefinition(particle);

  G4double sx,sy,sz,su,sv,sw;
  dwells->Select(sx,sy,sz,su,sv,sw);
  phsp->Next();
  G4ThreeVector position(phsp->x,phsp->y,phsp->z);   // CLHEP::mm
  G4ThreeVector momentum;
  momentum.setRThetaPhi(1.,phsp->theta,phsp->phi);
  //if ( isHDR ) {
    position.rotateUz( G4ThreeVector(su,sv,sw) );
    momentum.rotateUz( G4ThreeVector(su,sv,sw) );
  //} else ParticlePosition *= 1.000001;
  position += G4ThreeVector(sx,sy,sz);  // CLHEP::mm

  fParticleGun->SetParticlePosition( position );
  fParticleGun->SetParticleMomentumDirection( momentum );
  fParticleGun->SetParticleEnergy( phsp->E );  //CLHEP::MeV
  fParticleGun->GeneratePrimaryVertex(anEvent);
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "G4Track.hh"
#include "G4SteppingManager.hh"
#include "G4Step.hh"

void DsimBrachy::UserSteppingAction(const G4Step* aStep)
{
    G4Track *tr = aStep->GetTrack();

    G4String volumeName = tr->GetVolume()->GetName();
    G4String nextVolumeName = tr->GetNextVolume()->GetName();

    G4double en = aStep->GetPreStepPoint()->GetKineticEnergy();
    G4double dose = LTE->MuenRho(tr->GetMaterial(),en)*en;
    G4String procName = aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName();
    G4double edep = aStep->GetTotalEnergyDeposit();
    G4int charge=aStep->GetPostStepPoint()->GetCharge();

    if ( nextVolumeName=="hall_phys" ) {
        tr->SetTrackStatus(fStopAndKill);
        return;
    }
    if ( volumeName!="phantom_phys" ) return;
    if ( !tr->GetMaterial()->GetName().contains("x_") ) return;

    vector< pair<G4int,G4double> > Steps = G4RegularNavigationHelper::Instance()->theStepLengths;
    it = Steps.begin();
    it_end = Steps.end();

    G4double CumulStepLength = 0.;
    G4double StepLength = aStep->GetStepLength();

    while ( fabs(StepLength-CumulStepLength)>1e-6 && it!=it_end ) {
	    if ( CumulStepLength+it->second > StepLength ) it->second = StepLength - CumulStepLength;
	    CumulStepLength += it->second;
	    it->second *= dose;
	    ret = Edep.insert(*it);
	    if ( !ret.second ) Edep[it->first] += it->second;
	    ++it;
    }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "G4Event.hh"

void DsimBrachy::BeginOfEventAction(const G4Event* anEvent) {
    G4int evtID=anEvent->GetEventID();
    //if (evtID%10000==0) G4cout<<"*/*   "<<evtID<<"   began"<<G4endl;
    Edep.clear();
}

void DsimBrachy::EndOfEventAction(const G4Event* anEvent) {
  G4int evtID=anEvent->GetEventID();
  if (evtID%10000==0) G4cout<<"#/#   "<<evtID<<"   "<<G4endl;

  if ( !Edep.size() ) return;
  it2     = Edep.begin();
  it_end2 = Edep.end();
  while (it2!=it_end2) {
    G4ThreeVector pos = phantom->GetParam()->GetTranslation(it2->first) + phantom->GetCenter();
    EDEP00->Fill(pos[0],pos[1],pos[2],it2->second);
    ++it2;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "G4Track.hh"

G4ClassificationOfNewTrack DsimBrachy::ClassifyNewTrack(const G4Track* aTrack) {
  //if ( aTrack->GetParticleDefinition()->GetParticleName() != "gamma" )
  //  return fKill;
  //else
    return fUrgent;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "G4Run.hh"

void DsimBrachy::BeginOfRunAction(const G4Run*){
}

void DsimBrachy::EndOfRunAction(const G4Run* ){
   ////////////////////////////////////////////////////////////////////////////
  // Phantom
  G4cout<<"(((((((((((((((((((((((((((((((((("<<G4endl;
  G4cout<<"Saving ..."<<G4endl;
  TFile f1(Form("t.%d.root",thread),"recreate");
  EDEP00->SetDirectory(&f1);
  EDEP00->SetBinContent(0,phsp->TotalE);
  EDEP00->SetBinContent(1,phsp->Nphoton);
  EDEP00->Write("Edep");
  f1.Close();
  G4cout<<"))))))))))))))))))))))))))))))))))"<<G4endl;
  ////////////////////////////////////////////////////////////////////////////
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
