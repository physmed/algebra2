//################################################################
// http://vali.physics.carleton.ca/clrp/seed_database/Ir192_HDR/Flexisource/
//################################################################
#include "G4Cons.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PhysicalVolumeStore.hh" 
#include "G4PVPlacement.hh"
#include "G4NistManager.hh"
#include "G4Element.hh"
#include "Randomize.hh"
#include "FlexiSourceIr192.hh"

G4double FlexiSourceIr192::energy[34] = { 65.1*CLHEP::keV, 66.8*CLHEP::keV, 75.7*CLHEP::keV, 136.3*CLHEP::keV, 296.0*CLHEP::keV, 308.5*CLHEP::keV, 316.5*CLHEP::keV, 468.1*CLHEP::keV, 476.5*CLHEP::keV, 588.6*CLHEP::keV,
           593.4*CLHEP::keV, 604.4*CLHEP::keV, 612.5*CLHEP::keV, 884.5*CLHEP::keV, 1061.5*CLHEP::keV, 1089.0*CLHEP::keV, 1378.0*CLHEP::keV, 61.5*CLHEP::keV, 63.0*CLHEP::keV, 71.4*CLHEP::keV, 177.0*CLHEP::keV, 201.3*CLHEP::keV,
           205.8*CLHEP::keV, 214.7*CLHEP::keV, 280.0*CLHEP::keV, 283.3*CLHEP::keV, 314.8*CLHEP::keV, 374.5*CLHEP::keV, 415.4*CLHEP::keV, 416.5*CLHEP::keV, 420.5*CLHEP::keV, 484.6*CLHEP::keV, 485.0*CLHEP::keV, 489.1*CLHEP::keV};

G4double FlexiSourceIr192::CDF[34] = { 0.0117433, 0.0318809, 0.0406232, 0.0414104, 0.1662375, 0.2958489, 0.6568471, 0.8643123, 0.8643249, 0.8838101, 
           0.8839980, 0.9191844, 0.9420621, 0.9432974, 0.9435253, 0.9435301, 0.9435357, 0.9485810, 0.9573232, 0.9610637, 0.9611072, 0.9630818,
           0.9769128, 0.9769185, 0.9769259, 0.9780089, 0.9781828, 0.9812665, 0.9812883, 0.9841763, 0.9844546, 0.9981117, 0.9981211, 1.0000000};

G4double FlexiSourceIr192::a[7] = {0.00099501,-0.0079781,1.0049,0.10701,0.0062005,8.7502e-8,0.10542};

G4double FlexiSourceIr192::R[2] = {0.2,20.};

FlexiSourceIr192::FlexiSourceIr192():VSeed("FlexiSourceIr192") { 
  VSeed::numentries = 34;
  VSeed::energy = FlexiSourceIr192::energy;
  VSeed::CDF    = FlexiSourceIr192::CDF;
  VSeed::a      = FlexiSourceIr192::a;
  VSeed::R      = FlexiSourceIr192::R;
  VSeed::Calibration = 0.9658; // +/- 0.0006
  VSeed::Lambda = 1.116;
  VSeed::HalfLife = 73.827*24; // [h]

  //################################################################
  // Seed dimensions
  //################################################################
  outerSourceRadius = 0.3*CLHEP::mm;
  heightSourceHalf = 1.75*CLHEP::mm;
  outerCylinderRadius = 0.425*CLHEP::mm;
  innerCylinderRadius = 0.335*CLHEP::mm;
  heightCylinderHalf = 1.8*CLHEP::mm;
  heightLCapHalf = 0.2*CLHEP::mm;
  heightRCapCylHalf = 0.492*CLHEP::mm/2.;
  innerRCapConRadius = 0.17*CLHEP::mm;
  heightRCapConHalf = 0.108*CLHEP::mm/2.; // rather from the schema here: 

  outerCableRadius = 0.25*CLHEP::mm;
  heightCableHalf = 2.5*CLHEP::mm;
  cable_pos = heightCylinderHalf + 2.*heightLCapHalf + heightCableHalf;

  VSeed::OutRadius = outerCylinderRadius;
  VSeed::ZHalfLength = heightCylinderHalf + 2.*heightLCapHalf + 2.*heightCableHalf;
  VSeed::ActiveLength = 2*heightSourceHalf;
}

void FlexiSourceIr192::Construct() {
  //################################################################
  // Define the seed materials
  //################################################################
  G4int nel;
  G4double density, a, z, pct;
  G4String name, symbol;

  G4Element* Ir = new G4Element(name="Ir"  ,symbol="Ir", z=77.0, a=192.21700*CLHEP::g/CLHEP::mole);
  G4Element* Fe = new G4Element(name="Iron"     ,symbol="Fe", z=26.0, a=55.845000*CLHEP::g/CLHEP::mole);
  G4Element* Cr = new G4Element(name="Chromium" ,symbol="Cr", z=24.0, a=51.996100*CLHEP::g/CLHEP::mole);
  G4Element* Ni = new G4Element(name="Nickel"   ,symbol="Ni", z=28.0, a=58.693400*CLHEP::g/CLHEP::mole);
  G4Element* Mn = new G4Element(name="Manganese",symbol="Mn", z=25.0, a=54.938049*CLHEP::g/CLHEP::mole);
  G4Element* Si = new G4Element(name="Silicium" ,symbol="Si", z=14.0, a=28.085500*CLHEP::g/CLHEP::mole);
  G4Element* C  = new G4Element(name="Carbon"   ,symbol="C" , z= 6.0, a=12.010700*CLHEP::g/CLHEP::mole);

  G4Material* mat_Ir = new G4Material("Ir",density=22.42*CLHEP::g/CLHEP::cm3,nel=1);
  mat_Ir->AddElement(Ir,pct=1.0);

  // AISI 304 stainless steel capsule and cable (density of 7.8 CLHEP::g/CLHEP::cm3)
  G4Material* mat_Steel = new G4Material("INOX_304_capsule",density=8.0*CLHEP::g/CLHEP::cm3,nel=6);
  mat_Steel->AddElement(Fe,pct=0.6792);
  mat_Steel->AddElement(Cr,pct=0.1900);
  mat_Steel->AddElement(Ni,pct=0.1000);
  mat_Steel->AddElement(Mn,pct=0.0200);
  mat_Steel->AddElement(Si,pct=0.0100);
  mat_Steel->AddElement(C ,pct=0.0008);

/* 
 MEDIUM=SS_AISI304,STERNCID=SS_AISI304 MIXT,RHO= 8.0200E+00,NE= 5, IUNRST=0, EPSTFL=0, IAPRIM=1
 ASYM=NI,Z=28.,A=   58.710,PZ= 1.70329E-03,RHOZ= 1.00000E-01
 ASYM=SI,Z=14.,A=   28.088,PZ= 3.56024E-04,RHOZ= 1.00000E-02
 ASYM=CR,Z=24.,A=   51.998,PZ= 3.65399E-03,RHOZ= 1.90000E-01
 ASYM=MN,Z=25.,A=   54.938,PZ= 3.64047E-04,RHOZ= 2.00000E-02
 ASYM=FE,Z=26.,A=   55.847,PZ= 1.21761E-02,RHOZ= 6.80000E-01
*/
  G4Material* mat_Cable = new G4Material("INOX_304_cable",density=5.6*CLHEP::g/CLHEP::cm3,nel=6);
  mat_Cable->AddElement(Fe,pct=0.6792);
  mat_Cable->AddElement(Cr,pct=0.1900);
  mat_Cable->AddElement(Ni,pct=0.1000);
  mat_Cable->AddElement(Mn,pct=0.0200);
  mat_Cable->AddElement(Si,pct=0.0100);
  mat_Cable->AddElement(C ,pct=0.0008);

  G4NistManager* man = G4NistManager::Instance();
  G4Material* Air = man->FindOrBuildMaterial("G4_AIR");

  //################################################################
  // Define the seed geometry
  //################################################################

  // Ir active core
  G4Tubs* core_vol = new G4Tubs("core_vol",0.,outerSourceRadius,heightSourceHalf,0.,CLHEP::twopi);
  core_log = new G4LogicalVolume(core_vol,mat_Ir,"core_log");
  G4VisAttributes* core_att = new G4VisAttributes(G4Colour(0,1,0));
  core_att->SetForceSolid(true);
  core_att->SetVisibility(true);
  core_log->SetVisAttributes(core_att);

  // Gap filled with air
  G4Tubs* gap_vol = new G4Tubs("gap_vol",0.,innerCylinderRadius,heightCylinderHalf,0.,CLHEP::twopi);
  gap_log = new G4LogicalVolume(gap_vol,Air,"gap_log");
  G4VisAttributes* gap_att = new G4VisAttributes(G4Colour(0.1,0.1,1,0.5));
  gap_att->SetForceSolid(true);
  gap_att->SetVisibility(true);
  gap_log->SetVisAttributes(gap_att);

  // Cylinder capsule
  G4Tubs* cyl_vol = new G4Tubs("cyl_vol",innerCylinderRadius,outerCylinderRadius,heightCylinderHalf,0.,CLHEP::twopi);
  // Left conical cap
  G4Cons* LCap_vol = new G4Cons("LCap_vol",0.,outerCableRadius,0.,outerCylinderRadius,heightLCapHalf,0.,CLHEP::twopi);
  // Right cylindrical and conical parts
  G4Tubs* RCapCyl_vol = new G4Tubs("RCapCyl_vol",0.,outerCylinderRadius,heightRCapCylHalf,0.,CLHEP::twopi);
  G4Cons* RCapCon_vol = new G4Cons("RCapCon_vol",0.,outerCylinderRadius,0.,innerRCapConRadius,heightRCapConHalf,0.,CLHEP::twopi);

  // Union of Cylinder+LCap+RCap+Cable = metallic capsule (INOX 304)
  G4UnionSolid* Vol1 = new G4UnionSolid("Vol1",cyl_vol,LCap_vol,0,G4ThreeVector(0,0,-heightCylinderHalf-heightLCapHalf));
  G4UnionSolid* Vol2 = new G4UnionSolid("Vol2",Vol1,RCapCyl_vol,0,G4ThreeVector(0,0,heightCylinderHalf+heightRCapCylHalf));
  G4UnionSolid* caps_vol = new G4UnionSolid("caps_vol",Vol2,RCapCon_vol,0,G4ThreeVector(0,0,heightCylinderHalf+2*heightRCapCylHalf+heightRCapConHalf));
  caps_log = new G4LogicalVolume(caps_vol,mat_Steel,"caps_log");
  G4VisAttributes* caps_att = new G4VisAttributes(G4Colour(1,1,0,0.5));
  caps_att->SetForceSolid(true);
  caps_att->SetVisibility(true);
  caps_log->SetVisAttributes(caps_att);

  G4Tubs* cable_vol = new G4Tubs("cable_vol",0.,outerCableRadius,heightCableHalf,0.,CLHEP::twopi);
  cable_log = new G4LogicalVolume(cable_vol,mat_Cable,"cable_log");
  G4VisAttributes* cable_att = new G4VisAttributes(G4Colour(1,1,1,0.5));
  cable_att->SetForceSolid(true);
  cable_att->SetVisibility(true);
  cable_log->SetVisAttributes(cable_att);

  G4UnionSolid* shell = new G4UnionSolid("shell",caps_vol,cable_vol,0,G4ThreeVector(0.,0.,-cable_pos));
  VSeed::OuterShell = shell;
}

void FlexiSourceIr192::PlaceYourself(G4VPhysicalVolume* parent,G4int copyNo) {
  if ( !VSeed::OuterShell ) Construct();
  new G4PVPlacement(0,G4ThreeVector(),"caps_phys",caps_log,parent,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(0.,0.,-cable_pos),"cable_phys",cable_log,parent,false,copyNo);
  G4PVPlacement* gap_phys = new G4PVPlacement(0,G4ThreeVector(),"gap_phys",gap_log,parent,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(),"core_phys",core_log,gap_phys,false,copyNo);
}

G4ThreeVector FlexiSourceIr192::GetEmissionPosition() {
  G4double rho,phi,x,y,z;
  rho = outerSourceRadius*sqrt(G4UniformRand());
  phi = 2.*CLHEP::pi*G4UniformRand();
  x = rho*cos(phi);
  y = rho*sin(phi);
  z = heightSourceHalf*(2.*G4UniformRand()-1.);
  return G4ThreeVector(x,y,z);
}
