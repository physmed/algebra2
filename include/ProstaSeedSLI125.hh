#ifndef ProstaSeedSLI125_hh
#define ProstaSeedSLI125_hh

#include "VSeed.hh"

class G4LogicalVolume;

class ProstaSeedSLI125: public VSeed {
public:
  ProstaSeedSLI125();
  ~ProstaSeedSLI125(){;}

  void Construct();
  void PlaceYourself(G4VPhysicalVolume*,G4int);
  G4ThreeVector GetEmissionPosition();

private:
  static G4double energy[5];
  static G4double CDF[5];
  static G4double a[7];
  static G4double R[2];
  G4double VolCDF;

  G4double TiInRadius;
  G4double TiOutRadius;
  G4double TiZHalfLength;
  G4double CoatingThickness;
  G4double AgOutRadius;
  G4double AgZHalfLength;
  G4double AirSphereRadius;
  G4double AgSpherePos;
  G4double AgSpherePos2;

  G4LogicalVolume* caps_log;
  G4LogicalVolume* gap_log;
  G4LogicalVolume* Ag_log;
};

#endif
