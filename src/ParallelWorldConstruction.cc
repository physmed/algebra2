#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
//#include "PrimaryGeneratorAction.hh"
#include "VSeed.hh"
#include "GoldGrain.hh"
#include "G4Tubs.hh"
#include "Riostream.h"
#include "ParallelWorldConstruction.hh"

//#include "G4GDMLParser.hh"

#include "G4Material.hh"
#include "G4NistManager.hh"

#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"

#include "G4Material.hh"

#include "G4Torus.hh"
#include "G4Tubs.hh"
#include "G4ExtrudedSolid.hh"

#include "G4RotationMatrix.hh"

#include "G4VisAttributes.hh"

#include "G4SubtractionSolid.hh"

ParallelWorldConstruction::ParallelWorldConstruction(G4String& worldName) :G4VUserParallelWorld(worldName) {;}

ParallelWorldConstruction::~ParallelWorldConstruction() {;}

void ParallelWorldConstruction::Construct() {
  G4VPhysicalVolume* ghostWorld = GetWorld();

  // Insert all seeds if LDR
//   PrimaryGeneratorAction* gen = PrimaryGeneratorAction::GetInstance();
//
//   G4LogicalVolume* cont_log = new G4LogicalVolume(new G4Tubs("cont_vol",0.,gen->s->OutRadius,gen->s->ZHalfLength,0.,CLHEP::twopi),0,"cont_log",0,0,0);
//
//   if ( !gen->isHDR ) {
//     for (unsigned int i=0;i<gen->Positions.size();i++) {
//       G4ThreeVector Position = gen->Positions[i];
//       G4ThreeVector Direction = gen->Directions[i];
//
//       G4RotationMatrix* rot = new G4RotationMatrix();
//       rot->rotateY( Direction.theta() );
//       rot->rotateZ( Direction.phi() );
//
//       gen->s->PlaceYourself(new G4PVPlacement(G4Transform3D(*rot,Position),"cont_phys",cont_log,ghostWorld,false,i),i);
//     }
//   }


	/**
  // Insert gold grains if present
  if ( gen->GoldGrainPositions.size()>0 ) {
    GoldGrain* g = new GoldGrain();
    G4LogicalVolume* grain_log = new G4LogicalVolume(new G4Tubs("grain_vol",0.,g->OutRadius,g->ZHalfLength,0.,CLHEP::twopi),0,"grain_log",0,0,0);
    for (unsigned int i=0;i<gen->GoldGrainPositions.size();i++) {
      g->PlaceYourself(new G4PVPlacement(G4Transform3D(G4RotationMatrix(),gen->GoldGrainPositions[i]),"cont_phys",grain_log,ghostWorld,false,i),i);
    }
  }
	 **/


//    G4GDMLParser parser;
//    parser.Read("/Users/markma/geant4_workdir/algebra/tg186cylapp-withsource.gdml", false);
//    G4LogicalVolume* LV = parser.GetVolume("expHall_log");
//	new G4PVPlacement(0,G4ThreeVector(0,0,0),"app-physics-volume",LV,ghostWorld,false,0);


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//            WGMBDCAIr192 Applicator
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Definition of basic variables
	G4double density;
	G4Material* mat;
	G4int nel;
  
	G4NistManager* man = G4NistManager::Instance();
  
	G4Element* elH  = man->FindOrBuildElement( 1);
	G4Element* elC  = man->FindOrBuildElement( 6);
	G4Element* elN  = man->FindOrBuildElement( 7);
	G4Element* elO  = man->FindOrBuildElement( 8);
	G4Element* elNa = man->FindOrBuildElement(11);
	G4Element* elMg = man->FindOrBuildElement(12);
	G4Element* elSi = man->FindOrBuildElement(14);
	G4Element* elP  = man->FindOrBuildElement(15);
	G4Element* elS  = man->FindOrBuildElement(16);
	G4Element* elCl = man->FindOrBuildElement(17);
	G4Element* elAr = man->FindOrBuildElement(18);
	G4Element* elK  = man->FindOrBuildElement(19);
	G4Element* elCa = man->FindOrBuildElement(20);
	G4Element* elCr = man->FindOrBuildElement(24);
	G4Element* elMn = man->FindOrBuildElement(25);
	G4Element* elFe = man->FindOrBuildElement(26);
	G4Element* elNi = man->FindOrBuildElement(28);
	G4Element* elI  = man->FindOrBuildElement(53);
	G4Element* elW  = man->FindOrBuildElement(74);
	G4Element* elIr = man->FindOrBuildElement(77);
  
	//// AIR
    density = 0.001205;
    G4Material* air = new G4Material("Air",density*CLHEP::g/CLHEP::cm3,nel=4);
    air->AddElement(elC, 0.000124);
    air->AddElement(elN, 0.755268);
    air->AddElement(elO, 0.231781);
    air->AddElement(elAr,0.012827);
  
	//// Iradium
    density = 17.6;
    G4Material* ir= new G4Material("Iradium",density*CLHEP::g/CLHEP::cm3,nel=1);
    ir->AddElement(elIr,  1.0);
  
	//// Steel
    density = 8.02;
    G4Material* steel = new G4Material("Steel",density*CLHEP::g/CLHEP::cm3,nel=5);
    steel->AddElement(elSi, 0.010000);
    steel->AddElement(elMn, 0.020000);
    steel->AddElement(elCr, 0.170000);
    steel->AddElement(elNi, 0.120000);
	steel->AddElement(elFe, 0.680000);
  
	//// Stainless Steel
    density = 8.00;
    G4Material* stainless = new G4Material("Stainless",density*CLHEP::g/CLHEP::cm3,nel=7);
    stainless->AddElement(elC,  0.000800);
    stainless->AddElement(elSi, 0.010000);
    stainless->AddElement(elP,  0.000450);
    stainless->AddElement(elCr, 0.190000);
	stainless->AddElement(elMn, 0.020000);
    stainless->AddElement(elNi, 0.095000);
	stainless->AddElement(elFe, 0.683750);
  
 	//// SteelCable
    density = 5.00;
    G4Material* cable = new G4Material("SteelCable",density*CLHEP::g/CLHEP::cm3,nel=5);
    cable->AddElement(elSi, 0.010000);
    cable->AddElement(elMn, 0.020000);
    cable->AddElement(elCr, 0.170000);
    cable->AddElement(elNi, 0.120000);
	cable->AddElement(elFe, 0.680000);
  
	//// Tungsten
    density = 17.6;
    G4Material* tungsten = new G4Material("Tungsten",density*CLHEP::g/CLHEP::cm3,nel=1);
    tungsten->AddElement(elW,  1.0);
  
	//// Densimet
    density = 17.6;
    G4Material* densimet = new G4Material("Densimet",density*CLHEP::g/CLHEP::cm3,nel=3);
    densimet->AddElement(elW,  0.925000);
    densimet->AddElement(elNi, 0.050000);
	densimet->AddElement(elFe, 0.002500);
  
	//// PMMA
    density = 1.19;
    G4Material* pmma = new G4Material("PMMA",density*CLHEP::g/CLHEP::cm3,nel=3);
    pmma->AddElement(elC, 0.599720);
    pmma->AddElement(elO, 0.319584);
	pmma->AddElement(elH, 0.080695);
  
  
	//// Solids and Logicals
  
    G4Tubs* sld_body = new G4Tubs("body_solid", 12*CLHEP::mm, 18*CLHEP::mm, 61*CLHEP::mm, 0, 2*CLHEP::pi);
	G4LogicalVolume* log_body = new G4LogicalVolume(sld_body, pmma, "body_log");
  
	G4Tubs* sld_shield = new G4Tubs("shield_solid", 1.6*CLHEP::mm,12*CLHEP::mm, 61*CLHEP::mm, -CLHEP::pi/2, CLHEP::pi);
	G4LogicalVolume* log_shield = new G4LogicalVolume(sld_shield, densimet, "shield_log");
  
	G4Tubs* sld_airshield = new G4Tubs("airshield_solid", 1.6*CLHEP::mm,12*CLHEP::mm, 61*CLHEP::mm, CLHEP::pi/2, CLHEP::pi);
	G4LogicalVolume* log_airshield = new G4LogicalVolume(sld_airshield, air, "airshield_log");
  
	G4Tubs* sld_probe  = new G4Tubs("probe_solid", 0.5*CLHEP::mm, 1.6*CLHEP::mm, 61*CLHEP::mm, 0, 2*CLHEP::pi);
	G4LogicalVolume* log_probe = new G4LogicalVolume(sld_probe, stainless, "probe_log");
  
  
	G4Tubs* sld_inprobe  = new G4Tubs("inprobe_solid", 0*CLHEP::mm, 0.5*CLHEP::mm, 61*CLHEP::mm, 0, 2*CLHEP::pi);
	G4LogicalVolume* log_inprobe = new G4LogicalVolume(sld_inprobe, air, "inprobe_log");
  
	G4Sphere* sld_cap = new G4Sphere("cap_solid", 0, 18*CLHEP::mm, 0, 2*CLHEP::pi, 0, CLHEP::pi/2);
	G4LogicalVolume* log_cap = new G4LogicalVolume(sld_cap, pmma, "cap_log");
  
  
	//// Placement
  
	new G4PVPlacement(0, G4ThreeVector(0,0,0),"body_phys",log_body,     ghostWorld,false,0);
	new G4PVPlacement(0, G4ThreeVector(0,0,0),"shld_phys",log_shield,   ghostWorld,false,0);
	new G4PVPlacement(0, G4ThreeVector(0,0,0),"aish_phys",log_airshield,ghostWorld,false,0);
	new G4PVPlacement(0, G4ThreeVector(0,0,0),"prob_phys",log_probe,    ghostWorld,false,0);
	new G4PVPlacement(0, G4ThreeVector(0,0,0),"inpb_phys",log_inprobe,  ghostWorld,false,0);
	new G4PVPlacement(0, G4ThreeVector(0,0,61*CLHEP::mm),"cap_phys",log_cap,      ghostWorld,false,0);


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////
////            Nasopharynx Applicator
////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
// 
// //// Water
//  G4Material* fMaterial=G4NistManager::Instance()->FindOrBuildMaterial("G4_WATER");
//  //// Tungsten
//  G4Element* elW  = G4NistManager::Instance()->FindOrBuildElement(74);
//  G4double density = 17.6;
//  G4Material* fTungsten = new G4Material("Tungsten",density*CLHEP::g/CLHEP::cm3,1);
//  fTungsten->AddElement(elW,  1.0);
//  
//  
//    G4double fX, fY, fZ;
//     fX=-2.4*CLHEP::mm;
//     fY=-260.1*CLHEP::mm;
//     fZ=88.5*CLHEP::mm;
//   
//   G4double fR=10.*CLHEP::mm;
//   G4double fr1=2.6/2*CLHEP::mm;
//   G4double fr2=5.5/2*CLHEP::mm;
//   G4double fH=18.85*CLHEP::mm-fr2*2;
//   G4double fH2=18.85*CLHEP::mm-6*CLHEP::mm; // distance between the tungstens
//   G4double fr0=4.0/2*CLHEP::mm;
//   
//   G4double fL1=3.*CLHEP::cm;   /// z-axis arms
//   G4double fL2=3.*CLHEP::cm;   /// x-axis arms
//   
//   G4double fSlabH=fr2;//1.6*CLHEP::mm;
//   
//   G4double fConxtL=18*CLHEP::mm;
//   G4double fConxtW=2*CLHEP::mm; // thickness 
//   G4double fConxtH=18.85*CLHEP::mm-6*CLHEP::mm-5.5*CLHEP::mm; //fSlabH*2;
//   
//   G4double fHoleR = 2*CLHEP::mm;
//   
//   G4VisAttributes* transparent0=new G4VisAttributes();
//   G4Colour  white(0.0, 1.0, 0.0, 0.01);
//   transparent0->SetColor(white); 
//   
//   const G4VisAttributes* transparent=&G4VisAttributes::GetInvisible ();
//   
//   G4VisAttributes* orange=new G4VisAttributes();
//   G4Colour  orge(1.0, 0.65, 0.0, 1);
//   orange->SetColor(orge);
//   
//   G4VisAttributes* red=new G4VisAttributes();
//   G4Colour  redd(0.0, 0.0, 1.0, 1);
//   red->SetColor(redd);
//   
//   G4VisAttributes* green=new G4VisAttributes();
//   G4Colour  gren(0.0, 1.0, 0.0, 1);
//   green->SetColor(gren);
//   
//   ////////////////////////////////////////////////////////////////////////
//   ////////////////////////////////////////////////////////////////////////
//   ////////////////////////////////////////////////////////////////////////
//   ////////////////////////////////////////////////////////////////////////
//   // 
//   G4Torus *sldCorner = new G4Torus("Corner",1.3*CLHEP::mm,2.75*CLHEP::mm,fR,0*CLHEP::degree,90*CLHEP::degree);
//   G4LogicalVolume*  logCorner = new G4LogicalVolume(sldCorner,fTungsten,fMaterial->GetName());
//   logCorner->SetVisAttributes(red);
//     G4RotationMatrix* xRotArm0 = new G4RotationMatrix; xRotArm0->rotateY(M_PI/2.*CLHEP::rad);
//   G4VPhysicalVolume* fCornerA = new G4PVPlacement(xRotArm0,           //no rotation
//                            G4ThreeVector(fX+fH/2,fY-fR,fZ-fR), //at (0,0,0)
//                            "pCornerA",                         //its name
//                            logCorner,                          //its logical volume
//                            ghostWorld,                               //its mother  volume
//                            false,                              //no boolean operation
//                            0);                                 //copy number
//   G4VPhysicalVolume*  fCornerB = new G4PVPlacement(xRotArm0,          //no rotation
//                            G4ThreeVector(fX-fH/2,fY-fR,fZ-fR), //at (0,0,0)
//                            "pCornerB",                         //its name
//                            logCorner,                          //its logical volume
//                            ghostWorld,                         	//its mother  volume
//                            false,                        	//no boolean operation
//                            0);                           	//copy number
// 
//   // arms along z-axis
//   G4Tubs *sldArm1 = new G4Tubs("Arm1",1.3*CLHEP::mm,2.75*CLHEP::mm,fL1/2,0*CLHEP::degree,360*CLHEP::degree);
//   G4LogicalVolume*  logArm1 = new G4LogicalVolume(sldArm1,fMaterial,fMaterial->GetName());
//   logArm1->SetVisAttributes(red);
//   //G4RotationMatrix* xRotArm1 = new G4RotationMatrix; xRotArm1->rotateX(M_PI/2.*CLHEP::rad);
//   G4VPhysicalVolume*  fArm1A = new G4PVPlacement(0,       //no rotation
//                            G4ThreeVector(fX+fH/2,fY,fZ-fR-fL1/2),             
//                            "pArm1A",         //its name
//                            logArm1,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
//   G4VPhysicalVolume*  fArm1B = new G4PVPlacement(0,       //no rotation
//                            G4ThreeVector(fX-fH/2,fY,fZ-fR-fL1/2),              
//                            "pArm1B",         //its name
//                            logArm1,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
// 
//   // arms along y-axis
//   G4Tubs *sldArm2 = new G4Tubs("Arm2",1.3*CLHEP::mm,2.75*CLHEP::mm,fL2/2,0*CLHEP::degree,360*CLHEP::degree);
//   G4LogicalVolume*  logArm2 = new G4LogicalVolume(sldArm2,fMaterial,fMaterial->GetName());
//   logArm2->SetVisAttributes(red);
//   G4RotationMatrix* yRotArm2 = new G4RotationMatrix; yRotArm2->rotateX(M_PI/2.*CLHEP::rad);
//   G4VPhysicalVolume*  fArm2A = new G4PVPlacement(yRotArm2,       //no rotation
//                            G4ThreeVector(fX+fH/2,fY-fR-fL2/2,fZ),              
//                            "pArm2A",         //its name
//                            logArm2,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
//   G4VPhysicalVolume*  fArm2B = new G4PVPlacement(yRotArm2,       //no rotation
//                            G4ThreeVector(fX-fH/2,fY-fR-fL2/2,fZ),              
//                            "pArm2B",         //its name
//                            logArm2,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
// 
//     
//   ////////////////////////////////////////////////////////////////////////
//   ////////////////////////////////////////////////////////////////////////
//   // 
//   G4Tubs *sldCornerSlab = new G4Tubs("CornerSlab",0*CLHEP::mm,fR-fr2,fSlabH,0*CLHEP::degree,90*CLHEP::degree);
//   G4LogicalVolume*  logSlab = new G4LogicalVolume(sldCornerSlab,fMaterial,fMaterial->GetName());
//   G4VPhysicalVolume*  fSlabA = new G4PVPlacement(xRotArm0,       //no rotation
//                            G4ThreeVector(fX+fH/2,fY-fR,fZ-fR),              //at (0,0,0)
//                            "pSlabA",         //its name
//                            logSlab,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
//   G4VPhysicalVolume*  fSlabB = new G4PVPlacement(xRotArm0,       //no rotation
//                            G4ThreeVector(fX-fH/2,fY-fR,fZ-fR),              //at (0,0,0)
//                            "pSlabB",         //its name
//                            logSlab,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
// 
//   // 
//   std::vector<G4TwoVector> points(5);
//   points[0]=G4TwoVector(-fr2,-fR);
//   points[1]=G4TwoVector(-fR,-fR);
//   points[2]=G4TwoVector(-fR,-fr2);
//   points[3]=G4TwoVector(-19.2*CLHEP::mm,-fr2);
//   points[4]=G4TwoVector(-fr2,-19.2*CLHEP::mm);
//   G4ExtrudedSolid *sldCornerSlabX = new G4ExtrudedSolid("CornerSlabX",points,fSlabH, G4TwoVector(0, 0), 1.0, G4TwoVector(0, 0), 1.0);
//   G4LogicalVolume*  logSlabX = new G4LogicalVolume(sldCornerSlabX,fMaterial,fMaterial->GetName());
//   G4VPhysicalVolume*  fSlabXA = new G4PVPlacement(xRotArm0,       //no rotation
//                            G4ThreeVector(fX+fH/2,fY+0,fZ+0),              //at (0,0,0)
//                            "pSlabXA",         //its name
//                            logSlabX,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
//   G4VPhysicalVolume*  fSlabXB = new G4PVPlacement(xRotArm0,       //no rotation
//                            G4ThreeVector(fX-fH/2,fY+0,fZ+0),              //at (0,0,0)
//                            "pSlabXB",         //its name
//                            logSlabX,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
//   
//   ////////////////////////////////////////////////////////////////////////
//   ////////////////////////////////////////////////////////////////////////
//   // Tungsten Stripes
//   std::vector<G4TwoVector> pointsx(4);
//   pointsx[0]=G4TwoVector(-19.2*CLHEP::mm,-fr2);
//   pointsx[1]=G4TwoVector(-fr2,-19.2*CLHEP::mm);
//   pointsx[2]=G4TwoVector(-fr2,-23.2*CLHEP::mm);
//   pointsx[3]=G4TwoVector(-23.2*CLHEP::mm,-fr2);
//   G4ExtrudedSolid *sldStripe = new G4ExtrudedSolid("StripeTungsten",pointsx,3*CLHEP::mm, G4TwoVector(0, 0), 1.0, G4TwoVector(0, 0), 1.0);
//   G4LogicalVolume*  logStripe = new G4LogicalVolume(sldStripe,fTungsten,fMaterial->GetName());
//   logStripe->SetVisAttributes(green);
//   G4VPhysicalVolume*  fStripeA = new G4PVPlacement(xRotArm0,       //no rotation
//                            G4ThreeVector(fX+fH/2,fY,fZ),              //at (0,0,0)
//                            "pStripeA",         //its name
//                            logStripe,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
//   G4VPhysicalVolume*  fStripeB = new G4PVPlacement(xRotArm0,       //no rotation
//                            G4ThreeVector(fX-fH/2,fY,fZ),              //at (0,0,0)
//                            "pStripeB",         //its name
//                            logStripe,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
// 
//   ////////////////////////////////////////////////////////////////////////
//   ////////////////////////////////////////////////////////////////////////
//   // Silicon Stripes Under Tungsten
//   std::vector<G4TwoVector> pointsxx(4);
//   pointsxx[0]=G4TwoVector(-23.2*CLHEP::mm,-fr2);
//   pointsxx[1]=G4TwoVector(-fr2,-23.2*CLHEP::mm);
//   pointsxx[2]=G4TwoVector(-fr2,-26.2*CLHEP::mm);
//   pointsxx[3]=G4TwoVector(-26.2*CLHEP::mm,-fr2);
//   G4ExtrudedSolid *sldStripeX = new G4ExtrudedSolid("StripeX",pointsxx,fSlabH, G4TwoVector(0, 0), 1.0, G4TwoVector(0, 0), 1.0);
//   G4LogicalVolume*  logStripeX = new G4LogicalVolume(sldStripeX,fMaterial,fMaterial->GetName());
//   //logStripeX->SetVisAttributes();
//   G4VPhysicalVolume*  fStripeXA = new G4PVPlacement(xRotArm0,       //no rotation
//                            G4ThreeVector(fX+fH/2,fY,fZ+0),              //at (0,0,0)
//                            "pStripeXA",         //its name
//                            logStripeX,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
//   G4VPhysicalVolume*  fStripeXB = new G4PVPlacement(xRotArm0,       //no rotation
//                            G4ThreeVector(fX-fH/2,fY,fZ+0),              //at (0,0,0)
//                            "pStripeXB",         //its name
//                            logStripeX,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
// 
//   ////////////////////////////////////////////////////////////////////////
//   ////////////////////////////////////////////////////////////////////////
//   //
//   std::vector<G4TwoVector> vertice(9);
//   vertice[0]=G4TwoVector(-10*CLHEP::mm,-fr2);
//   for (int i=1;i<6;i++){
//     G4double R0=10*CLHEP::mm-fr2;
//     G4double agl=CLHEP::pi/12;
//     vertice[i]=G4TwoVector(-10*CLHEP::mm+R0*sin(i*agl),-10*CLHEP::mm+R0*cos(i*agl));
//   }
//   vertice[6]=G4TwoVector(-fr2,-10*CLHEP::mm);
//   vertice[7]=G4TwoVector(-fr2,-23.2*CLHEP::mm);
//   vertice[8]=G4TwoVector(-23.2*CLHEP::mm,-fr2);
//   G4ExtrudedSolid *sldConxt0 = new G4ExtrudedSolid("StripeTungsten",vertice,fConxtH/2, G4TwoVector(0, 0), 1.0, G4TwoVector(0, 0), 1.0);
//   G4Tubs *sldHole = new G4Tubs("Hole",0,fHoleR,10*CLHEP::cm,0,CLHEP::twopi);
//   G4RotationMatrix* yRot = new G4RotationMatrix;  // Rotates X and Z axes only
//   yRot->rotateX(M_PI/2.);
//   yRot->rotateY(-M_PI/4.);
//   G4ThreeVector zTrans1( -15*CLHEP::mm,  -5*CLHEP::mm, 0);
//   G4ThreeVector zTrans2(  -5*CLHEP::mm, -15*CLHEP::mm, 0);
//   G4SubtractionSolid* sldConxt1=new G4SubtractionSolid("Connect1",sldConxt0,sldHole,yRot,zTrans1);
//   G4SubtractionSolid* sldConxt =new G4SubtractionSolid("Connect", sldConxt1,sldHole,yRot,zTrans2);
//   G4LogicalVolume*    logConxt =new G4LogicalVolume(sldConxt,fMaterial,"logConxt");
//   logConxt->SetVisAttributes(orange);
//   ///////////G4RotationMatrix* xRotConxt = new G4RotationMatrix; xRotConxt->rotateZ(M_PI/4.*CLHEP::rad);
//   G4VPhysicalVolume*   fConxt = new G4PVPlacement(xRotArm0, //xRotConxt,       //no rotation
//                            G4ThreeVector(fX+0,fY+0,fZ), //(fX-fR,fY-fR,fZ),              //at (0,0,0)
//                            "pConxt",         //its name
//                            logConxt,                      //its logical volume
//                            ghostWorld,                         //its mother  volume
//                            false,                        //no boolean operation
//                            0);                           //copy number
//   ////////////////////////////////////////////////////////////////////////
//   ////////////////////////////////////////////////////////////////////////
// 

}
