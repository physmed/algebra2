#ifndef LinearTrackLengthEstimator_hh
#define LinearTrackLengthEstimator_hh

#include "globals.hh"

using namespace std;

class LinearTrackLengthEstimator {
public:
  LinearTrackLengthEstimator();
  ~LinearTrackLengthEstimator();
  static inline LinearTrackLengthEstimator* GetInstance() { return theLinearTrackLengthEstimator; }
  G4double MuenRho(G4Material*,G4double);
  G4double MuRho(G4Material*,G4double);

private:
  static LinearTrackLengthEstimator* theLinearTrackLengthEstimator;
  G4double linLogLogInterpolate_muen(G4double,G4int);
  G4double linLogLogInterpolate_mu(G4double,G4int);

  vector< vector<G4double> > Energy;
  vector< vector<G4double> > MuenValue;
  vector< vector<G4double> > MuValue;
};

#endif
