#!/usr/bin/env python
import os,sys,time
from random import randint
from ROOT import TFile,TCanvas,TLegend,TH1F,gStyle,TH3D,TObject
from math import log,exp
from threading import Thread
import subprocess as sp
from multiprocessing import cpu_count
nthreads = cpu_count()

# ==================================================================
# Configuration
# ==================================================================
os.chdir(sys.argv[1])

A = {}
for l in open('run.conf').readlines():
  v = l.strip().split()  
  try: A[v[0]] = eval(v[1])
  except: A[v[0]] = v[1]

for i in A.keys(): print "%15s : %-s"%(str(i),str(A[i]))

# ==================================================================
# Manage the threads
# ==================================================================
nPhotons = int(A['NPHOTONS']/nthreads)

Processes = []

# ==================================================================
# From Edep to Dose
# ==================================================================
os.system('rm -f doses.root')
os.system('hadd -f doses.root t.*.root')
#os.system('rm -f t.*.root')
f = TFile('doses.root','update')
h = f.Get('Edep')

# ==================================================================
# Calibration factor
# ==================================================================
#                               Lambda|Calibration|HalfLife in days
pars = {'MicroSelectronV2Ir192':[1.108,   0.9663,   73.827],
        'FlexiSourceIr192':     [1.116,   0.9658,   73.827],
        'SelectSeedI125':       [0.917,   0.4872,   59.43],
        'OncoSeed6711I125':     [0.917,   0.4668,   59.43],
        'ProsperaSeed3631I125': [0.917,   0.4486,   59.43],
        'ProstaSeedSLI125':     [0.917,   0.4947,   59.43],
        'MBDCA_WGIr192':        [1.111,   0.9882,   73.827],
        'TheraSeed200Pd103':    [0.686,   0.6620,   17.0]}

factor = 0.1/(1e-3*h.GetXaxis().GetBinWidth(1)*h.GetYaxis().GetBinWidth(1)*h.GetZaxis().GetBinWidth(1)) # mm->cm ; mm3->cm3
factor *= 1e-2*1e3*A['AirKermaRate']*pars[A['MODEL']][0]/pars[A['MODEL']][1] # cGy->Gy ; g->kg

# if HDR
if '192' in A['MODEL']:
  TotalTime = 0.
  for i in file('dwells.dat','r'): TotalTime += eval(i.split()[3])
  factor *= TotalTime/3600.
else:
  factor *= float(len(file('dwells.dat','r').readlines()))*pars[A['MODEL']][2]*24./log(2)

# ==================================================================
# Calculate the errors
# ==================================================================
import numpy as np
### total number of photons simulated
Nx=int(A['NPHOTONS'])
N = h.GetBinContent(1) 
TE= h.GetBinContent(0)   
print "N=%i"%(N), " N=%i"%(Nx)
h.SetBinContent(0,0.)
h.SetBinContent(1,0.)
#
n = [h.GetNbinsX(),h.GetNbinsY(),h.GetNbinsZ()]
n2 = (n[0]+2)*(n[1]+2)*(n[2]+2)
d = h.GetArray()
d2 = h.GetSumw2().GetArray()
d.SetSize(n2)
D = np.array(d,np.float64)
d2.SetSize(n2)
D2 = np.array(d2,np.float64)
fN = h.fN
D /= N
h.Set(fN,D)
D2 /= N
D *= D
D2 -= D
D2 /= N-1.
h.GetSumw2().Set(fN,D2)
h.Scale(factor)
h.Write('doses',TObject.kOverwrite)

he = h.Clone('errors')
he.Reset()
d2 = h.GetSumw2().GetArray()
d2.SetSize(n2)
D2 = np.array(d2,np.float64)
he.Set(fN,np.sqrt(D2))
he.Divide(h)
he.Write()

# ==================================================================
# Save results in a 3ddose file
# ==================================================================
print 'creating 3ddose file'
f1 = file('doses.3ddose','w')
f1.write(' %d %d %d\n'%(n[0],n[1],n[2]))

a = np.zeros((n[0]+1),np.double)
for i in range(n[0]+1): a[i] = 0.1*h.GetXaxis().GetBinLowEdge(i+1)
np.savetxt(f1,a,fmt=' %7.5f',newline='')
f1.write('\n')
a = np.zeros((n[1]+1),np.double)
for i in range(n[1]+1): a[i] = 0.1*h.GetYaxis().GetBinLowEdge(i+1)
np.savetxt(f1,a,fmt=' %7.5f',newline='')
f1.write('\n')
a = np.zeros((n[2]+1),np.double)
for i in range(n[2]+1): a[i] = 0.1*h.GetZaxis().GetBinLowEdge(i+1)
np.savetxt(f1,a,fmt=' %7.5f',newline='')
f1.write('\n ')

for c in ['doses','errors']:
  a = f.Get(c).GetArray()
  a.SetSize(n2)
  data = np.array(a)
  data.resize(n[2]+2,n[1]+2,n[0]+2)
  data = data[1:-1,1:-1,1:-1]
  for data_slice in data: np.savetxt(f1,data_slice,fmt='%10.8f')

f1.close()
f.Close()
