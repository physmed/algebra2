#ifndef FlexiSourceIr192_hh
#define FlexiSourceIr192_hh 

#include "VSeed.hh"

class G4LogicalVolume;

class FlexiSourceIr192: public VSeed {
public:
  FlexiSourceIr192();
  ~FlexiSourceIr192(){;}

  void Construct();
  void PlaceYourself(G4VPhysicalVolume*,G4int);
  G4ThreeVector GetEmissionPosition();

private:
  static G4double energy[34];
  static G4double CDF[34];
  static G4double a[7];
  static G4double R[2];

  G4double outerSourceRadius;
  G4double heightSourceHalf;

  G4double innerCylinderRadius;
  G4double outerCylinderRadius;
  G4double heightCylinderHalf;

  G4double heightLCapHalf;

  G4double heightRCapCylHalf;
  G4double innerRCapConRadius;
  G4double heightRCapConHalf;

  G4double outerCableRadius;
  G4double heightCableHalf;
  G4double cable_pos;

  G4LogicalVolume* caps_log;
  G4LogicalVolume* core_log;
  G4LogicalVolume* gap_log;
  G4LogicalVolume* cable_log;
};

#endif
