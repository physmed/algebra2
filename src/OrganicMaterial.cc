#include "G4Material.hh"
#include "G4NistManager.hh"
#include "TH1F.h"
#include "OrganicMaterial.hh"
#include "G4String.hh"

OrganicMaterial::OrganicMaterial() {
  nMaterials = 0;
}

G4int OrganicMaterial::GetNumberOfMaterials() { return nMaterials; }

G4int OrganicMaterial::CreateBaseMaterial(G4String MaterialName) {
  // Definition of basic variables
  G4double density;
  G4Material* mat;
  G4int nel;

  G4NistManager* man = G4NistManager::Instance();
  G4Element* elH  = man->FindOrBuildElement( 1);
  G4Element* elC  = man->FindOrBuildElement( 6);
  G4Element* elN  = man->FindOrBuildElement( 7);
  G4Element* elO  = man->FindOrBuildElement( 8);
  G4Element* elNa = man->FindOrBuildElement(11);
  G4Element* elMg = man->FindOrBuildElement(12);
  G4Element* elP  = man->FindOrBuildElement(15);
  G4Element* elS  = man->FindOrBuildElement(16);
  G4Element* elCl = man->FindOrBuildElement(17);
  G4Element* elAr = man->FindOrBuildElement(18);
  G4Element* elK  = man->FindOrBuildElement(19);
  G4Element* elCa = man->FindOrBuildElement(20);
  G4Element* elI  = man->FindOrBuildElement(53);

  // Air (from NIST, dry, near see level, see Attix)
  if ( MaterialName == "Air" ) {
	  density = 0.001205;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=4);
    mat->AddElement(elC, 0.000124);
    mat->AddElement(elN, 0.755268);
    mat->AddElement(elO, 0.231781);
    mat->AddElement(elAr,0.012827);
    Float_t densities[13] = {0,0.0005,0.001,0.0012,0.0014,0.0016,0.0018,0.002,0.005,0.008,0.01,0.02,0.05};
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",12,densities) );

  // Water
  } else if ( MaterialName == "Water" ) {
    density = 1.;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=2);
    mat->AddElement(elH,nel=2);
    mat->AddElement(elO,nel=1);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",1,0.995,1.005) );

  // Prostate
  } else if ( MaterialName == "Prostate" ) {
    density = 1.04;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=8);
    mat->AddElement(elH, 0.105);
    mat->AddElement(elC, 0.089);
    mat->AddElement(elN, 0.025);
    mat->AddElement(elO, 0.774);
    mat->AddElement(elNa,0.002);
    mat->AddElement(elP, 0.001);
    mat->AddElement(elS, 0.002);
    mat->AddElement(elK, 0.002);
    MatList.push_back(mat);
    Float_t densities[15] = {0.90,0.92,0.94,0.96,0.98,1.00,1.01,1.02,1.03,1.035,1.04,1.045,1.05,1.07,1.09};
    DensityList.push_back( new TH1F(MaterialName,"",14,densities) );

  // Mean adipose
  } else if ( MaterialName == "Mean_adipose" ) {
    density = 0.95;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=7);
    mat->AddElement(elH, 0.114);
    mat->AddElement(elC, 0.598);
    mat->AddElement(elN, 0.007);
    mat->AddElement(elO, 0.278);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elS, 0.001);
    mat->AddElement(elCl,0.001);
    MatList.push_back(mat);
    Float_t densities[24] = {0.5,0.55,0.60,0.65,0.70,0.75,0.80,0.85,0.87,0.89,0.90,0.91,0.92,0.93,0.94,0.95,0.96,0.97,0.99,1.01,1.06,1.11,1.16,1.21};
    DensityList.push_back( new TH1F(MaterialName,"",23,densities) );

  // Mean gland
  } else if ( MaterialName == "Mean_gland" ) {
    density = 1.02;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=8);
    mat->AddElement(elH, 0.106);
    mat->AddElement(elC, 0.332);
    mat->AddElement(elN, 0.030);
    mat->AddElement(elO, 0.527);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.001);
    mat->AddElement(elS, 0.002);
    mat->AddElement(elCl,0.001);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  // Mean male soft tissue
  } else if ( MaterialName == "Mean_male_soft_tissue" ) {
    density = 1.03;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.105);
    mat->AddElement(elC, 0.256);
    mat->AddElement(elN, 0.027);
    mat->AddElement(elO, 0.602);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.002);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.002);
    mat->AddElement(elK, 0.002);
    MatList.push_back(mat);
    Float_t densities[21] = {0.8,0.85,0.9,0.92,0.94,0.96,0.98,1.00,1.01,1.02,1.03,1.04,1.05,1.06,1.07,1.08,1.1,1.13,1.15,1.20,1.25};
    DensityList.push_back( new TH1F(MaterialName,"",20,densities) );
  

  // Mean female soft tissue
  } else if ( MaterialName == "Mean_female_soft_tissue" ) {
    density = 1.02;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.106);
    mat->AddElement(elC, 0.315);
    mat->AddElement(elN, 0.024);
    mat->AddElement(elO, 0.547);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.002);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.001);
    mat->AddElement(elK, 0.002);
    MatList.push_back(mat);
    Float_t densities[21] = {0.8,0.85,0.9,0.92,0.94,0.96,0.98,1.00,1.01,1.02,1.03,1.04,1.05,1.06,1.07,1.08,1.1,1.13,1.15,1.20,1.25};
    DensityList.push_back( new TH1F(MaterialName,"",20,densities) );

  // Mean skin
  } else if ( MaterialName == "Mean_skin" ) {
    density = 1.09;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.100);
    mat->AddElement(elC, 0.204);
    mat->AddElement(elN, 0.042);
    mat->AddElement(elO, 0.645);
    mat->AddElement(elNa,0.002);
    mat->AddElement(elP, 0.001);
    mat->AddElement(elS, 0.002);
    mat->AddElement(elCl,0.003);
    mat->AddElement(elK, 0.001);

    MatList.push_back(mat);
    Float_t densities[12] = {1.04,1.05,1.06,1.07,1.08,1.09,1.1,1.11,1.12,1.13,1.14,1.15};
    DensityList.push_back( new TH1F(MaterialName,"",11,densities) );

  // Cortical bone
  } else if ( MaterialName == "Cortical_bone" ) {
    density = 1.92;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.034);
    mat->AddElement(elC, 0.155);
    mat->AddElement(elN, 0.042);
    mat->AddElement(elO, 0.435);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elMg,0.002);
    mat->AddElement(elP, 0.103);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCa,0.225);
    MatList.push_back(mat);
    Float_t densities[44] = {1.10,1.15,1.25,1.30,1.35,1.40,1.45,1.50,1.55,1.60,1.65,1.70,1.75,1.77,1.79,1.81,1.83,1.85,1.87,1.89,1.90,1.91,1.92,1.93,1.94,1.95,1.97,1.99,2.01,2.05,2.10,2.15,2.20,2.25,2.30,2.40,2.50,2.60,2.70,2.80,3.00,3.40,3.80,4.00};
    DensityList.push_back( new TH1F(MaterialName,"",43,densities) );


  // Femur bone
  } else if ( MaterialName == "Femur_bone" ) {
    density = 1.92;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.034);
    mat->AddElement(elC, 0.155);
    mat->AddElement(elN, 0.042);
    mat->AddElement(elO, 0.435);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elMg,0.002);
    mat->AddElement(elP, 0.103);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCa,0.225);
    MatList.push_back(mat);
    Float_t densities[44] = {1.10,1.15,1.25,1.30,1.35,1.40,1.45,1.50,1.55,1.60,1.65,1.70,1.75,1.77,1.79,1.81,1.83,1.85,1.87,1.89,1.90,1.91,1.92,1.93,1.94,1.95,1.97,1.99,2.01,2.05,2.10,2.15,2.20,2.25,2.30,2.40,2.50,2.60,2.70,2.80,3.00,3.40,3.80,4.00};
    DensityList.push_back( new TH1F(MaterialName,"",43,densities) );

  // Eye lens
  } else if ( MaterialName == "Eye_lens" ) {
    density = 1.07;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=8);
    mat->AddElement(elH, 0.096);
    mat->AddElement(elC, 0.195);
    mat->AddElement(elN, 0.057);
    mat->AddElement(elO, 0.646);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.001);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.001);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  // Lung (inflated)
  } else if ( MaterialName == "Lung" ) {
    density = 0.26;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.103);
    mat->AddElement(elC, 0.105);
    mat->AddElement(elN, 0.031);
    mat->AddElement(elO, 0.749);
    mat->AddElement(elNa,0.002);
    mat->AddElement(elP, 0.002);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.003);
    mat->AddElement(elK, 0.002);
    MatList.push_back(mat);
    Float_t densities[23] = {0.20,0.23,0.26,0.29,0.32,0.35,0.38,0.41,0.44,0.47,0.48,0.49,0.51,0.53,0.55,0.57,0.60,0.63,0.66,0.69,0.72,0.75,0.80};
    DensityList.push_back( new TH1F(MaterialName,"",22,densities) );

  // Liver
  } else if ( MaterialName == "Liver" ) {
    density = 1.06;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.102);
    mat->AddElement(elC, 0.139);
    mat->AddElement(elN, 0.030);
    mat->AddElement(elO, 0.716);
    mat->AddElement(elNa,0.002);
    mat->AddElement(elP, 0.003);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.002);
    mat->AddElement(elK, 0.003);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  // Heart
  } else if ( MaterialName == "Heart" ) {
    density = 1.05;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.104);
    mat->AddElement(elC, 0.139);
    mat->AddElement(elN, 0.029);
    mat->AddElement(elO, 0.718);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.002);
    mat->AddElement(elS, 0.002);
    mat->AddElement(elCl,0.002);
    mat->AddElement(elK, 0.003);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  // ContrastIodine
  } else if ( MaterialName == "ContrastIodine5%" ) {
    density = 1.0203;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0191);
    mat->AddElement(elH,0.1064);
    mat->AddElement(elI,0.0319);
    mat->AddElement(elN,0.0035);
    mat->AddElement(elO,0.8390);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  } else if ( MaterialName == "ContrastIodine10%" ) {
    density = 1.0406;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0375);
    mat->AddElement(elH,0.1011);
    mat->AddElement(elI,0.0626);
    mat->AddElement(elN,0.0069);
    mat->AddElement(elO,0.7918);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  } else if ( MaterialName == "ContrastIodine15%" ) {
    density = 1.0609;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0552);
    mat->AddElement(elH,0.0960);
    mat->AddElement(elI,0.0922);
    mat->AddElement(elN,0.0102);
    mat->AddElement(elO,0.7464);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  } else if ( MaterialName == "ContrastIodine20%" ) {
    density = 1.0812;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0556);
    mat->AddElement(elH,0.0911);
    mat->AddElement(elI,0.1206);
    mat->AddElement(elN,0.0133);
    mat->AddElement(elO,0.7027);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  } else if ( MaterialName == "ContrastIodine25%" ) {
    density = 1.1015;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0887);
    mat->AddElement(elH,0.0864);
    mat->AddElement(elI,0.1480);
    mat->AddElement(elN,0.0163);
    mat->AddElement(elO,0.6607);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  // Calcification
  } else if ( MaterialName == "Calcification" ) {
    density = 3.06;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=6);
    mat->AddElement(elH, 0.003);
    mat->AddElement(elC, 0.016);
    mat->AddElement(elN, 0.005);
    mat->AddElement(elO, 0.407);
    mat->AddElement(elP, 0.187);
    mat->AddElement(elCa,0.382);
    MatList.push_back(mat);
    Float_t densities[28] = {1.35,1.40,1.45,1.50,1.52,1.54,1.56,1.58,1.60,1.62,1.64,1.66,1.68,1.70,1.72,1.74,1.76,1.78,1.8,1.83,1.86,1.89,1.92,1.95,2.00,2.05,2.10,2.14};
    DensityList.push_back( new TH1F(MaterialName,"",27,densities) );

  // Stainless steel
  } else if ( MaterialName == "Stainless_steel" ) {
    density = 7.98;
    mat = new G4Material("Stainless_steel",density*CLHEP::g/CLHEP::cm3,nel=7);
    mat->AddElement(man->FindOrBuildElement(26),0.6197); // Fe
    mat->AddElement(man->FindOrBuildElement(24),0.1800); // Cr
    mat->AddElement(man->FindOrBuildElement(28),0.1400); // Ni
    mat->AddElement(man->FindOrBuildElement( 6),0.0003); // C
    mat->AddElement(man->FindOrBuildElement(42),0.0300); // Mo
    mat->AddElement(man->FindOrBuildElement(25),0.0200); // Mn
    mat->AddElement(man->FindOrBuildElement(14),0.0100); // Si
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,5.0,10.0) );

  } else {
    cerr << "OrganicMaterial; no match for : " << MaterialName << endl;
    return 0;
  }
  return 1;
}

G4Material* OrganicMaterial::Build(G4String MaterialName) {
  // Definition of basic variables
  G4double density;
  G4Material* mat;
  G4int nel;

  G4NistManager* man = G4NistManager::Instance();
  G4Element* elH  = man->FindOrBuildElement( 1);
  G4Element* elC  = man->FindOrBuildElement( 6);
  G4Element* elN  = man->FindOrBuildElement( 7);
  G4Element* elO  = man->FindOrBuildElement( 8);
  G4Element* elNa = man->FindOrBuildElement(11);
  G4Element* elMg = man->FindOrBuildElement(12);
  G4Element* elP  = man->FindOrBuildElement(15);
  G4Element* elS  = man->FindOrBuildElement(16);
  G4Element* elCl = man->FindOrBuildElement(17);
  G4Element* elAr = man->FindOrBuildElement(18);
  G4Element* elK  = man->FindOrBuildElement(19);
  G4Element* elCa = man->FindOrBuildElement(20);
  G4Element* elI  = man->FindOrBuildElement(53);

  // Air (from NIST, dry, near see level, see Attix)
  if ( MaterialName == "Air" ) {
	  density = 0.001205;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=4);
    mat->AddElement(elC, 0.000124);
    mat->AddElement(elN, 0.755268);
    mat->AddElement(elO, 0.231781);
    mat->AddElement(elAr,0.012827);
    Float_t densities[13] = {0,0.0005,0.001,0.0012,0.0014,0.0016,0.0018,0.002,0.005,0.008,0.01,0.02,0.05};
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",12,densities) );

  // Water
  } else if ( MaterialName == "Water" ) {
    density = 1.;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=2);
    mat->AddElement(elH,nel=2);
    mat->AddElement(elO,nel=1);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",1,0.995,1.005) );

  // Prostate
  } else if ( MaterialName == "Prostate" ) {
    density = 1.04;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=8);
    mat->AddElement(elH, 0.105);
    mat->AddElement(elC, 0.089);
    mat->AddElement(elN, 0.025);
    mat->AddElement(elO, 0.774);
    mat->AddElement(elNa,0.002);
    mat->AddElement(elP, 0.001);
    mat->AddElement(elS, 0.002);
    mat->AddElement(elK, 0.002);
    MatList.push_back(mat);
    Float_t densities[15] = {0.90,0.92,0.94,0.96,0.98,1.00,1.01,1.02,1.03,1.035,1.04,1.045,1.05,1.07,1.09};
    DensityList.push_back( new TH1F(MaterialName,"",14,densities) );

  // Mean adipose
  } else if ( MaterialName == "Mean_adipose" ) {
    density = 0.95;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=7);
    mat->AddElement(elH, 0.114);
    mat->AddElement(elC, 0.598);
    mat->AddElement(elN, 0.007);
    mat->AddElement(elO, 0.278);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elS, 0.001);
    mat->AddElement(elCl,0.001);
    MatList.push_back(mat);
    Float_t densities[24] = {0.5,0.55,0.60,0.65,0.70,0.75,0.80,0.85,0.87,0.89,0.90,0.91,0.92,0.93,0.94,0.95,0.96,0.97,0.99,1.01,1.06,1.11,1.16,1.21};
    DensityList.push_back( new TH1F(MaterialName,"",23,densities) );

  // Mean gland
  } else if ( MaterialName == "Mean_gland" ) {
    density = 1.02;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=8);
    mat->AddElement(elH, 0.106);
    mat->AddElement(elC, 0.332);
    mat->AddElement(elN, 0.030);
    mat->AddElement(elO, 0.527);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.001);
    mat->AddElement(elS, 0.002);
    mat->AddElement(elCl,0.001);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  // Mean male soft tissue
  } else if ( MaterialName == "Mean_male_soft_tissue" ) {
    density = 1.03;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.105);
    mat->AddElement(elC, 0.256);
    mat->AddElement(elN, 0.027);
    mat->AddElement(elO, 0.602);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.002);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.002);
    mat->AddElement(elK, 0.002);
    MatList.push_back(mat);
    Float_t densities[21] = {0.8,0.85,0.9,0.92,0.94,0.96,0.98,1.00,1.01,1.02,1.03,1.04,1.05,1.06,1.07,1.08,1.1,1.13,1.15,1.20,1.25};
    DensityList.push_back( new TH1F(MaterialName,"",20,densities) );
  

  // Mean female soft tissue
  } else if ( MaterialName == "Mean_female_soft_tissue" ) {
    density = 1.02;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.106);
    mat->AddElement(elC, 0.315);
    mat->AddElement(elN, 0.024);
    mat->AddElement(elO, 0.547);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.002);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.001);
    mat->AddElement(elK, 0.002);
    MatList.push_back(mat);
    Float_t densities[21] = {0.8,0.85,0.9,0.92,0.94,0.96,0.98,1.00,1.01,1.02,1.03,1.04,1.05,1.06,1.07,1.08,1.1,1.13,1.15,1.20,1.25};
    DensityList.push_back( new TH1F(MaterialName,"",20,densities) );

  // Mean skin
  } else if ( MaterialName == "Mean_skin" ) {
    density = 1.09;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.100);
    mat->AddElement(elC, 0.204);
    mat->AddElement(elN, 0.042);
    mat->AddElement(elO, 0.645);
    mat->AddElement(elNa,0.002);
    mat->AddElement(elP, 0.001);
    mat->AddElement(elS, 0.002);
    mat->AddElement(elCl,0.003);
    mat->AddElement(elK, 0.001);

    MatList.push_back(mat);
    Float_t densities[12] = {1.04,1.05,1.06,1.07,1.08,1.09,1.1,1.11,1.12,1.13,1.14,1.15};
    DensityList.push_back( new TH1F(MaterialName,"",11,densities) );

  // Cortical bone
  } else if ( MaterialName == "Cortical_bone" ) {
    density = 1.92;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.034);
    mat->AddElement(elC, 0.155);
    mat->AddElement(elN, 0.042);
    mat->AddElement(elO, 0.435);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elMg,0.002);
    mat->AddElement(elP, 0.103);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCa,0.225);
    MatList.push_back(mat);
    Float_t densities[44] = {1.10,1.15,1.25,1.30,1.35,1.40,1.45,1.50,1.55,1.60,1.65,1.70,1.75,1.77,1.79,1.81,1.83,1.85,1.87,1.89,1.90,1.91,1.92,1.93,1.94,1.95,1.97,1.99,2.01,2.05,2.10,2.15,2.20,2.25,2.30,2.40,2.50,2.60,2.70,2.80,3.00,3.40,3.80,4.00};
    DensityList.push_back( new TH1F(MaterialName,"",43,densities) );


  // Femur bone
  } else if ( MaterialName == "Femur_bone" ) {
    density = 1.92;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.034);
    mat->AddElement(elC, 0.155);
    mat->AddElement(elN, 0.042);
    mat->AddElement(elO, 0.435);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elMg,0.002);
    mat->AddElement(elP, 0.103);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCa,0.225);
    MatList.push_back(mat);
    Float_t densities[44] = {1.10,1.15,1.25,1.30,1.35,1.40,1.45,1.50,1.55,1.60,1.65,1.70,1.75,1.77,1.79,1.81,1.83,1.85,1.87,1.89,1.90,1.91,1.92,1.93,1.94,1.95,1.97,1.99,2.01,2.05,2.10,2.15,2.20,2.25,2.30,2.40,2.50,2.60,2.70,2.80,3.00,3.40,3.80,4.00};
    DensityList.push_back( new TH1F(MaterialName,"",43,densities) );

  // Eye lens
  } else if ( MaterialName == "Eye_lens" ) {
    density = 1.07;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=8);
    mat->AddElement(elH, 0.096);
    mat->AddElement(elC, 0.195);
    mat->AddElement(elN, 0.057);
    mat->AddElement(elO, 0.646);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.001);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.001);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  // Lung (inflated)
  } else if ( MaterialName == "Lung" ) {
    density = 0.26;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.103);
    mat->AddElement(elC, 0.105);
    mat->AddElement(elN, 0.031);
    mat->AddElement(elO, 0.749);
    mat->AddElement(elNa,0.002);
    mat->AddElement(elP, 0.002);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.003);
    mat->AddElement(elK, 0.002);
    MatList.push_back(mat);
    Float_t densities[23] = {0.20,0.23,0.26,0.29,0.32,0.35,0.38,0.41,0.44,0.47,0.48,0.49,0.51,0.53,0.55,0.57,0.60,0.63,0.66,0.69,0.72,0.75,0.80};
    DensityList.push_back( new TH1F(MaterialName,"",22,densities) );

  // Liver
  } else if ( MaterialName == "Liver" ) {
    density = 1.06;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.102);
    mat->AddElement(elC, 0.139);
    mat->AddElement(elN, 0.030);
    mat->AddElement(elO, 0.716);
    mat->AddElement(elNa,0.002);
    mat->AddElement(elP, 0.003);
    mat->AddElement(elS, 0.003);
    mat->AddElement(elCl,0.002);
    mat->AddElement(elK, 0.003);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  // Heart
  } else if ( MaterialName == "Heart" ) {
    density = 1.05;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=9);
    mat->AddElement(elH, 0.104);
    mat->AddElement(elC, 0.139);
    mat->AddElement(elN, 0.029);
    mat->AddElement(elO, 0.718);
    mat->AddElement(elNa,0.001);
    mat->AddElement(elP, 0.002);
    mat->AddElement(elS, 0.002);
    mat->AddElement(elCl,0.002);
    mat->AddElement(elK, 0.003);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  // ContrastIodine
  } else if ( MaterialName == "ContrastIodine5%" ) {
    density = 1.0203;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0191);
    mat->AddElement(elH,0.1064);
    mat->AddElement(elI,0.0319);
    mat->AddElement(elN,0.0035);
    mat->AddElement(elO,0.8390);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  } else if ( MaterialName == "ContrastIodine10%" ) {
    density = 1.0406;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0375);
    mat->AddElement(elH,0.1011);
    mat->AddElement(elI,0.0626);
    mat->AddElement(elN,0.0069);
    mat->AddElement(elO,0.7918);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  } else if ( MaterialName == "ContrastIodine15%" ) {
    density = 1.0609;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0552);
    mat->AddElement(elH,0.0960);
    mat->AddElement(elI,0.0922);
    mat->AddElement(elN,0.0102);
    mat->AddElement(elO,0.7464);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  } else if ( MaterialName == "ContrastIodine20%" ) {
    density = 1.0812;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0556);
    mat->AddElement(elH,0.0911);
    mat->AddElement(elI,0.1206);
    mat->AddElement(elN,0.0133);
    mat->AddElement(elO,0.7027);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  } else if ( MaterialName == "ContrastIodine25%" ) {
    density = 1.1015;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=5);
    mat->AddElement(elC,0.0887);
    mat->AddElement(elH,0.0864);
    mat->AddElement(elI,0.1480);
    mat->AddElement(elN,0.0163);
    mat->AddElement(elO,0.6607);
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,0.5,1.3) );

  // Calcification
  } else if ( MaterialName == "Calcification" ) {
    density = 3.06;
    mat = new G4Material(G4String("x_")+MaterialName,density*CLHEP::g/CLHEP::cm3,nel=6);
    mat->AddElement(elH, 0.003);
    mat->AddElement(elC, 0.016);
    mat->AddElement(elN, 0.005);
    mat->AddElement(elO, 0.407);
    mat->AddElement(elP, 0.187);
    mat->AddElement(elCa,0.382);
    MatList.push_back(mat);
    Float_t densities[28] = {1.35,1.40,1.45,1.50,1.52,1.54,1.56,1.58,1.60,1.62,1.64,1.66,1.68,1.70,1.72,1.74,1.76,1.78,1.8,1.83,1.86,1.89,1.92,1.95,2.00,2.05,2.10,2.14};
    DensityList.push_back( new TH1F(MaterialName,"",27,densities) );

  // Stainless steel
  } else if ( MaterialName == "Stainless_steel" ) {
    density = 7.98;
    mat = new G4Material("Stainless_steel",density*CLHEP::g/CLHEP::cm3,nel=7);
    mat->AddElement(man->FindOrBuildElement(26),0.6197); // Fe
    mat->AddElement(man->FindOrBuildElement(24),0.1800); // Cr
    mat->AddElement(man->FindOrBuildElement(28),0.1400); // Ni
    mat->AddElement(man->FindOrBuildElement( 6),0.0003); // C
    mat->AddElement(man->FindOrBuildElement(42),0.0300); // Mo
    mat->AddElement(man->FindOrBuildElement(25),0.0200); // Mn
    mat->AddElement(man->FindOrBuildElement(14),0.0100); // Si
    MatList.push_back(mat);
    DensityList.push_back( new TH1F(MaterialName,"",50,5.0,10.0) );

  } else {
    cerr << "OrganicMaterial; no match for : " << MaterialName << endl;
    return NULL;
  }
  return mat;
}