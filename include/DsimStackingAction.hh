#ifndef DsimStackingAction_hh
#define DsimStackingAction_hh

#include "G4TrackStatus.hh"
#include "G4UserStackingAction.hh"
#include "CtrBase.hh"

class G4Track;

class DsimStackingAction : public G4UserStackingAction {
public:
  DsimStackingAction(CtrBase* aCtr) {Ctr=aCtr;};
  ~DsimStackingAction(){};
  virtual G4ClassificationOfNewTrack ClassifyNewTrack(const G4Track*);
private:

  CtrBase* Ctr;
};

#endif
