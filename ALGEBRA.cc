//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//#include <stdio.h>      /* printf */
#include <stdlib.h>     /* getenv */

#include "G4RunManager.hh"
#include "G4UImanager.hh"

#include "DsimDetectorConstruction.hh"
#include "DsimPhysicsList.hh"
#include "DsimPrimaryGeneratorAction.hh"
#include "PhysicsList.hh"

#include "DsimRunAction.hh"
#include "DsimEventAction.hh"
#include "DsimSteppingAction.hh"
#include "DsimStackingAction.hh"

#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

#include <string.h>

//////////////////////////////////////////////////////////////////////////////////////////
// Controllers
#include "DsimPhsp.hh"
#include "DsimBrachy.hh"
//////////////////////////////////////////////////////////////////////////////////////////


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int main(int argc,char** argv) {

  ////////////////////////////////////////////////////////////////////
  // Controller Init

  if (argc<2){
      G4cout<<"Error: # of parameter is wrong! "<<G4endl;
      G4cout<<"Usage: "<<G4endl;
      G4cout<<"   Dsim command [script.mac]"<<G4endl;
      G4cout<<"   Valid commands: "<<G4endl;
      G4cout<<"       Brachy: perform brachy dose simulation "<<G4endl;
      G4cout<<"       Phsp:   perform phasespace simulation "<<G4endl;
      exit(0);
  }

  ////////////////////////////////////////////////////////////////////

  G4RunManager * runManager = new G4RunManager;
  G4String paraWorldName = "ParallelWorld";

  DsimDetectorConstruction* det;
  DsimPrimaryGeneratorAction* prim;
  runManager->SetUserInitialization(new PhysicsList(paraWorldName));

  ////////////////////////////////////////////////////////////////////

  CtrBase* aCtr=NULL;
  G4int thread = atoi(argv[2]);
  G4String model = argv[4]; // sourceTag

  if (G4String(argv[1])=="Phsp") {
    aCtr=new DsimPhsp(thread,model);
    aCtr->Initialize();
    //
    time_t seed;
    time(&seed);
    seed += atoll(argv[5]);
    CLHEP::RanecuEngine *theRanGenerator = new CLHEP::RanecuEngine;
    theRanGenerator->setSeed(seed);
    CLHEP::HepRandom::setTheEngine(theRanGenerator);
  }
  else if(G4String(argv[1])=="Brachy"){
      aCtr=new DsimBrachy(thread,model);
  }
  else{
      G4cout<<"Error: Command Not Recognized! "<<G4endl;
      G4cout<<"Valid commands: "<<G4endl;
      G4cout<<"   Brachy: perform brachy dose simulation "<<G4endl;
      G4cout<<"   Phsp:   perform phasespace simulation "<<G4endl;
      G4cout<<"     [$ Dsim Phsp 1 1000000 MicroSelectronV2Ir192 999999999] "<<G4endl;
      exit(0);
  }
  G4cout<<G4endl;

  ////////////////////////////////////////////////////////////////////

  det = new DsimDetectorConstruction(aCtr);

  //////////////////////////
  //// Determine if a parallel world should be registered
  ////if ( true ) {
  ////  paraWorldName = "ParallelWorld";
  ////  det->RegisterParallelWorld( new ParallelWorldConstruction(paraWorldName) );
  ////}///////////////////////
  ////aCtr->setup_parallel();
  det->RegisterParallelWorld( new ParallelWorldConstruction(paraWorldName) );

  runManager->SetUserInitialization(det);
  runManager->SetUserAction(prim = new DsimPrimaryGeneratorAction(aCtr));

  runManager->SetUserAction(new DsimSteppingAction(aCtr));
  runManager->SetUserAction(new DsimEventAction(aCtr));
  runManager->SetUserAction(new DsimRunAction(aCtr));
  runManager->SetUserAction(new DsimStackingAction(aCtr));

  runManager -> Initialize();

#ifdef G4VIS_USE
  G4VisManager* visManager = new G4VisExecutive;
  visManager->Initialize();
#endif

  ////////////////////////////////////////////////////////////////////
  G4int nPhotons = atoi(argv[3]);
	// nPhotons<0 is a trick to execute run.mac and vis.mac
  if (nPhotons<0)   // batch mode using script macro
    {
     G4String command = "/control/execute ";
     G4String fileName = "run.mac";
     G4UImanager::GetUIpointer()->ApplyCommand(command+fileName);
    }
  ////////////////////////////////////////////////////////////////////
  else           // define UI terminal for interactive mode
  {
	  runManager->BeamOn(nPhotons);
/*
#ifdef G4UI_USE
      G4UIExecutive * ui = new G4UIExecutive(argc,argv);
      ui->SessionStart();
      delete ui;
#endif
*/
    }

  ////////////////////////////////////////////////////////////////////
  delete runManager;
#ifdef G4VIS_USE
  delete visManager;
#endif

  return 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
