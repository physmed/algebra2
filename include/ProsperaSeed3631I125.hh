#ifndef ProsperaSeed3631I125_hh
#define ProsperaSeed3631I125_hh

#include "VSeed.hh"

class G4LogicalVolume;

class ProsperaSeed3631I125: public VSeed {
public:
  ProsperaSeed3631I125();
  ~ProsperaSeed3631I125(){;}

  void Construct();
  void PlaceYourself(G4VPhysicalVolume*,G4int);
  G4ThreeVector GetEmissionPosition();

private:
  static G4double energy[5];
  static G4double CDF[5];
  static G4double a[7];
  static G4double R[2];
  G4double VolCDF;

  G4double TiInRadius;
  G4double TiOutRadius;
  G4double TiZHalfLength;
  G4double CoatingThickness;
  G4double PolyOutRadius;
  G4double PolyZHalfLength;
  G4double AirSphereRadius;
  G4double PolySpherePos;
  G4double PolySpherePos2;
  G4double MarkerSpherePos;

  G4LogicalVolume* caps_log;
  G4LogicalVolume* gap_log;
  G4LogicalVolume* marker_log;
  G4LogicalVolume* polysphere_log;
};

#endif
