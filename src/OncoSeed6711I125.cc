//################################################################  
// http://vali.physics.carleton.ca/clrp/seed_database/I125/OncoSeed_6711/
//################################################################  
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4UnionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4RandomDirection.hh"
#include "Randomize.hh"
#include "OncoSeed6711I125.hh"

G4double OncoSeed6711I125::energy[4] = {27.2*CLHEP::keV,27.47*CLHEP::keV,31.0*CLHEP::keV,35.49*CLHEP::keV};

G4double OncoSeed6711I125::CDF[4] = {0.2721,0.7777,0.953,1.0};

G4double OncoSeed6711I125::a[7] = {5.9638e-4,-1.3360e-2,1.1634,4.0710e-1,-5.5487e-3,1.7421e-3,4.4080e-1};

G4double OncoSeed6711I125::R[2] = {0.05,10.};

OncoSeed6711I125::OncoSeed6711I125():VSeed("OncoSeed6711I125"){
  VSeed::numentries = 4;
  VSeed::energy = OncoSeed6711I125::energy;
  VSeed::CDF    = OncoSeed6711I125::CDF;
  VSeed::a      = OncoSeed6711I125::a;
  VSeed::R      = OncoSeed6711I125::R;
  VSeed::Calibration = 0.4668; // +/- 0.0007
  VSeed::Lambda = 0.917;
  VSeed::HalfLife = 59.43*24; // [h]

  //################################################################  
  // Seed dimensions
  //################################################################  
  TiInRadius = 350.*CLHEP::um;
  TiOutRadius = 400.*CLHEP::um;
  TiZHalfLength = (3.5/2)*CLHEP::mm;
  CoatingThickness = 0.1*CLHEP::um;
  AgOutRadius = 250.*CLHEP::um;
  AgZHalfLength = (3.0/2)*CLHEP::mm;

  VSeed::OutRadius = TiOutRadius;
  VSeed::ZHalfLength = TiZHalfLength + TiOutRadius;
  VSeed::ActiveLength = 2*AgZHalfLength;
}

void OncoSeed6711I125::Construct() {
  //###############################################################                                                                                                           
  // Define the seed materials
  //###############################################################
  G4int nel;
  G4double density, a, z;
  G4String name, symbol;

  G4Element* el_I  = new G4Element(name="I", symbol="I", z=53.0,a=126.90447*CLHEP::g/CLHEP::mole);
  G4Element* el_Ag = new G4Element(name="Ar",symbol="Ag",z=47.0,a=107.868*CLHEP::g/CLHEP::mole);
  G4Element* el_Cl = new G4Element(name="Cl",symbol="Cl",z=17.0,a=35.453*CLHEP::g/CLHEP::mole);

  G4Material* Air = G4NistManager::Instance()->FindOrBuildMaterial("G4_AIR");
  G4Material* mat_Ti = G4NistManager::Instance()->FindOrBuildMaterial("G4_Ti");
  G4Material* mat_Ag = G4NistManager::Instance()->FindOrBuildMaterial("G4_Ag");

/*
  G4Material* matAgCl = new G4Material(name="AgCl",density=5.56*CLHEP::g/CLHEP::cm3,nel=2);
  matAgCl->AddElement(el_Ag,0.7526);
  matAgCl->AddElement(el_Cl,0.2474);

  G4Material* matAgI = new G4Material(name="AgI",density=5.68*CLHEP::g/CLHEP::cm3,nel=2);
  matAgI->AddElement(el_Ag,0.4595);
  matAgI->AddElement(el_I,0.5405);
*/

  G4Material* mat_AgIAgClAgI = new G4Material(name="AgIAgClAgI",density=5.64*CLHEP::g/CLHEP::cm3,nel=3);
  mat_AgIAgClAgI->AddElement(el_Ag,0.557);
  mat_AgIAgClAgI->AddElement(el_Cl,0.083);
  mat_AgIAgClAgI->AddElement(el_I,0.360);

  //################################################################ 
  // Define the seed geometry
  //################################################################

  // Outer titanium capsule
  G4Tubs* TiCyl_vol = new G4Tubs("TiCyl_vol",TiInRadius,TiOutRadius,TiZHalfLength,0.,CLHEP::twopi);
  G4Sphere* RCap = new G4Sphere("RCap",0,TiOutRadius,0.,CLHEP::twopi,0,CLHEP::halfpi);
  G4Sphere* LCap = new G4Sphere("LCap",0,TiOutRadius,0.,CLHEP::twopi,CLHEP::halfpi,CLHEP::pi);
  G4UnionSolid* Vol1 = new G4UnionSolid("Vol1",TiCyl_vol,RCap,0,G4ThreeVector(0.,0.,TiZHalfLength));
  G4UnionSolid* caps = new G4UnionSolid("caps",Vol1,LCap,0,G4ThreeVector(0.,0.,-TiZHalfLength));
  VSeed::OuterShell = caps;

  caps_log = new G4LogicalVolume(caps,mat_Ti,"caps_log",0,0,0);
  G4VisAttributes* caps_att = new G4VisAttributes(G4Colour(1,1,0.7,0.5));
  caps_att->SetVisibility(true);
  caps_att->SetForceSolid(true);
  caps_log->SetVisAttributes(caps_att);

  // Air gap cylinder
  G4Tubs* AirCyl_vol = new G4Tubs("AirCyl_vol",0.,TiInRadius,TiZHalfLength,0.,CLHEP::twopi);
  gap_log = new G4LogicalVolume(AirCyl_vol,Air,"gap_log",0,0,0);
  G4VisAttributes* gap_att = new G4VisAttributes(G4Colour(0.9,0.9,1,0.5));
  gap_att->SetVisibility(true);
  gap_att->SetForceSolid(true);
  gap_log->SetVisAttributes(gap_att);

  // Silver coating
  G4Tubs* coating_vol = new G4Tubs("Coating_Vol",0,AgOutRadius+CoatingThickness,AgZHalfLength+CoatingThickness,0.,CLHEP::twopi);
  coating_log = new G4LogicalVolume(coating_vol,mat_AgIAgClAgI,"coating_log",0,0,0);
  G4VisAttributes* coating_att = new G4VisAttributes(G4Colour(0.7,0.7,0.8,0.3));
  coating_att->SetVisibility(true);
  coating_att->SetForceSolid(true);
  coating_log->SetVisAttributes(coating_att);

  // Inner silver cylinder
  G4Tubs* AgCyl_vol = new G4Tubs("AgCyl_vol",0,AgOutRadius,AgZHalfLength,0.,CLHEP::twopi);
  core_log = new G4LogicalVolume(AgCyl_vol,mat_Ag,"core_log",0,0,0);
  G4VisAttributes* core_att = new G4VisAttributes(G4Colour(0.2,0.2,1.0));
  core_att->SetVisibility(true);
  core_att->SetForceSolid(true);
  core_log->SetVisAttributes(core_att);

  VolCDF = (CLHEP::pi*(AgOutRadius+CoatingThickness)*(AgOutRadius+CoatingThickness)*CoatingThickness)/(coating_vol->GetCubicVolume()-AgCyl_vol->GetCubicVolume());
}

void OncoSeed6711I125::PlaceYourself(G4VPhysicalVolume* parent,G4int copyNo) {
  if ( !VSeed::OuterShell ) Construct();
  new G4PVPlacement(0,G4ThreeVector(),"caps_phys",caps_log,parent,false,copyNo);
  G4PVPlacement* gap_phys = new G4PVPlacement(0,G4ThreeVector(),"gap_phys",gap_log,parent,false,copyNo);
  G4PVPlacement* coating_phys = new G4PVPlacement(0,G4ThreeVector(),"coating_phys",coating_log,gap_phys,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(),"core_phys",core_log,coating_phys,false,copyNo);
}

G4ThreeVector OncoSeed6711I125::GetEmissionPosition() {
  enum {leftcap,rightcap,center};
  G4double u = G4UniformRand();
  G4ThreeVector v;
  G4int i=2;
  if ( u<VolCDF ) i=0;
  else if ( u<2*VolCDF ) i=1;

  switch (i) {
    case leftcap:
      v = SelectPointOnDisk(AgOutRadius+CoatingThickness);
      v.setZ(-AgZHalfLength-G4UniformRand()*CoatingThickness);
      break;
    case rightcap:
      v = SelectPointOnDisk(AgOutRadius+CoatingThickness);
      v.setZ(AgZHalfLength+G4UniformRand()*CoatingThickness);
      break;
    case center:
      v = SelectPointOnCylinder(sqrt(AgOutRadius*AgOutRadius+G4UniformRand()*CoatingThickness*(AgOutRadius+CoatingThickness)),AgZHalfLength);
      // r = sqrt(R1^2+u*(R2^2-R1^2)) = sqrt(R*R+u*d*(R+d))
      break;
    default:
      break;
  }
  return v;
}
