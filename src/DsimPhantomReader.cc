#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4PVParameterised.hh"
#include "G4PhantomParameterisation.hh"

#include "G4Region.hh"
#include "G4ProductionCuts.hh"

#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

#include "G4ios.hh"
#include "G4StepPoint.hh"
#include "G4ParticleDefinition.hh"
#include "G4TrackStatus.hh"
#include "G4ParticleDefinition.hh"
#include "G4Gamma.hh"

#include "G4Color.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"

#include "DsimPhantomReader.hh"
#include "OrganicMaterial.hh"

#include <fstream>

#include "G4String.hh"

DsimPhantomReader::DsimPhantomReader(){
  MAT=new OrganicMaterial;
}

DsimPhantomReader::~DsimPhantomReader(){}

void DsimPhantomReader::LoadFile(G4String filename)
{
  fstream thefile(filename,ios::in);
  if( !thefile.is_open() ) { 
    G4cout << "Error: Phantom file <"<<filename<<"> could not be opened." << G4endl; 
    exit(1); 
  }else{
    G4cout << "Reading Phantom : <"<<filename<<"> ..." << G4endl; 
  }
  
  G4int NbMat; thefile >> NbMat;G4cout<<"> "<<NbMat<<G4endl;
  G4String MatName;
  for (int i=0;i<NbMat;i++) {
    thefile  >> MatName;
    fVectMatName.push_back(MatName);
    G4cout<<"> "<<MatName<<G4endl;
  }
  
  for (int i=0;i<NbMat;i++){
    G4Material* mat=MAT->Build(fVectMatName[i]);
    if (mat==NULL) {
      G4cout<<"Error: Material <"<<fVectMatName[i]<<"> Not Available. "<<G4endl;
      exit(1);
    }
    G4cout<<mat->GetName()<<" "<<mat->GetDensity()/(g/cm3)<<" --> created"<<G4endl;
    AllMaterials.push_back(mat);
  }
	
  double dummy;
  for (int i=0;i<NbMat;i++) {thefile>>dummy;}
  thefile >> NbinsX >> NbinsY >> NbinsZ;
  G4cout<<"> "<<NbinsX<<" "<<NbinsY<<" "<<NbinsZ<<G4endl;
  
  //G4cout<<"> ";
  vector<G4double> xx;G4double x;
  for (int i=0;i<NbinsX+1;i++){
    thefile>>x;xx.push_back(x);//G4cout<<x<<" ";
  }
  for (int i=0;i<NbinsX;i++){
    fVectXX.push_back(xx[i]/2+xx[i+1]/2);
  }
  //G4cout<<G4endl;
  
  //G4cout<<"> ";
  vector<G4double> yy;G4double y;
  for (int i=0;i<NbinsY+1;i++){
    thefile>>y;yy.push_back(y);//G4cout<<y<<" ";
  }
  for (int i=0;i<NbinsY;i++){
    fVectYY.push_back(yy[i]/2+yy[i+1]/2);
  }
  //G4cout<<G4endl;
  
  //G4cout<<"> ";
  vector<G4double> zz;G4double z;
  for (int i=0;i<NbinsZ+1;i++){
    thefile>>z;zz.push_back(z);//G4cout<<z<<" ";
  }
  for (int i=0;i<NbinsZ;i++){
    fVectZZ.push_back(zz[i]/2+zz[i+1]/2);
  }
  //G4cout<<G4endl;
  
  cout<<"=================================="<<endl;
  
  string buffer;
  buffer.reserve(1000);
  if( thefile.peek()!='\n' || thefile.peek()!='\r' ) getline(thefile, buffer);

  //vector<G4Material*> BaseMaterials = mat->MatList;
  //vector<TH1F*> hDensity = mat->DensityList;
  materialIDs = new size_t[NbinsX*NbinsY*NbinsZ];
  //map<G4String,G4int> AllMaterialsNames;

  int p=0;
  for (G4int k=0;k<NbinsZ;k++) {
    for (G4int j=0;j<NbinsY;j++) {
      getline(thefile,buffer);
      for (G4int i=0;i<NbinsX;i++) {
        char M[] = {'\0','\0'};
        M[0] = buffer.c_str()[i];
        materialIDs[p] = atoi(M)-1; // egsphant materials start at 1
        p++;
      }
    }
    getline(thefile,buffer);
  }

  /*
  double density;
  G4String name;

  p=0;
  for (G4int k=0;k<NbinsZ;k++) {
    for (G4int j=0;j<NbinsY;j++) {
      getline(thefile, buffer);
      stringstream Array(buffer);
      for (G4int i=0;i<NbinsX;i++) {
        Array >> density;
        int id = materialIDs[p];
        TH1F *h = hDensity[id];
        h->Fill(density);
        //density = h->GetXaxis()->GetBinCenter( h->FindBin(density) );
        name = Form("m_%d_%d",id,int(1e4*density));
        if ( AllMaterialsNames.find(name)!=AllMaterialsNames.end() ) id = AllMaterialsNames[name];
        else {
		  G4cout<<"::::::::"<<density<<G4endl;
          AllMaterials.push_back(man->BuildMaterialWithNewDensity(name,BaseMaterials[id]->GetName(),density*g/cm3));
          id = AllMaterials.size()-1;
          AllMaterialsNames[name] = id;
        }
        materialIDs[p] = id;
        p++;
      }
    }
    getline(thefile,buffer);
  }*/
  
  /*
  G4int den;
  for (int i=0;i<NbinsZ*NbinsY*NbinsZ;i++){
    thefile>>den;fVectDensity.push_back(den);
  }*/
  ////////////////////////////////////////////////////////////////////////////////////
  //// now everything is in milimeter
  Xmin = fVectXX[0]*CLHEP::cm;
  Xmax = fVectXX[NbinsX-1]*CLHEP::cm;
  Ymin = fVectYY[0]*CLHEP::cm;
  Ymax = fVectYY[NbinsY-1]*CLHEP::cm;
  Zmin = fVectZZ[0]*CLHEP::cm;
  Zmax = fVectZZ[NbinsZ-1]*CLHEP::cm;
  halfX=(fVectXX[1]-fVectXX[0])/2*CLHEP::cm;
  halfY=(fVectYY[1]-fVectYY[0])/2*CLHEP::cm;
  halfZ=(fVectZZ[1]-fVectZZ[0])/2*CLHEP::cm;
  ////////////////////////////////////////////////////////////////////////////////////
}

void DsimPhantomReader::Construct(G4LogicalVolume* mother_log)
{
  ////////////////////////////////////////////////////////////////////////////////////
  G4cout<<"Building Phantom:"<<G4endl;
  G4cout<<"{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{"<<G4endl;
  G4cout<<"NbinsX:"<<NbinsX<<" "<<"NbinsY:"<<NbinsY<<" "<<"NbinsZ:"<<NbinsZ<<G4endl;
  G4cout<<"Xmin:"<<Xmin<<"*mm Xmax:"<<Xmax<<"*mm"<<G4endl;
  G4cout<<"Ymin:"<<Ymin<<"*mm Ymax:"<<Ymax<<"*mm"<<G4endl;
  G4cout<<"Zmin:"<<Zmin<<"*mm Zmax:"<<Zmax<<"*mm"<<G4endl;
  G4cout<<"VoxelX:"<<halfX*2<<"*mm "<<"VoxelY:"<<halfY*2<<"*mm "<<"VoxelZ:"<<halfZ*2<<"*mm"<<G4endl;
  G4cout<<"Center:"<<GetCenter()<<" * mm"<<G4endl;
  G4cout<<"}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}"<<G4endl;
  ////////////////////////////////////////////////////////////////////////////////////
  G4Box* phantomVoxel_vol = new G4Box("phantomVoxel_vol",halfX,halfY,halfZ);
  phantomVoxel_log = new G4LogicalVolume(phantomVoxel_vol,
					 G4NistManager::Instance()->FindOrBuildMaterial("G4_WATER"),
					 "phantomVoxel_log",0,0,0);
  G4VisAttributes* phantomVoxel_att = new G4VisAttributes();
  phantomVoxel_att->SetVisibility(false);
  phantomVoxel_log->SetVisAttributes(phantomVoxel_att);
  
  cont_vol = new G4Box("cont_vol",NbinsX*halfX,NbinsY*halfY,NbinsZ*halfZ);
  cont_log = new G4LogicalVolume(cont_vol,
				 G4NistManager::Instance()->FindOrBuildMaterial("G4_WATER"),
				 "cont_log",0,0,0);
  cont_phys = new G4PVPlacement(0,G4ThreeVector(),cont_log,"cont_phys",mother_log,false,0);
  G4VisAttributes* cont_att = new G4VisAttributes(G4Colour(0,0,1));
  cont_att->SetVisibility(true);
  cont_log->SetVisAttributes(cont_att);
  
  // Parameterisation of the human phantom
  param = new G4PhantomParameterisation();
  param->SetVoxelDimensions(halfX,halfY,halfZ);
  param->SetNoVoxel(NbinsX,NbinsY,NbinsZ);
  param->SetMaterials(AllMaterials);
  param->SetMaterialIndices(materialIDs);
  param->SetSkipEqualMaterials(true);
  param->BuildContainerSolid(cont_phys);
  param->CheckVoxelsFillContainer(cont_vol->GetXHalfLength(),cont_vol->GetYHalfLength(),cont_vol->GetZHalfLength());
  
  phantom_phys = new G4PVParameterised("phantom_phys",phantomVoxel_log,cont_log,kZAxis,param->GetNoVoxel(),param);
  phantom_phys->SetRegularStructureId(1);
}