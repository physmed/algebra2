#ifndef DsimDwellList_H
#define DsimDwellList_H 1

#include "globals.hh"
#include <vector>

using namespace std;

class G4String;

class DsimDwellList
{

public:
     DsimDwellList(G4String filename);
    ~DsimDwellList();
    
    void Dump(); // dump 
    
    // randomly select according to weights (and dwell time)
    G4int Select(G4double &x,G4double &y,G4double &z,G4double &u,G4double &v,G4double &w);

private:
    vector<G4double> fVectX; // bin center
    vector<G4double> fVectY; // bin center
    vector<G4double> fVectZ; // bin center
    vector<G4double> fVectU; // bin center
    vector<G4double> fVectV; // bin center
    vector<G4double> fVectW; // bin center
    vector<G4double> fVectT; // bin center
    
    vector<G4double> fWeights; // cumulative weights, normalized to 1.00
    
    G4String fFilename;
   
};

#endif