#ifndef DsimPhspace_H
#define DsimPhspace_H 1

#include "globals.hh"
#include <vector>
#include "TTree.h"
#include "TFile.h"

using namespace std;

class G4String;
class TTree;
class TFile;

class DsimPhspace
{

public:
     DsimPhspace(G4String);
    ~DsimPhspace();
    
public:
    void ConnectFile(G4String);
    G4int Next();
  
public:
  TTree*     t;
  Float_t    x,y,z,theta,phi,E;
  Int_t      N;
  
  G4int      Nphoton;
  G4double   TotalE;
 
private:
  TFile*     f;
  
  
};

#endif