#ifndef DsimEventAction_hh
#define DsimEventAction_hh

#include "G4UserEventAction.hh"

#include "CtrBase.hh"

class DsimEventAction : public G4UserEventAction {
public:
  DsimEventAction(CtrBase*);
  ~DsimEventAction();
  void BeginOfEventAction(const G4Event*);
  void EndOfEventAction(const G4Event*);

private:

  CtrBase* Ctr;

};

#endif
