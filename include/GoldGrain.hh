#ifndef GoldGrain_hh
#define GoldGrain_hh

#include "globals.hh"
#include "G4ThreeVector.hh"

class G4LogicalVolume;
class G4VPhysicalVolume;

class GoldGrain {
public:
  GoldGrain();
  ~GoldGrain() {}

  void PlaceYourself(G4VPhysicalVolume*,G4int);
  G4double OutRadius;
  G4double ZHalfLength;  

private:
  G4String      GoldGrainName;
  G4LogicalVolume* grain_log;
};

#endif
