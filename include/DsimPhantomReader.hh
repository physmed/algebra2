#ifndef DsimPhantomReader_H
#define DsimPhantomReader_H 1

#include "globals.hh"
#include <vector>

#include "G4ThreeVector.hh"

using namespace std;

class G4String;
class G4PhantomParameterisation;
class G4Material;
class G4PVParameterised;
class G4LogicalVolume;
class OrganicMaterial;

class DsimPhantomReader
{

public: 
     DsimPhantomReader();
    ~DsimPhantomReader();
    
public:
    void LoadFile(G4String filename);
    void Construct(G4LogicalVolume*);
    
    G4ThreeVector GetCenter(){
      return G4ThreeVector((Xmin+Xmax)/2,(Ymin+Ymax)/2,(Zmin+Zmax)/2);
    };
    
    G4PhantomParameterisation* GetParam() {return param;}

private:
    vector<G4String> fVectMatName; // material names
    
    vector<G4double> fVectXX; // bin center
    vector<G4double> fVectYY; // bin center
    vector<G4double> fVectZZ; // bin center
    vector<G4int>    fVectorIndex;
    vector<G4double> fVectDensity;
    
    OrganicMaterial* MAT;
    
    G4PhantomParameterisation* param;
    size_t* materialIDs;
    vector<G4Material*> AllMaterials;
    G4PVParameterised* phantom_phys;
    G4LogicalVolume* phantomVoxel_log;

    G4Box* cont_vol;
    G4LogicalVolume* cont_log;
    G4VPhysicalVolume* cont_phys;

public:
    G4double Xmax,Xmin,halfX;
    G4double Ymax,Ymin,halfY;
    G4double Zmin,Zmax,halfZ;
    G4int NbinsX, NbinsY, NbinsZ;
   
};

#endif
