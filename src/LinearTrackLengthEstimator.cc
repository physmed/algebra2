#include <stdlib.h>     /* getenv */
#include "G4Material.hh"
#include <algorithm>
#include "LinearTrackLengthEstimator.hh"
#include "TFile.h"

LinearTrackLengthEstimator *LinearTrackLengthEstimator::theLinearTrackLengthEstimator=NULL;

LinearTrackLengthEstimator::~LinearTrackLengthEstimator() {
  theLinearTrackLengthEstimator=NULL;
}

LinearTrackLengthEstimator::LinearTrackLengthEstimator() {
  theLinearTrackLengthEstimator=this;

  G4String MuenFile = Form("%s/data/Muen.dat",getenv("ALGEBRADIR"));
  G4cout << "Muen file: " << MuenFile << G4endl;
  ifstream fin(MuenFile);

  G4int i=0,n,N;
  G4double muEnergy,muenValue,muValue;

  while ( fin >> n >> N ) {
    Energy.push_back( vector<double>() );
    MuValue.push_back( vector<double>() );
    MuenValue.push_back( vector<double>() );
    n=0;
    while (n<N) {
      fin >> muEnergy >> muValue >> muenValue;
      //G4cout<<muEnergy<<"\t"<<muValue<<"\t"<<muenValue<<G4endl;
      Energy[i].push_back( muEnergy );
      MuValue[i].push_back( muValue );
      MuenValue[i].push_back( muenValue );
      n++;
    }
    i++;
  }

  fin.close();
}

G4double LinearTrackLengthEstimator::MuenRho(G4Material* material, G4double energy) {
  const G4ElementVector *elementVector = material->GetElementVector();
  const G4double* weightFrac = material->GetFractionVector();
  G4int numElements = material->GetNumberOfElements();
  G4double Muen = 0.;
  for (G4int i=0;i<numElements;i++) Muen += weightFrac[i]*linLogLogInterpolate_muen(energy,G4int((*elementVector)[i]->GetZ()));
  return Muen;
}

G4double LinearTrackLengthEstimator::MuRho(G4Material* material, G4double energy) {
  const G4ElementVector *elementVector = material->GetElementVector();
  const G4double* weightFrac = material->GetFractionVector();
  G4int numElements = material->GetNumberOfElements();
  G4double Mu = 0.;
  for (G4int i=0;i<numElements;i++) Mu += weightFrac[i]*linLogLogInterpolate_mu(energy,G4int((*elementVector)[i]->GetZ()));
  return Mu;
}

G4double LinearTrackLengthEstimator::linLogLogInterpolate_muen(G4double x, G4int index) {
  vector<G4double>& En = Energy[index];

  if ( x<En.front() ) return 0.;
  else if ( x>En.back() ) return MuenValue[index].back();


  G4double value = 0.;
  vector<G4double>& Mu = MuenValue[index];
  G4int bin = G4int(upper_bound(En.begin(),En.end(),x)-En.begin())-1;

  G4double e1 = En[bin];
  G4double e2 = En[bin+1];
  G4double d1 = Mu[bin];
  G4double d2 = Mu[bin+1];

  if ( d1>0. && d2>0. ) value = pow(10.,(log10(d1)*log10(e2/x) + log10(d2)*log10(x/e1)) / log10(e2/e1));
  else value = (d1*log10(e2/x) + d2*log10(x/e1)) / log10(e2/e1);

  return value;
}

G4double LinearTrackLengthEstimator::linLogLogInterpolate_mu(G4double x, G4int index) {
  vector<G4double>& En = Energy[index];

  if ( x<En.front() ) return 0.;
  else if ( x>En.back() ) return MuValue[index].back();

  G4double value = 0.;
  vector<G4double>& Mu = MuValue[index];
  G4int bin = G4int(upper_bound(En.begin(),En.end(),x)-En.begin())-1;

  G4double e1 = En[bin];
  G4double e2 = En[bin+1];
  G4double d1 = Mu[bin];
  G4double d2 = Mu[bin+1];

  if ( d1>0. && d2>0. ) value = pow(10.,(log10(d1)*log10(e2/x) + log10(d2)*log10(x/e1)) / log10(e2/e1));
  else value = (d1*log10(e2/x) + d2*log10(x/e1)) / log10(e2/e1);

  return value;
}