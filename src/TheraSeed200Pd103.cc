//################################################################
// http://vali.physics.carleton.ca/clrp/seed_database/Pd103/TheraSeed_200/
//################################################################
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4ParticleTable.hh"
#include "G4Material.hh"
#include "G4NistManager.hh"
#include "G4RandomDirection.hh"
#include "Randomize.hh"
#include "TheraSeed200Pd103.hh"

G4double TheraSeed200Pd103::energy[8] = {20.074*CLHEP::keV, 20.216*CLHEP::keV, 22.72*CLHEP::keV, 23.18*CLHEP::keV, 39.75*CLHEP::keV, 294.98*CLHEP::keV, 357.5*CLHEP::keV, 497.1*CLHEP::keV};

G4double TheraSeed200Pd103::CDF[8] = {0.2904,0.5484,0.1348,0.0251,0.00882,0.000039,0.000285,0.0000518};

G4double TheraSeed200Pd103::a[7] = {-0.0036394,-0.058095,1.9098,-0.26445,0.012862,7.0066E-11,0.46780};

G4double TheraSeed200Pd103::R[2] = {0.05,10.};

TheraSeed200Pd103::TheraSeed200Pd103():VSeed("TheraSeed200Pd103") {
  VSeed::numentries = 8;
  VSeed::energy = TheraSeed200Pd103::energy;
  VSeed::CDF    = TheraSeed200Pd103::CDF;
  VSeed::Calibration = 0.6620;// +/- 0.0009
  VSeed::Lambda = 0.686;
  VSeed::HalfLife = 17.0*24; // [h]

  //################################################################ 
  // Seed dimensions 
  //################################################################ 
  ShellHalfLength = 4.5*CLHEP::mm/2.;
  ShellOutRadius = 0.413*CLHEP::mm;
  ShellInRadius = 0.357*CLHEP::mm;
  CylindricalHalfLength = 0.334*CLHEP::mm/2.;
  CoatingThickness = 2.2*CLHEP::micrometer;
  GraphiteOutRadius = 0.28*CLHEP::mm;
  GraphiteLength = 0.89*CLHEP::mm;
  LeadOutRadius = 0.255*CLHEP::mm;
  LeadLength = 1.09*CLHEP::mm;
  HemisphereInRadius = 0.306*CLHEP::mm;
  HemisphereOutRadius = 0.346*CLHEP::mm;
  GapHalfLength = ShellHalfLength-2.*CylindricalHalfLength;
  PhaseGraphite = 1.057*CLHEP::mm;

  RatioDiskTotal = ((GraphiteOutRadius+CoatingThickness)*(GraphiteOutRadius+CoatingThickness)*CLHEP::pi*CoatingThickness)/(4*(GraphiteOutRadius+CoatingThickness)*(GraphiteOutRadius+CoatingThickness)*CLHEP::pi*CoatingThickness+2*GraphiteLength*(CLHEP::pi*(GraphiteOutRadius+CoatingThickness)*(GraphiteOutRadius+CoatingThickness)-CLHEP::pi*GraphiteOutRadius*GraphiteOutRadius));
    
  SurfCDF[0] = RatioDiskTotal;
  SurfCDF[1] = 2.*RatioDiskTotal;
  SurfCDF[2] = 3.*RatioDiskTotal;
  SurfCDF[3] = 4.*RatioDiskTotal;
  SurfCDF[4] = 1.-((1.-4.*RatioDiskTotal)/2.);
  SurfCDF[5] = 1.;

  VSeed::OutRadius = ShellOutRadius;
  VSeed::ZHalfLength = ShellHalfLength;
  VSeed::ActiveLength = 4.23*CLHEP::mm;//2*VSeed::ZHalfLength;
}

void TheraSeed200Pd103::Construct() { 
  //################################################################ 
  // Define the seed materials 
  //################################################################ 

  // NIST material definitions
  G4NistManager *man = G4NistManager::Instance();

  //Material definitions from NIST
  G4Material* Air = man->FindOrBuildMaterial("G4_AIR");
  G4Material* matTi = man->FindOrBuildMaterial("G4_Ti");
  G4Material* matPd = man->FindOrBuildMaterial("G4_Pd");
  G4Material* matPb = man->FindOrBuildMaterial("G4_Pb");
  G4Material* matGraphite = man->FindOrBuildMaterial("G4_GRAPHITE");

  //################################################################
  // Define the seed geometry
  //################################################################

  // Outer titanium shell
  G4Tubs* TiShell = new G4Tubs("TiShell",ShellInRadius,ShellOutRadius,ShellHalfLength,0,CLHEP::twopi);
  G4Tubs* TiCylinder = new G4Tubs("TiCylinder",HemisphereInRadius,ShellInRadius,CylindricalHalfLength,0,CLHEP::twopi);
  G4Sphere* TiSphere1 = new G4Sphere("TiSphere1",HemisphereInRadius,HemisphereOutRadius,0,CLHEP::twopi,0,CLHEP::halfpi);
  G4Sphere* TiSphere2 = new G4Sphere("TiSphere2",HemisphereInRadius,HemisphereOutRadius,0,CLHEP::twopi,CLHEP::halfpi,CLHEP::pi);
  G4UnionSolid* Shell1 = new G4UnionSolid("Shell1",TiShell,TiCylinder,0,G4ThreeVector(0,0,-GapHalfLength-CylindricalHalfLength));
  G4UnionSolid* Shell2 = new G4UnionSolid("Shell2",Shell1,TiCylinder,0,G4ThreeVector(0,0,+GapHalfLength+CylindricalHalfLength));
  G4UnionSolid* Shell3 = new G4UnionSolid("Shell3",Shell2,TiSphere1,0,G4ThreeVector(0,0,-GapHalfLength));
  G4UnionSolid* Shell = new G4UnionSolid("Shell",Shell3,TiSphere2,0,G4ThreeVector(0,0,+GapHalfLength));
  caps_log = new G4LogicalVolume(Shell,matTi,"caps_log");
  G4VisAttributes* caps_att = new G4VisAttributes(G4Colour(1,0,1,1.));
  caps_att->SetVisibility(true);
  caps_att->SetForceSolid(true);
  caps_log->SetVisAttributes(caps_att);

  // Air gap
  G4Tubs* Air_tube = new G4Tubs("Air_tube",0,ShellInRadius,GapHalfLength,0,CLHEP::twopi);
  G4Sphere* TiSphereL = new G4Sphere("TiSphereL",0,HemisphereOutRadius,0,CLHEP::twopi,0,CLHEP::halfpi);
  G4Sphere* TiSphereR = new G4Sphere("TiSphereR",0,HemisphereOutRadius,0,CLHEP::twopi,CLHEP::halfpi,CLHEP::pi);
  G4SubtractionSolid* gap1 = new G4SubtractionSolid("gap1",Air_tube,TiSphereL,0,G4ThreeVector(0,0,-GapHalfLength)); 
  G4SubtractionSolid* gap_vol = new G4SubtractionSolid("gap_vol",gap1,TiSphereR,0,G4ThreeVector(0,0,+GapHalfLength)); 
  gap_log = new G4LogicalVolume(gap_vol,Air,"gap_log");
  G4VisAttributes* gap_att = new G4VisAttributes(G4Colour(1,0,1,0.5));
  gap_att->SetVisibility(true);
  gap_att->SetForceSolid(true);
  gap_log->SetVisAttributes(gap_att);
  
  // Pd103 coating
  G4Tubs* coating_vol = new G4Tubs("coating_vol",GraphiteOutRadius,GraphiteOutRadius+CoatingThickness,0.5*(GraphiteLength+CoatingThickness),0,CLHEP::twopi);
  coating_log = new G4LogicalVolume(coating_vol,matPd,"coating_log");
  G4VisAttributes* coating_att = new G4VisAttributes(G4Colour(1,0.5,0,0.5));
  coating_att->SetVisibility(true);
  coating_att->SetForceSolid(true);
  coating_log->SetVisAttributes(coating_att);

  // Lead marker
  G4Tubs* marker_vol = new G4Tubs("marker_vol",0,LeadOutRadius,0.5*LeadLength,0,CLHEP::twopi);
  marker_log = new G4LogicalVolume(marker_vol,matPb,"marker_log");
  G4VisAttributes* marker_att = new G4VisAttributes(G4Colour(1,0.1,0.1,0.5));
  marker_att->SetVisibility(true);
  marker_att->SetForceSolid(true);
  marker_log->SetVisAttributes(marker_att);

  // Graphite cylinders
  G4Tubs* core_vol = new G4Tubs("core_vol",0,GraphiteOutRadius,0.5*GraphiteLength,0,CLHEP::twopi);
  core_log = new G4LogicalVolume(core_vol,matGraphite,"core_log");
  G4VisAttributes* core_att = new G4VisAttributes(G4Colour(1,1,1,0.5));
  core_att->SetVisibility(true);
  core_att->SetForceSolid(true);
  core_log->SetVisAttributes(core_att);

  VSeed::OuterShell = Shell;
}

void TheraSeed200Pd103::PlaceYourself(G4VPhysicalVolume* parent,G4int copyNo) {
  if ( !VSeed::OuterShell ) Construct();
  new G4PVPlacement(0,G4ThreeVector(),"caps_phys",caps_log,parent,false,copyNo);
  G4PVPlacement* gap_phys = new G4PVPlacement(0,G4ThreeVector(),"gap_phys",gap_log,parent,false,copyNo);
  new G4PVPlacement(0,G4ThreeVector(),"marker_phys",marker_log,gap_phys,false,copyNo);

  new G4PVPlacement(0,G4ThreeVector(0,0,+PhaseGraphite),"coating_phys",coating_log,gap_phys,false,2*copyNo);
  new G4PVPlacement(0,G4ThreeVector(0,0,+PhaseGraphite),"core_phys",core_log,gap_phys,false,2*copyNo);
  new G4PVPlacement(0,G4ThreeVector(0,0,-PhaseGraphite),"coating_phys",coating_log,gap_phys,false,2*copyNo+1);
  new G4PVPlacement(0,G4ThreeVector(0,0,-PhaseGraphite),"core_phys",core_log,gap_phys,false,2*copyNo+1);
}

G4ThreeVector TheraSeed200Pd103::GetEmissionPosition() {
  enum {side1pellet1,side2pellet1,side1pellet2,side2pellet2,pellet1,pellet2};

  G4double rval = G4UniformRand();
  G4ThreeVector v;
  G4int i;

  for (i=0;i<5;i++) {
    if ( rval<SurfCDF[i] ) break;
  }

  switch (i) {
    case side1pellet1:
      v = SelectPointOnDisk(GraphiteOutRadius+CoatingThickness);
      v.setZ(-(GraphiteLength/2)-G4UniformRand()*CoatingThickness-PhaseGraphite); 
      return v;
      break;
    case side2pellet1:
      v = SelectPointOnDisk(GraphiteOutRadius+CoatingThickness);
      v.setZ(GraphiteLength/2+G4UniformRand()*CoatingThickness-PhaseGraphite); 
      return v;
      break;
    case side1pellet2:
      v = SelectPointOnDisk(GraphiteOutRadius+CoatingThickness);
      v.setZ(-(GraphiteLength/2)-G4UniformRand()*CoatingThickness+PhaseGraphite); 
      return v;
      break;
    case side2pellet2:
      v = SelectPointOnDisk(GraphiteOutRadius+CoatingThickness);
      v.setZ(GraphiteLength/2+G4UniformRand()*CoatingThickness+PhaseGraphite); 
      return v;
      break;
    case pellet1:
      v = SelectPointOnCylinder(GraphiteOutRadius+G4UniformRand()*CoatingThickness, GraphiteLength/2);
      v += G4ThreeVector(0,0,-PhaseGraphite);
      return v;
      break;
    case pellet2:
      v = SelectPointOnCylinder(GraphiteOutRadius+G4UniformRand()*CoatingThickness, GraphiteLength/2);
      v += G4ThreeVector(0,0,PhaseGraphite);
      return v;
      break;
  }
  
  return G4ThreeVector(); 
}
