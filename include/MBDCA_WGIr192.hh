#ifndef MBDCA_WGIr192_hh
#define MBDCA_WGIr192_hh

//#include "globals.hh"
#include "VSeed.hh"

class G4LogicalVolume;

class MBDCA_WGIr192: public VSeed {
public:
  MBDCA_WGIr192();
  ~MBDCA_WGIr192(){;}
 
  void Construct();
  void PlaceYourself(G4VPhysicalVolume*,G4int);
  G4ThreeVector GetEmissionPosition();
  
private:
  static G4double energy[34];
  static G4double CDF[34];
  static G4double a[7];
  static G4double R[2];

  G4double core_radius;
  G4double core_half_length;
  G4double caps_radius;
  G4double caps_half_length;
  G4double cable_half_length;
  G4double phase;
  G4double cable_pos;

  G4LogicalVolume* caps_log;
  G4LogicalVolume* core_log;
  G4LogicalVolume* cable_log;
};

#endif
