#include "G4ParticleDefinition.hh"
#include "G4ProductionCutsTable.hh"
#include "G4ProcessManager.hh"
#include "G4ParticleTypes.hh"
#include "G4PhysicsListHelper.hh"
#include "G4ParallelWorldProcess.hh"
#include "G4EmProcessOptions.hh"
#include "G4UAtomicDeexcitation.hh"
#include "PhysicsList.hh"

// radioactive decay
#ifdef PHASESPACE
#include "G4IonConstructor.hh"
#include "G4RadioactiveDecay.hh"
#include "G4RadioactiveDecayPhysics.hh"
#endif

// ga	a
#include "G4RayleighScattering.hh"
#include "G4PhotoElectricEffect.hh"
#include "G4ComptonScattering.hh"
#include "G4GammaConversion.hh"
#include "G4LivermoreRayleighModel.hh"
#include "G4LivermorePhotoElectricModel.hh"
#include "G4LivermoreComptonModel.hh"
#include "G4LivermoreGammaConversionModel.hh"
// e-
#include "G4eMultipleScattering.hh"
#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4LivermoreIonisationModel.hh"
#include "G4LivermoreBremsstrahlungModel.hh"
// e+
#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"

PhysicsList::PhysicsList(G4String& parWorldName) :pWorldName(parWorldName) {
  SetVerboseLevel(1);
}

PhysicsList::~PhysicsList() { }

void PhysicsList::ConstructParticle() {
  // In this method, static member functions should be called for all particles which you want to use.
  // This ensures that objects of these particle types will be created in the program.
  G4Gamma::GammaDefinition();
  G4Electron::ElectronDefinition();
  G4Positron::PositronDefinition();
#ifdef PHASESPACE
  G4NeutrinoE::NeutrinoEDefinition();
  G4AntiNeutrinoE::AntiNeutrinoEDefinition();
  G4IonConstructor iConstructor; iConstructor.ConstructParticle();
#endif
}

void PhysicsList::ConstructProcess() {
#ifdef PHASESPACE
  // Decay
  G4RadioactiveDecay* radioactiveDecay = new G4RadioactiveDecay();
  //radioactiveDecay->SetHLThreshold(-1.*s);
  //radioactiveDecay->SetICM(true);
  //radioactiveDecay->SetARM(false);
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();
  ph->RegisterProcess(radioactiveDecay,G4GenericIon::GenericIon());
#endif

  AddTransportation();
  if ( pWorldName=="ParallelWorld" ) AddParallelWorldProcess();
  ConstructEM();
}

void PhysicsList::AddParallelWorldProcess() {
  // Add parallel world process
  G4ParallelWorldProcess* theParallelWorldProcess = new G4ParallelWorldProcess("paraWorldProc");
  theParallelWorldProcess->SetParallelWorld(pWorldName);
  theParallelWorldProcess->SetLayeredMaterialFlag();

  theParticleIterator->reset();

  while ( (*theParticleIterator)() ) {
    G4ParticleDefinition* particle = theParticleIterator->value();
    if ( !particle->IsShortLived() ) {
      G4ProcessManager* pmanager = particle->GetProcessManager();
      pmanager->AddProcess(theParallelWorldProcess);
      if ( theParallelWorldProcess->IsAtRestRequired(particle) ) { pmanager->SetProcessOrdering(theParallelWorldProcess,idxAtRest,9999); }
      pmanager->SetProcessOrdering(theParallelWorldProcess,idxAlongStep,1);
      pmanager->SetProcessOrdering(theParallelWorldProcess,idxPostStep,9999);
    }
  }

}

void PhysicsList::ConstructEM() {

  theParticleIterator->reset();
  G4PhysicsListHelper* ph = G4PhysicsListHelper::GetPhysicsListHelper();

  while ( (*theParticleIterator)() ) {
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4String particleName = particle->GetParticleName();

    // Photon
    if ( particleName=="gamma" ) {
      G4RayleighScattering* theRayleighScattering = new G4RayleighScattering();
      theRayleighScattering->SetEmModel(new G4LivermoreRayleighModel());
      ph->RegisterProcess(theRayleighScattering,particle);

      G4PhotoElectricEffect* thePhotoElectricEffect = new G4PhotoElectricEffect();
      thePhotoElectricEffect->SetEmModel(new G4LivermorePhotoElectricModel());
      ph->RegisterProcess(thePhotoElectricEffect,particle);

      G4ComptonScattering* theComptonScattering = new G4ComptonScattering();
      theComptonScattering->SetEmModel(new G4LivermoreComptonModel());
      ph->RegisterProcess(theComptonScattering,particle);

      G4GammaConversion* theGammaConversion = new G4GammaConversion();
      theGammaConversion->SetEmModel(new G4LivermoreGammaConversionModel());
      ph->RegisterProcess(theGammaConversion,particle);

    // Electron
    } else if ( particleName=="e-" ) {
      G4eMultipleScattering* msc = new G4eMultipleScattering();
      msc->SetStepLimitType(fUseDistanceToBoundary);
      ph->RegisterProcess(msc,particle);

      // Ionisation
      G4eIonisation* eIonisation = new G4eIonisation();
      eIonisation->SetEmModel(new G4LivermoreIonisationModel());
      eIonisation->SetStepFunction(0.2,100*CLHEP::um); // improved precision in tracking
      ph->RegisterProcess(eIonisation,particle);

      // Bremsstrahlung
      G4eBremsstrahlung* eBremsstrahlung = new G4eBremsstrahlung();
      eBremsstrahlung->SetEmModel(new G4LivermoreBremsstrahlungModel());
      ph->RegisterProcess(eBremsstrahlung,particle);

    // Positron
    } else if ( particleName=="e+" ) {
      G4eMultipleScattering* msc = new G4eMultipleScattering();
      msc->SetStepLimitType(fUseDistanceToBoundary);
      ph->RegisterProcess(msc,particle);

      // Ionisation
      G4eIonisation* eIonisation = new G4eIonisation();
      eIonisation->SetStepFunction(0.2,100*CLHEP::um);
      ph->RegisterProcess(eIonisation,particle);

      // Bremsstrahlung (use default, no low-energy available)
      ph->RegisterProcess(new G4eBremsstrahlung(),particle);

      // Annihilation
      ph->RegisterProcess(new G4eplusAnnihilation(),particle);
    }
  }

  /*
  // Deexcitation
  G4EmProcessOptions emOptions;
  emOptions.SetDeexcitationActiveRegion("World",true,true,true);
  emOptions.SetFluo(true);
  emOptions.SetAuger(true);
  emOptions.SetPIXE(true);

  // for 4.9.6
  G4VAtomDeexcitation* de = new G4UAtomicDeexcitation();
  de->SetFluo(true);
  de->SetAuger(true);
  de->SetPIXE(true);
  G4LossTableManager::Instance()->SetAtomDeexcitation(de);
*/
}

void PhysicsList::SetCuts() {
  // The production threshold is fixed to 0.1 mm for all the particles
  // Secondary particles with a range bigger than 0.1 mm are generated;
  // otherwise their energy is considered deposited locally

  defaultCutValue = 0.5 * CLHEP::mm;

  const G4double cutForGamma = defaultCutValue;
  const G4double cutForElectron = defaultCutValue;
  const G4double cutForPositron = defaultCutValue;

  SetCutValue(cutForGamma,"gamma");
  SetCutValue(cutForElectron,"e-");
  SetCutValue(cutForPositron,"e+");

  // Set the secondary production cut lower than 990. eV
  // Very important for high precision of lowenergy processes at low energies

  G4double lowLimit = 250. * CLHEP::eV;
  G4double highLimit = 100. * CLHEP::GeV;
  G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(lowLimit,highLimit);

  if (verboseLevel>0) DumpCutValuesTable();
}
